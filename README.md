What is this thing?
====================

DCTV is a trace analysis tool and viewer.

How do I locally develop?
=========================

## Get the source code

You're looking at it.

## Post-checkout repository initialization

    ./setup-git-hooks.sh

## First build and automatic sanity check

    make -j4 check

## Clean native code

    make clean-native

## Building with -O0 and asserts

    make clean-native  # Optional
    make -j4 dev-debug

## Building with -O0 and asserts and GCC

    make clean-native  # Optional
    make -j4 dev-debug CXX=g++-8

## Run random tests

    ./pytest src/dctv/myfile.py:mytest

## Compiling a package with debug symbols

    ./venv/run pip3 uninstall numpy
    ./venv/run pip3 install  --no-binary=:all: --no-deps -vvvv --global-option={build_ext,-g} numpy==1.15.4

## Submitting

    git push origin HEAD:refs/for/master

## Print raw DCTV snapshot data

    sed -nre 's/^# (\{.*)/\1/p' < dctv.trace  | jq -r '.["snapshots"][0]' > raw_snapshot

As a side effect of the `git push`, git will print a Gerrit
URL that you can use to manage your CL and eventually submit it.

How do I use this thing?
========================

# Start up the GUI on a trace file

    ./dctv gui myfile.trace

# SQL REPL

    ./dctv repl mytrace=myfile.trace

Maintenance tasks
=================

# Refresh the bootstrap package list

We use Poetry for most package management, but bootstrap it with pip
manually when building a fresh venv.  See `venv.mk` for details.
To refresh the checked-in packages, run `make -f venv.mk
download-bootstrap-packages`.

It's okay for the bootstrap packages to be somewhat out of date, since
poetry can update these packages the usual way once it
initializes itself.

# Why zsh for runner scripts?

Zsh is the only thing that provides realpath-like functionality
out-of-the-box in a way that's short enough for a one-liner and
portable to macOS.

External dependencies
=====================

We download external dependencies at venv setup time.  We use poetry
for Python packages.  We handle native dependencies by asking for
system packages (e.g., for boost).  Where system packages aren't
widely available (e.g., Abseil), we have ad-hoc code in native.mk to
download specific snapshots of certain projects, with hashes and
upstream repository information if native-deps-conf.mk.

Abseil
------

Strongly prefer standard C++ and Boost to Abseil.  We snapshot Abseil.
We don't "live at head".  We're interested mostly in the hash tables.
