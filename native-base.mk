# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Base makefile for native.mk
include base.mk

ABSEIL_BUILD_DIR:=$(VENV)/abseil-build
ABSEIL_INSTALL_DIR:=$(VENV)/abseil-install

BOOST_BUILD_DIR:=$(VENV)/boost-build
BOOST_INSTALL_DIR:=$(VENV)/boost-install

.PHONY: clean
clean:
	rm -f compile_commands.json
	rm -f $(DCTV_SRC)/_native.cpp
	rm -f $(DCTV_SRC)/*_gen.cpp
	rm -f $(DCTV_SRC)/_native.so
	rm -f $(DCTV_SRC)/*.o
	rm -f $(DCTV_SRC)/*.d
	rm -f $(DCTV_SRC)/*.typelib
	rm -f $(DCTV_SRC)/*.parser.out
	rm -f $(DCTV_SRC)/include_all_modules.h
	rm -f $(DCTV_SRC)/do_all_modules.h
	rm -rf $(ABSEIL_BUILD_DIR) $(ABSEIL_INSTALL_DIR)
	rm -rf $(BOOST_BUILD_DIR) $(BOOST_INSTALL_DIR)
