# -*- mode: shell-script; sh-basic-offset: 2 -*-
# This file contains the data used by the three syntax diagram rendering
# programs:
#
#   bubble-generator.tcl
#

# Graphs:
#
set all_graphs {
  sql-stmt-list {
    line {toploop {optx sql-stmt} ;} {opt ;}
  }
  sql-stmt {
    line
      {or
         select-stmt
         create-view-stmt
         drop-view-stmt
         mount-trace-stmt
         drop-all-stmt
      }
  }
  type-name {
    or
    INT8
    UINT8
    INT16
    UINT16
    INT32
    UINT32
    INT64
    UINT64
    FLOAT32
    FLOAT64
    BOOL
    STRING
  }
  ns-prefix {
    line {loop /namespace-part .}
  }
  table-ns-name {
    line
    {opt {line {loop /namespace-part .} .}}
    /table-name
  }
  tvf-ns-name {
    line
    {opt {line {loop /namespace-part .} .}}
    /tvf-name
  }
  tvf-arglist {
    line
    {or
     nil
     {line
      {loop tvf-expr ,}
      {optx , {loop {line /kwarg => tvf-expr} ,}}
     }
     {line {loop {line /kwarg => tvf-expr} ,}}}
    {opt ,}
  }
  tvf-expr {
    or
    literal-value
    {line tvf-expr /tvf-binop tvf-expr}
    {line /tvf-unop tvf-expr}
    {line table-ns-name}
    {line tvf-ns-name \( tvf-arglist \)}
    {line \( tvf-expr \) }
    {line \( select-stmt \) }
    {line \[ {loop tvf-expr ,} {opt ,} \]}
    {line "[L_CURLY]"
     {loop
      {or
       {line /kwarg => tvf-expr}
       {line tvf-expr : tvf-expr}
      }
      ,}
     {opt ,}
     "[R_CURLY]"}
  }
  function-name {
    line
    {opt {line {loop /namespace-part .} .}}
    /function-name
  }
  create-view-stmt {
    stack
       {line CREATE {opt OR REPLACE} VIEW}
       {line table-ns-name
        {opt \( {loop /rename-column ,}  {opt ,} \)}}
       {line  AS select-stmt}
  }
  values-list-row {
    line \( {loop literal-value ,} {opt ,} \)
  }
  values-list {
    line
    VALUES
    {loop values-list-row ,}
    {opt ,}
  }
  common-table-expression {
    stack
    {line /table-name {opt \( {loop /rename-column ,} {opt ,} \)} AS }
    {or
     table-ns-name
     {line tvf-ns-name \( tvf-arglist \)}
     {line \( values-list \) }
     {line \( select-stmt \) }
     {line \( table-spec \) }
    }
  }
  drop-view-stmt {
    line DROP VIEW {optx IF EXISTS} table-ns-name
  }
  bind-parameter {
    or
    {line : /parameter-name}
    {line {?} {optx /parameter-number}}
  }
  expr {
    or
     {line literal-value}
     {line bind-parameter}
     {line
      {optx table-ns-name .}
      /column-name}
     {line /unop expr}
     {line expr /binop expr}
     {line function-name \( {optx DISTINCT} function-arglist \) }
     {line ( expr ) }
     {line CAST ( expr AS type-name )}
     {line expr IN /unit-name}
     {line expr COLLATE /collation-name}
     {line expr IS {optx NOT} expr}
     {line expr {optx NOT} BETWEEN expr AND expr}
  }
  function-arglist {
    or
    {line
     {or
      nil
      {line
       {loop expr ,}
       {optx , {loop {line /kwarg => expr} ,}}
      }
      {line {loop {line /kwarg => expr} ,}}}
     {opt ,}
    }
    *
  }
  literal-value {
    or
     {line /numeric-literal {optx /unit-name}}
     {line /string-literal}
     {line NULL}
     {line TRUE}
     {line FALSE}
  }
  numeric-literal {
    or
     {stack {or
              {line {loop /digit nil} {opt /decimal-point {loop nil /digit}}}
              {line /decimal-point {loop /digit nil}}
           }
           {opt E {or nil + -} {loop /digit nil}}}
  }

  regular-select {
    stack
    {line SELECT
     {or
      {line SPAN {opt {loop result-column ,} {opt ,}}}
      {line {or nil DISTINCT ALL} {loop result-column ,} {opt ,}}}
    }
    {optx FROM table-or-join}
    {optx WHERE expr}
    {optx GROUP
     {or
      {line BY {loop expr ,} {opt ,}}
      {stack
       {line {opt AND {or INTERSECT UNION}} USING}
       {or
        {line PARTITION {optx FROM table-spec}}
        {line SPANS FROM table-spec}
       }}}}
    {optx HAVING expr}
  }

  select-stmt {
   stack
     {opt {line WITH {loop common-table-expression ,} {opt ,}}}
     {loop 
       {or regular-select values-list}
       compound-operator-name
     }
     {optx ORDER BY {loop ordering-term ,} {opt ,}}
     {optx LIMIT expr {optx {or OFFSET ,} expr}}
  }
  table-or-join {
    or
    {line table-or-join , table-or-join}
    conventional-join
    span-join
    span-broadcast
    {stack
     {or
      table-ns-name
      {line tvf-ns-name \( tvf-arglist \)}
      {line \( table-or-join \)}
      {line \( select-stmt \)}}
     {optx AS /table-alias
      {optx \( {loop /rename-column ,} {opt ,} \)}
     }}
  }
  conventional-join {
    stack
    {line
     table-or-join
     {or
      {line INNER}
      {line LEFT {opt OUTER}}
      {line RIGHT {opt OUTER}}
      {line OUTER}
     }
     JOIN
    }
    {line table-or-join
     {or
      {line ON expr}
      {line USING \( {loop /column-name ,} {opt ,} \)}
     }
    }
  }
  span-join {
    stack
    {line
     table-or-join
     SPAN
     {or
      {line INNER}
      {line LEFT {opt OUTER}}
      {line RIGHT {opt OUTER}}
      {line OUTER}
     }
     JOIN
    }
    {line table-or-join {opt PARTITION AS /partition-column}}
  }
  span-broadcast {
    stack
    {line table-or-join SPAN {or nil INNER OUTER} BROADCAST}
    {line {or INTO FROM} table-spec}
  }
  table-spec {
     or
     table-ns-name
     {line tvf-ns-name \( tvf-arglist \)}
     {line \( select-stmt \) }
  }
  result-column {
     or
        {line expr {opt {optx AS} /column-alias}}
        {line table-ns-name . *}
        *
  }
  ordering-term {
      line expr {opt COLLATE /collation-name} {or nil ASC DESC} 
  }
  compound-operator-name {
     or UNION {line UNION ALL} INTERSECT EXCEPT
  }
  comment-syntax {
    or
    {line {or {line -- whitepace} {#}}
     {loop nil /anything-except-newline}
     {or /newline /end-of-input}}
    {line /* {loop nil /anything-except-*/}
     {or */ /end-of-input}}
  }
  mount-trace-stmt {
    line
    MOUNT
    TRACE
    /quoted-trace-file-name
    AS
    ns-prefix
  }
  drop-all-stmt {
    line
    DROP ALL {opt IF EXISTS} ns-prefix
  }
}
