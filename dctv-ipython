#!/bin/zsh -e
# -*- python -*-
# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""":;s="${0:P}";. "${s:h}/shebang.zsh" python3 -- "$s" "$@";:<<'' #"""
import sys
from os.path import dirname, realpath, join as pjoin
from importlib.machinery import SourceFileLoader
import logging

mydir = dirname(realpath(__file__))
SourceFileLoader("dctv_main", pjoin(mydir, "dctv")) \
  .load_module() \
  .very_early_init()

logging.basicConfig(
  level=logging.DEBUG,
  format="%(process)d/%(threadName)s %(levelname)s:%(name)s:%(message)s")

# Work around logspam from ipython internals
logging.getLogger("root").setLevel(logging.INFO)
def _filter(record):
  return record.name != "root"

# TODO(dancol): this list of hard to maintain; just switch to a
# whitelist
logging.getLogger().addFilter(_filter)
logging.getLogger("py4j").setLevel(logging.INFO) # Stop spam
logging.getLogger("parso.python.diff").setLevel(logging.INFO) # Stop spam
logging.getLogger("parso.cache").setLevel(logging.INFO) # Stop spam
logging.getLogger("matplotlib").setLevel(logging.INFO) # Stop spam
logging.getLogger("asyncio").setLevel(logging.INFO) # Stop spam

sys.argv[1:1] = ["--InteractiveShellApp.extra_extension=dctv.ipython"]
import IPython
def start():
  import dctv
  import os
  import sys
  import pandas as pd
  import numpy as np
  T = True
  F = False
  N = None
  from numpy.ma import masked_array as ma, nomask, masked
  def i64(*args):
    return np.asarray(args, dtype="l")
  def i32(*args):
    return np.asarray(args, dtype="i")
  return IPython.start_ipython(user_ns=locals())
sys.exit(start())
