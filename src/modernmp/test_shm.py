# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for shm"""
import logging
import os
import weakref
from functools import partial

# pylint: disable=missing-docstring,redefined-outer-name,unused-argument

import pytest

from .shm import (
  ClientConnection,
  ClientConnectionBase,
  OID_SELF_CONNECTION,
  ResourceConstructionFailedError,
  SharedObject,
  USE_RESMANC_THREAD,
)

from .util_test import isolated_resman  # pylint: disable=unused-import
from .util_test import (
  fetch_and_set_state,
  fetch_state,
  wait_for_state_or_die,
)

log = logging.getLogger(__name__)

def test_mapping_method_stub_generation():
  """Make sure we correctly autogenerate the client thunks"""
  assert hasattr(ClientConnectionBase, "make_connection_factory")

def test_hello_world(isolated_resman):
  """Test that we can start the resource manager"""
  resman_pid = isolated_resman.getpid()
  if USE_RESMANC_THREAD:
    assert resman_pid == os.getpid()
  else:
    assert resman_pid != os.getpid()

def test_new_connection(isolated_resman):
  """Test creating new resource manager connections"""
  resmanc = isolated_resman
  resmanc.getpid()
  resmanc2_factory = resmanc.make_connection_factory("resmanc2")
  resmanc2 = resmanc2_factory()
  assert isinstance(resmanc2, ClientConnection)
  assert resmanc.id != resmanc2.id, "{!r} != {!r}".format(
    resmanc.id, resmanc2.id)
  assert resmanc.getpid() == resmanc2.getpid()
  resmanc.close()
  resmanc2.getpid()
  resmanc2.close()

_test_object_init_counter = 0

class TestObject(SharedObject):
  """Dummy object for testing"""
  def __init__(self, pvalue, *, kwvalue=None):
    self.pvalue = pvalue
    self.kwvalue = kwvalue
    global _test_object_init_counter
    self.init_counter_snapshot = _test_object_init_counter
    _test_object_init_counter += 1

def test_object(isolated_resman):
  """Test that we can create and retrieve a server-side object"""
  resmanc = isolated_resman
  obj = TestObject(42, kwvalue=5)
  assert obj.pvalue == 42
  assert obj.kwvalue == 5
  obj_oid = obj.oid
  assert obj_oid == 3
  assert resmanc.get_refcounts(obj_oid) == (1, 1)

class TestObject2(TestObject):
  """Dummy object that increments the object counter again

  Makes sure that superclass init works with TestObject
  metaclass rewrite.
  """
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    global _test_object_init_counter
    _test_object_init_counter += 1

def test_object_init_run_once(isolated_resman):
  """Test that object initialization happens only once"""
  global _test_object_init_counter
  _test_object_init_counter = 0
  _obj = TestObject2(42)
  assert _test_object_init_counter == (
    2 if USE_RESMANC_THREAD else 0)
  # pylint: disable=compare-to-zero
  assert _obj.init_counter_snapshot == 0
  _obj2 = TestObject2(1)
  assert _obj is not _obj2
  assert _test_object_init_counter == (
    4 if USE_RESMANC_THREAD else 0)
  assert _obj2.init_counter_snapshot == 2

def test_object_subref(isolated_resman):
  """Test object uniqueness"""
  resmanc = isolated_resman
  obj1 = TestObject(42)
  assert obj1.pvalue == 42
  obj1_oid = obj1.oid
  assert resmanc.get_refcounts(obj1_oid) == (1, 1)
  obj2 = TestObject(obj1)
  assert obj2.pvalue is obj1
  obj2_oid = obj2.oid
  assert obj2_oid != obj1_oid
  # The 2 comes from the server's LocalClient's reference to obj
  assert resmanc.get_net_refcounts(obj1_oid) == (1, 2)
  assert resmanc.get_net_refcounts(obj2_oid) == (1, 1)
  del obj2
  resmanc.sync_reference_counts()
  assert resmanc.get_net_refcounts(obj1_oid) == (1, 1)
  assert resmanc.get_net_refcounts(obj2_oid) == (0, 0)
  del obj1
  resmanc.sync_reference_counts()
  assert resmanc.get_net_refcounts(obj1_oid) == (0, 0)

def test_adopt_resource(isolated_resman):
  """Test that adopt_resource works as expected"""
  resmanc = isolated_resman
  obj1 = TestObject(42)
  obj1_oid = obj1.oid
  obj2 = TestObject(obj1)
  assert obj2.pvalue is obj1
  obj2_oid = obj2.oid
  resmanc.addref_for_test(obj2_oid)
  obj2_2 = resmanc.adopt_resource(obj2_oid)
  assert obj2_2 is obj2
  assert resmanc.get_net_refcounts(obj1_oid) == (1, 2)
  assert resmanc.get_net_refcounts(obj2_oid) == (1, 1)  # No refcount increase
  del obj2_2
  resmanc.sync_reference_counts()
  assert resmanc.get_net_refcounts(obj2_oid) == (1, 1)  # obj2_2 not tracked!
  resmanc.addref_for_test(obj2_oid)
  del obj2
  resmanc.sync_reference_counts()
  assert resmanc.get_net_refcounts(obj2_oid) == (1, 1)  # Net: no RC change
  obj2_3 = resmanc.adopt_resource(obj2_oid)
  assert obj2_3.pvalue is obj1  # Still the same object
  del obj2_3  # Drop it
  resmanc.sync_reference_counts()
  assert resmanc.get_net_refcounts(obj2_oid) == (0, 0)  # obj2_3 was owner; dead
  assert resmanc.get_net_refcounts(obj1_oid) == (1, 1)  # Server ref dropped

def test_object_construction_failure(isolated_resman):
  with pytest.raises(ResourceConstructionFailedError) as ar:
    TestObject()
  with pytest.raises(TypeError):
    raise ar.value.__cause__

def test_death_link_voluntary(isolated_resman):
  """Test that death notification works on explicit drop"""
  resmanc1 = isolated_resman
  obj1 = TestObject(42)
  obj1_oid = obj1.oid
  resmanc2 = resmanc1.make_connection_factory("resmanc2")()
  resmanc1.addref_for_test(obj1_oid, resmanc2.id)

  fetch_and_set_state(0)
  def _on_obj1_death():
    fetch_and_set_state(1)
  resmanc2.add_death_callback(obj1_oid, _on_obj1_death)
  def _on_fn_collected(_wr):
    assert fetch_state() == 1
    fetch_and_set_state(2)
  cb_wr = weakref.ref(_on_obj1_death, _on_fn_collected)
  del _on_obj1_death

  assert resmanc1.get_refcounts(obj1_oid) == (1, 2)
  resmanc2.release_resource(obj1_oid)
  resmanc2.sync_reference_counts()
  assert resmanc1.get_refcounts(obj1_oid) == (1, 1)

  del obj1
  resmanc1.sync_reference_counts()
  assert resmanc1.get_refcounts(obj1_oid) == (0, 0)

  wait_for_state_or_die(2)
  assert not cb_wr()
  resmanc2.close()

def test_death_link_force_shut(isolated_resman):
  """Test that resource notification works when connection forced shut"""
  resmanc1 = isolated_resman
  resmanc1.set_critical(False)
  obj1 = TestObject(42)
  obj1_oid = obj1.oid
  resmanc2 = resmanc1.make_connection_factory("resmanc2")()
  resmanc1.addref_for_test(obj1_oid, resmanc2.id)

  fetch_and_set_state(0)
  def _on_obj1_death():
    fetch_and_set_state(1)
  resmanc2.add_death_callback(obj1_oid, _on_obj1_death)

  assert resmanc1.get_refcounts(obj1_oid) == (1, 2)
  resmanc2.release_resource(obj1_oid)
  resmanc2.sync_reference_counts()
  resmanc1.sync_reference_counts()
  assert resmanc1.get_refcounts(obj1_oid) == (1, 1)

  resmanc1.close()  # Slam connection shut; drops ref to obj1
  wait_for_state_or_die(1)
  assert resmanc2.get_refcounts(obj1_oid) == (0, 0)
  resmanc2.close()

def test_death_link_multiple_calls(isolated_resman):
  obj1 = TestObject(42)
  fetch_and_set_state(0)
  def _on_death():
    fetch_and_set_state(fetch_state() + 1)
  isolated_resman.add_death_callback(obj1.oid, _on_death)
  isolated_resman.add_death_callback(obj1.oid, _on_death)
  del obj1
  wait_for_state_or_die(2)

def test_death_link_remove(isolated_resman):
  obj1 = TestObject(42)
  fetch_and_set_state(0)
  def _never_called():
    assert False
  key = isolated_resman.add_death_callback(obj1.oid, _never_called)
  isolated_resman.remove_death_callback(key)
  del obj1
  TestObject(41)

def test_death_link_multiple_calls_remove(isolated_resman):
  obj1 = TestObject(42)
  fetch_and_set_state(0)
  def _on_death():
    fetch_and_set_state(fetch_state() + 1)
  isolated_resman.add_death_callback(obj1.oid, _on_death)
  def _never_called():
    assert False
  key = isolated_resman.add_death_callback(obj1.oid, _never_called)
  isolated_resman.remove_death_callback(key)
  del obj1
  wait_for_state_or_die(1)

def test_death_link_nonexistent(isolated_resman):
  fetch_and_set_state(0)
  isolated_resman.add_death_callback(12345,
                                     partial(fetch_and_set_state, 1))
  wait_for_state_or_die(1)

def test_death_link_to_connection(isolated_resman):
  """Test that death link fires for connection OID too"""
  resmanc1 = isolated_resman
  resmanc2 = resmanc1.make_connection_factory("resmanc2")()

  fetch_and_set_state(0)
  resmanc1.add_death_callback(resmanc2.id,
                              partial(fetch_and_set_state, 1))
  resmanc2.close()
  wait_for_state_or_die(1)

def test_send_obj_with_ref(isolated_resman):
  resmanc = isolated_resman
  obj1 = TestObject(42)
  assert resmanc.get_net_refcounts(obj1.oid) == (1, 1)
  assert resmanc.get_refcounts(obj1.oid, OID_SELF_CONNECTION) == (0, 1)
  _var = TestObject(obj1)
  assert resmanc.get_net_refcounts(obj1.oid) == (1, 2)
  assert resmanc.get_refcounts(obj1.oid, OID_SELF_CONNECTION) == (1, 2)
