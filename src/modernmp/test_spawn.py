# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Test cases for spawn"""
import logging
from functools import partial
from .spawn import start_subprocess
from .util import unix_pipe

# pylint: disable=missing-docstring

log = logging.getLogger(__name__)

def _child(value):
  return value

def test_spawn_basic():
  value = 42
  child = start_subprocess(partial(_child, value))
  child.wait(5)
  assert child.returncode == value

def _pipe_child(pipe1_write, pipe2_read):
  pipe1_write.as_file("w", steal=True).write("hello")
  return int(pipe2_read.as_file(steal=True).read())

def test_spawn_fd_inherit():
  value = 34
  pipe1_read, pipe1_write = unix_pipe()
  pipe2_read, pipe2_write = unix_pipe()
  child = start_subprocess(partial(_pipe_child, pipe1_write, pipe2_read))
  del pipe1_write, pipe2_read
  assert pipe1_read.as_file(steal=True).read() == "hello"
  pipe2_write.as_file("w", steal=True).write(str(value))
  child.wait(5)
  assert child.returncode == value
