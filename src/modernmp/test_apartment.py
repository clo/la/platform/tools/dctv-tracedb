# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Test the apartment cross-context call stuff"""

# pylint: disable=missing-docstring,redefined-outer-name,unused-argument,not-callable

import logging
import threading
import weakref
import asyncio

from asyncio.exceptions import TimeoutError  # pylint: disable=redefined-builtin

import pytest

from .apartment import (
  Apartment,
  ApartmentAffinityMixin,
  ApartmentQuitError,
  Proxy,
  ThreadPool,
  proxy_for,
  unproxy,
)

from .util import (
  ChainableFuture,
)

from .util_test import (  # pylint: disable=unused-import
  ValueProducer,
  current_thread_apartment,
  fetch_and_set_state,
  fetch_state,
  isolated_resman,
  wait_for_state_or_die,
)

log = logging.getLogger(__name__)

@pytest.yield_fixture()
def event_loop():
  with current_thread_apartment() as apartment:
    yield apartment

class Thing(object):
  def __init__(self, value):
    self.value = value

  def get_value(self):
    return self.value

  def fail(self):  # pylint: disable=no-self-use
    raise ValueError("adfsa")

class Adder(object):
  def add(self, *args):  # pylint: disable=no-self-use
    value = 0
    for arg in args:
      value += arg()
    return value



def test_apartment_start(isolated_resman):
  with current_thread_apartment():
    pass

@pytest.mark.parametrize("nr_threads", [0, 1, 4])
def test_thread_pool_start(isolated_resman, nr_threads):
  with ThreadPool(nr_threads):
    pass

def test_apartment_call_oneway(isolated_resman):
  fetch_and_set_state(0)
  with ThreadPool(1) as tp:
    tp.call_gate.fn(fetch_and_set_state).call_oneway(1)
    wait_for_state_or_die(1)

@pytest.mark.asyncio
async def test_apartment_call_twoway_aio(isolated_resman):
  old_value = 5
  fetch_and_set_state(old_value)
  with ThreadPool(1) as tp:
    new_value = 42
    future = (tp.call_gate.fn(fetch_and_set_state)
              .call_aio(new_value))
    wait_for_state_or_die(new_value)
    assert await future == old_value

def _tp1_fn(value):
  return value + 2

def _tp2_fn(cg):
  future = cg.fn(_tp1_fn).call_async(fetch_state())
  fetch_and_set_state(future.result() + 1)

def test_apartment_call_twoway(isolated_resman):
  fetch_and_set_state(5)
  with ThreadPool(1) as tp1, ThreadPool(2) as tp2:
    tp2.call_gate.fn(_tp2_fn).call_oneway(tp1.call_gate)
    wait_for_state_or_die(5 + 3)

@pytest.mark.asyncio
async def test_proxy_call_async(isolated_resman):
  thing_value = 9
  thing = Thing(thing_value)
  thing_proxy = proxy_for(thing)
  assert await thing_proxy.get_value.call_aio() == thing_value

@pytest.mark.asyncio
async def test_proxy_call_async_fail(isolated_resman):
  thing_value = 9
  thing = Thing(thing_value)
  thing_proxy = proxy_for(thing)
  with pytest.raises(ValueError):
    await thing_proxy.fail.call_aio()

@pytest.mark.asyncio
async def test_unproxy(isolated_resman):
  thing = Thing(-1)
  thing_proxy = proxy_for(thing)
  assert thing is unproxy(thing_proxy)

@pytest.mark.asyncio
async def test_proxy_call_async_ref_drops(isolated_resman):
  """Test that the proxied object disappears when the proxy does"""
  thing_value = 9
  thing = Thing(thing_value)
  thing_proxy = proxy_for(thing)
  thing_proxy_wr = weakref.ref(thing_proxy)
  thing_wr = weakref.ref(thing)
  assert await thing_proxy.get_value.call_aio() == thing_value
  death_future = ChainableFuture()
  thing_finalizer = weakref.finalize(thing, death_future.set_result, 1)
  thing_finalizer.atexit = False
  assert thing_wr()
  del thing
  del thing_proxy  # Generates resmanc notification
  assert not thing_proxy_wr()
  assert await asyncio.wrap_future(death_future) == 1
  assert not thing_wr()

@pytest.mark.asyncio
async def test_proxy_call_sync_self_call(isolated_resman):
  """Test that synchronous intra-thread calls work"""
  value = proxy_for(Adder()).add()
  assert value == 0  # pylint: disable=compare-to-zero

@pytest.mark.asyncio
async def test_proxy_call_sync_self_recursive_call(isolated_resman):
  """Test that we can make synchronous subroutine calls to ourself"""
  value = proxy_for(Adder()).add(
    proxy_for(ValueProducer(2)),
    proxy_for(ValueProducer(3)))
  assert value == 5

def _make_value_producers(args):
  return [proxy_for(ValueProducer(arg)) for arg in args]

def _make_value_producer(arg):
  return _make_value_producers([arg])[0]

@pytest.mark.asyncio
async def test_proxy_call_sync_cross_apartment(isolated_resman):
  """Test that synchronous calls work across apartments"""
  with ThreadPool(1) as tp:
    value = proxy_for(Adder()).add(
      *tp.call_gate.fn(_make_value_producers)([2, 3]))
    assert value == 5

@pytest.mark.asyncio
async def test_proxy_call_sync_cross_apartment_callback(isolated_resman):
  """Test that we can get a callback from an apartment"""
  with ThreadPool(1) as tp:
    producer_1_proxy = proxy_for(ValueProducer(10))
    producer_2_proxy = \
      tp.call_gate.fn(_make_value_producer)(producer_1_proxy)
    producer_3_proxy = proxy_for(ValueProducer(producer_2_proxy))
    value = producer_3_proxy()
    assert value == 10

@pytest.mark.asyncio
async def test_ret_filter(isolated_resman):
  """Test that we can filter return values"""
  proxy = (Apartment.current().call_gate.fn(list)
           .proxy_ret().call_sync(("a", "b", "c")))
  assert isinstance(proxy, Proxy)
  assert proxy.index.call_sync("b") == 1

_pool_thread_cond = threading.Condition()
_pool_thread = None
def _capture_pool_thread():
  global _pool_thread
  with _pool_thread_cond:
    _pool_thread = threading.current_thread()
    _pool_thread_cond.notify_all()

def test_pool_auto_cleanup(isolated_resman):
  """Test that pool goes away when all references drop"""
  pool = ThreadPool(1)
  pool.call_gate.fn(_capture_pool_thread).call_oneway()
  with _pool_thread_cond:
    _pool_thread_cond.wait_for(lambda: _pool_thread)
  assert _pool_thread
  del pool  # Should cause cleanup
  _pool_thread.join(1)
  assert not _pool_thread.is_alive()

async def _say_hello_async():
  return "hello world"

async def _fetch_and_set_state_async(value):
  fetch_and_set_state(value)

@pytest.mark.asyncio
async def test_call_to_coroutine_oneway():
  fetch_and_set_state(1)
  with ThreadPool(1) as tp1:
    tp1.fn(_fetch_and_set_state_async).call_oneway(2)
    wait_for_state_or_die(2, 10)

@pytest.mark.asyncio
async def test_call_to_coroutine_twoway():
  # TODO(dancol): test detach with calls pending
  with ThreadPool(1) as tp1:
    assert await tp1.fn(_say_hello_async).call_aio() == "hello world"

async def _spam_do_call(thing, total_calls):
  futures = []
  for idx in range(total_calls):
    fetch_and_set_state(idx)
    future = thing.do_call.call_async()
    if future.done() and future.exception():
      raise future.exception()
    futures.append(future)
  result = 0
  for future in futures:
    result += await future
  return result

@pytest.mark.asyncio
async def test_blocking_message_queue_full():
  from socket import SOL_SOCKET, SO_SNDBUF
  from termios import TIOCOUTQ
  from .util import int_ioctl
  total_calls = 50
  fetch_and_set_state(0)
  with ThreadPool(1) as tp1, ThreadPool(1) as tp2:
    try:
      tp1_sock = tp1.call_gate.channel.data_socket
      tp1_sock.setsockopt(SOL_SOCKET, SO_SNDBUF, 4*4096)  # Block ASAP
      vpp = tp1.fn(ValueProducer).proxy_ret()(5)
      future = tp1.fn(wait_for_state_or_die).call_async(1000, 30.0)
      assert not future.done()
      future2 = tp2.fn(_spam_do_call).call_async(vpp, total_calls)
      with pytest.raises(TimeoutError):
        await asyncio.wait_for(future2, 0.1)
      assert not future2.done()
      # Make sure we have stuff in the queue and we're blocked
      queued_bytes = int_ioctl(tp1_sock, TIOCOUTQ)
      assert queued_bytes > 1
      assert 1 < fetch_state() < total_calls - 2
      # Unblock
      fetch_and_set_state(1000)
      assert await future2 == total_calls * 5
    finally:
      # Let pool thread exit and test terminate
      fetch_and_set_state(1000)
      await future

def test_last_detach_callback():
  detached_hook_run = False
  def _on_detach():
    nonlocal detached_hook_run
    detached_hook_run = True
  with current_thread_apartment() as loop:
    async def _doit():
      Apartment.current().add_weak_last_detach_callback(_on_detach)
    loop.run_until_complete(_doit())
    assert not detached_hook_run
  assert detached_hook_run

class TestApartmentAffinityObject(ApartmentAffinityMixin):
  def __init__(self):
    super().__init__()
    self.close_count = 0

  def close(self):
    self.close_count += 1
    super().close()

def test_apartment_affinity_object_auto_close():
  with pytest.raises(AssertionError):
    TestApartmentAffinityObject()
  obj = None
  with current_thread_apartment() as loop:
    async def _doit():
      nonlocal obj
      obj = TestApartmentAffinityObject()
      assert not obj.closed
    loop.run_until_complete(_doit())
    assert not obj.closed
  assert obj.closed
  assert obj.close_count == 1

def test_apartment_affinity_object_explicit_close():
  with pytest.raises(AssertionError):
    TestApartmentAffinityObject()
  obj = None
  with current_thread_apartment() as loop:
    async def _doit():
      nonlocal obj
      obj = TestApartmentAffinityObject()
      assert not obj.closed
      obj.close()
    loop.run_until_complete(_doit())
    assert obj.closed
  assert obj.close_count == 1

def test_apartment_call_async():
  outer_apartment = None
  def _say_hello():
    return "hello"
  def _fail():
    raise ValueError
  with current_thread_apartment() as loop:
    async def _doit():
      nonlocal outer_apartment
      outer_apartment = Apartment.current()
      with ThreadPool(1) as tp:
        apartment = tp.apartment
        assert await apartment.call_async(_say_hello) == "hello"
        with pytest.raises(ValueError):
          await apartment.call_async(_fail)
      future1 = apartment.call_async(_say_hello)
      with pytest.raises(ApartmentQuitError):
        await future1
    loop.run_until_complete(_doit())
    future2 = outer_apartment.call_async(_say_hello)
    assert not future2.done()
  assert future2.done()
  with pytest.raises(ApartmentQuitError):
    future2.result()
