// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "dctv.h"

#include <tuple>
#include <utility>

#include "automethod.h"
#include "dynamic_bitset.h"
#include "hash_table.h"
#include "native_aggregation.h"
#include "npy.h"
#include "npyiter.h"
#include "optional.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "result_buffer.h"
#include "string_table.h"
#include "vector.h"

namespace dctv {
namespace {

template<typename Value, typename Aggregation>
void
agg_1(QueryExecution* const qe,
      const bool distinct,
      NaContext nac,
      const npy_intp ngroups,
      const unique_pyref py_in,
      const unique_pyref py_out)
{
  using Kernel = typename Aggregation::template Kernel<Value>;
  using Out = typename Kernel::Out;
  npy_uint32 extra_op_flags[3] = { 0, NPY_ITER_NO_BROADCAST, 0 };
  NpyIterConfig config;
  config.extra_op_flags = &extra_op_flags[0];
  PythonChunkIterator<npy_intp, Value, bool> data(
      py_in.notnull().addref(), config);
  ResultBuffer<Out, bool> out(py_out.notnull().addref(), qe);
  using Crumb = std::pair<npy_intp, Value>;
  HashSet<Crumb> seen;

  assume(ngroups >= 0);

  Vector<Kernel> kernels;
  kernels.reserve(ngroups);
  for (npy_intp i = 0; i < ngroups; ++i)
    kernels.push_back(Kernel(0));
  DynamicBitset<> have_kernel(ngroups);

  for (; !data.is_at_eof(); data.advance()) {
    npy_intp groupno;
    Value value;
    bool is_masked;
    std::tie(groupno, value, is_masked) = data.get();
    assume(groupno >= 0);
    assume(groupno <= ngroups);

    if (is_masked)
      continue;

    if (distinct) {
      Crumb crumb(groupno, value);
      auto it = seen.find(crumb);
      if (it == seen.end())
        seen.insert(crumb);
      else
        continue;
    }

    if (!have_kernel[groupno]) {
      kernels[groupno] = Kernel(value);
      have_kernel[groupno] = true;
    } else {
      kernels[groupno].accumulate(value, &nac);
    }
  }

  for (npy_intp i = 0; i < ngroups; ++i) {
    if (have_kernel[i]) {
      out.add(kernels[i].get(), /*masked=*/false);
    } else if (Kernel::empty_value) {
      out.add(*Kernel::empty_value, /*masked=*/false);
    } else {
      out.add(0, /*masked=*/true);
    }
  }

  out.flush();
}

void
agg(QueryExecution* const qe,
    const std::string_view aggfunc,
    const bool distinct,
    const unique_dtype dtype,
    npy_intp ngroups,
    unique_pyref py_in,
    unique_pyref py_out,
    obj_pyref<StringTable> st,
    const pyref collation)
{
  String the_collation;
  if (collation && collation != Py_None)
    the_collation = str(collation);
  else
    the_collation = "binary";
  NaContext nac(st.get(), std::move(the_collation));
  auto do_agg = [&](auto value_dummy, auto agg_dummy) {
        using Value = decltype(value_dummy);
        using Aggregation = decltype(agg_dummy);
        agg_1<Value, Aggregation>(qe,
                                  distinct,
                                  std::move(nac),
                                  ngroups,
                                  std::move(py_in),
                                  std::move(py_out));
  };
  agg_and_dtype_dispatch(aggfunc, dtype, nac.is_string(), do_agg);
}

PyMethodDef functions[] = {
  AUTOFUNCTION(agg,
               "Conventional aggregation in native code",
               (QueryExecution*, qe)
               (std::string_view, aggfunc)
               (bool, distinct)
               (unique_dtype, dtype, no_default, convert_dtype)
               (npy_intp, ngroups)
               (unique_pyref, in)
               (unique_pyref, out)
               (OPTIONAL_ARGS_FOLLOW)
               (obj_pyref<StringTable>, st)
               (pyref, collation)
  ),
  { 0 }
};

}  // anonymous namespace

void
init_agg(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
