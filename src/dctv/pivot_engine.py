# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Pivot engine core"""
import logging
import asyncio

from collections import defaultdict
from itertools import count
from functools import partial
from enum import Enum
from weakref import WeakValueDictionary, ref as weak_ref

from cytoolz import valmap
import numpy as np

from modernmp.util import (
  ChainableFuture,
  assert_map_type,
  assert_seq_type,
  ignore,
  the,
)

from .util import (
  AllCompleteFuture,
  CodeGenerator,
  ExplicitInheritance,
  FrozenIntRangeDict,
  IdentityHashedImmutable,
  Immutable,
  Interned,
  SortedDict,
  abstract,
  cached_property,
  final,
  future_exception,
  future_result_now,
  get_object_cache,
  iattr,
  override,
)

from .sql_util import (
  SqlBundle,
)

from .gtk_util import (
  GObject,
)

log = logging.getLogger(__name__)

class Placeholder(object):
  """Marker class indicating non-data column query results"""

class PlaceholderEnum(Placeholder, Enum):
  """Non-datum results from query value lookup"""
  NO_DATA = "..."
  LOADING = "ldg"
  AMBIGUOUS = ""
  RESTRICTED = ""

  def __str__(self):
    return self.value

NO_DATA = PlaceholderEnum.NO_DATA
LOADING = PlaceholderEnum.LOADING
AMBIGUOUS = PlaceholderEnum.AMBIGUOUS
RESTRICTED = PlaceholderEnum.RESTRICTED

@final
class SqlFromPart(Interned):
  """Basically, the FROM part of a SQL query."""

  base = iattr(SqlBundle, converter=SqlBundle.coerce_)
  group_by = iattr(SqlBundle,
                   kwonly=True,
                   converter=SqlBundle.coerce_,
                   default=None,
                   nullable=True)
  where = iattr(SqlBundle,
                kwonly=True,
                converter=SqlBundle.coerce_,
                default=None,
                nullable=True)
  order_by = iattr(SqlBundle,
                   nullable=True,
                   kwonly=True,
                   default=None,
                   converter=SqlBundle.coerce_)

  def __make_sql_table_expression(self, *, sort):
    fmt = ":base WHERE :where GROUP BY :group_by"
    kwargs = dict(base=self.base,
                  where=self.where or SqlBundle.of("TRUE"),
                  group_by=self.group_by or SqlBundle.of("ROW_NUMBER()"))
    if sort and self.order_by:
      fmt += " ORDER BY :order_by"
      kwargs["order_by"] = self.order_by
    return SqlBundle.format(fmt, **kwargs)

  @cached_property
  def sql_table_expression(self):
    """SQL bundle for the SQL table expression providing columns"""
    return self.__make_sql_table_expression(sort=True)

  @final
  def make_query_for_ca(self, ca):
    """Make a SqlBundle for a combination of column spec and base"""
    assert isinstance(ca, SqlBundle)
    cache = get_object_cache(self)
    qb = cache.get(ca)
    if not qb:
      cache[ca] = qb = SqlBundle.format(
        "SELECT :column_expression FROM :sql_table_expression",
        column_expression=ca,
        sql_table_expression=self.sql_table_expression)
    return qb

  @cached_property
  def group_size_query(self):
    """Query bundle giving sizes of the result set"""
    return SqlBundle.format(
      "SELECT COUNT(*) FROM :sql_table_expression",
      sql_table_expression=self.sql_table_expression)

  @cached_property
  def count_query(self):
    """Query bundle for the size of the total result"""
    # To improve cache hit rate, we don't include the irrelevant ORDER
    # BY in the count.
    return SqlBundle.format(
      "SELECT COUNT(*) FROM (SELECT ROW_NUMBER() FROM :table_expression)",
      table_expression=self.__make_sql_table_expression(sort=False))

class PivotNode(IdentityHashedImmutable):
  """Description of one tree item in a PivotTable view"""

  number_rows = iattr(int, kwonly=True)
  number_restrict = iattr(int, kwonly=True)

  @abstract
  def lookup_column(self,
                    localcc,
                    display_row_number,
                    query_row_number,
                    ca):
    """Find the value of a column"""
    raise NotImplementedError("abstract")

  @abstract
  def lookup_group_size(self,
                        localcc,
                        display_row_number,
                        query_row_number):
    """Find the true size of a group aggregated into a single row"""
    # We need a separate lookup_group_size because the size of the
    # group isn't represented by any specific ca.
    raise NotImplementedError("abstract")

@final
class PlaceholderPivotNode(PivotNode):
  """A PivotNode that supplies immediate values"""

  # We use an instance of this class to provide dummy values not
  # backed by any query, e.g., in case a query results in an error,
  # when we haven't received the result of a query, and so on.
  # It always provides just the one row.

  value = iattr(kwonly=True)

  @override
  def __new__(cls, *, value, number_restrict=0, number_rows=1):
    return cls._do_new(cls,
                       value=value,
                       number_rows=number_rows,
                       number_restrict=number_restrict)

  @override
  def lookup_column(self,
                    localcc,
                    display_row_number,
                    query_row_number,
                    ca):
    return self.value

  @override
  def lookup_group_size(self,
                        localcc,
                        display_row_number,
                        query_row_number):
    return 1

@final
class RootPivotNode(PivotNode):
  """Special PivotNode that serves as root of the tree"""

  # By making the PivotNode tree always start with a zero-row
  # placeholder (an instance of this class) we can avoid special cases
  # in the rest of the tree management code.

  @override
  def __new__(cls):
    return cls._do_new(cls, number_rows=0, number_restrict=0)

  @override
  def lookup_column(self,
                    _localcc,
                    _display_row_number,
                    _query_row_number,
                    _ca):
    raise AssertionError("should never be reached")  # Zero visible rows

  @override
  def lookup_group_size(self,
                        _localcc,
                        _display_row_number,
                        _query_row_number):
    raise AssertionError("should never be reached")  # Zero visible rows

@final
class QueryPivotNode(PivotNode):
  """A PivotNode that retrieves data using DB queries"""

  qm = iattr(SqlFromPart, kwonly=True)

  @override
  def lookup_column(self,
                    localcc,
                    display_row_number,
                    query_row_number,
                    ca):
    return localcc.lookup_query_bundle_row(
      self.qm.make_query_for_ca(ca),
      display_row_number,
      query_row_number)

  @override
  def lookup_group_size(self,
                        localcc,
                        display_row_number,
                        query_row_number):
    return localcc.lookup_query_bundle_row(
      self.qm.group_size_query,
      display_row_number,
      query_row_number)

def gen_tree_intervals(root, get_children):
  """Generate (NODE, QUERY_ROW_OFFSET, SIZE) for tree ROOT

  The generated items form a flattened map of the tree.  Each NODE is
  the node that supplies data for the given tree region;
  QUERY_ROW_OFFSET is the offset from the start of NODE's result set
  at which records for the generated region begin; SIZE is the number
  of rows in this region.

  GET_CHILDREN is a function that given a node returns a sequence of
  (CHILD_LOCATION, CHILD) pairs, each pair naming a child of that node
  and that child's query-row index.
  """
  assert isinstance(root, PivotNode), \
    "expected a PivotNode: found a {!r}".format(root)
  query_row = 0
  for child_query_row, child in get_children(root):
    if query_row < child_query_row:
      yield root, query_row, child_query_row - query_row
    yield from gen_tree_intervals(child, get_children)
    query_row = child_query_row
  if query_row < root.number_rows:
    yield root, query_row, root.number_rows - query_row

def _gen_map_contents(root, get_children):
  display_row = 0
  for node, query_row_offset, size in gen_tree_intervals(
      root, get_children):
    assert size
    yield display_row, (node, query_row_offset, display_row)
    display_row += size
  yield display_row

def make_display_row_map(node, get_children):
  """Make a FrozenIntRangeDict mapping display rows to queries

  The returned mapping maps each display row number to a display
  chunk, encoded as (QUERY, QUERY_ROW_OFFSET,
  DISPLAY_ROW_OFFSET). QUERY is the SqlFromPart responsible for display
  the contents of this chunk; QUERY_ROW_OFFSET is the offset within
  QUERY's result set at which display of the chunk begins;
  DISPLAY_ROW_OFFSET is the display offset at which display of this
  chunk begins. Callers can use this information to compute the query
  row for any display row.

  GET_CHILDREN is a function from node instances to a mapping from
  child offset to child node instance.
  """
  return FrozenIntRangeDict(_gen_map_contents(node, get_children))

class QueryPage(ExplicitInheritance):
  """Query result page"""
  loading = True

  @override
  def __init__(self, array):
    assert isinstance(array, np.ndarray)
    self.__array = array

  def complete_load(self, array):
    """Mark a QueryPage as having finished loading"""
    assert self.loading
    assert isinstance(array, np.ndarray)
    self.__array = array
    self.loading = False

  def __getitem__(self, key):
    return self.__array[key]

  @override
  def __str__(self):
    if self.loading:
      return "<QP_LOADING>"
    return str(self.__array)

@final
class SqlBundlePageCache(dict, ExplicitInheritance):
  """Holds references to all query pages for a single display page"""

  @override
  def __init__(self, localcc):
    super().__init__()
    self.__localcc = the(PivotQueryEngine, localcc)

  def __missing__(self, query_bundle_and_page_number):
    self[query_bundle_and_page_number] = \
      page = self.__localcc.load_query_page(query_bundle_and_page_number)
    return page

  def dump_to_cg(self, cg):
    """Dump to a dctv.util CodeGenerator CG and return CG"""
    query_pages_by_query = defaultdict(set)
    for query_bundle, page_number in self:
      query_pages_by_query[query_bundle].add(page_number)
    for query_bundle in sorted(query_pages_by_query):
      with cg.indent("{}", query_bundle):
        cg("Referenced query pages: {}", sorted(query_pages_by_query[query_bundle]))
    return cg

  @override
  def __str__(self):
    return self.dump_to_cg(CodeGenerator()).get_source()

@final
class DisplayPageCache(dict, ExplicitInheritance):
  """Top of query cache tree

  Maps display pages to a mapping from (QUERY_BUNDLE, QUERY_PAGE) to a
  QueryPage.  The only strong references to query cache pages are from
  the by-display-page dictionary; this way, as we scroll, and we drop
  references to display pages, we automatically clean up unreferenced
  query pages.
  """

  @override
  def __init__(self, localcc):
    super().__init__()
    self.__localcc = the(PivotQueryEngine, localcc)

  def __missing__(self, display_page_number):
    self[display_page_number] = qbc = SqlBundlePageCache(self.__localcc)
    return qbc

  def dump_to_cg(self, cg):
    """Dump to a dctv.util CodeGenerator CG and return CG"""
    with cg.indent("DisplayPageCache ({} pages):", len(self)):
      for page_number in sorted(self):
        with cg.indent("Display page {}:", page_number):
          self[page_number].dump_to_cg(cg)
    return cg

  @override
  def __str__(self):
    return self.dump_to_cg(CodeGenerator()).get_source()

class InvalidPivotModelOperation(Exception):
  """Exception for invalid pivot model operations"""

class PivotModel(ExplicitInheritance):
  """Manages a tree of PivotNode objects"""

  @override
  def __init__(self):
    self.__plugged = 0
    self.__need_emit_changed = False
    self.__on_change_callback = ignore
    self.__root = RootPivotNode()
    self.__info = {self.__root: self._NodeInfo(None, None)}

  class _NodeInfo:
    def __init__(self, parent, index_in_parent):
      self.parent = parent
      self.index_in_parent = index_in_parent
      self.children = SortedDict()

  # Using this class as a context manager plugs and unplugs.

  def __enter__(self):
    self.plug()

  def __exit__(self, _exc_type, _exc_value, _traceback):
    self.unplug()

  @final
  def set_on_change_callback(self, callback):
    """Set the after-change callback"""
    assert callable(callback)
    self.__on_change_callback = callback

  @final
  def plug(self):
    """Temporarily prevent update notifications.

    Balance with unplug().
    """
    if not self.__plugged:
      assert not self.__need_emit_changed
    self.__plugged += 1

  def unplug(self):
    """Un-prevent update notifications.

    Balance with plug().
    """

    assert self.__plugged >= 1
    self.__plugged -= 1
    if not self.__plugged and self.__need_emit_changed:
      self.__need_emit_changed = False
      self.__on_change_callback()

  @property
  def plugged(self):
    """The plug count of this model"""
    return self.__plugged

  def _mark_changed(self):
    assert self.__plugged
    self.__need_emit_changed = True

  def _insert_node(self, child_node, parent_node, index):
    """Low-level pivot node addition"""
    assert self.__plugged
    assert isinstance(child_node, PivotNode)
    assert isinstance(parent_node, PivotNode)
    assert isinstance(index, int)
    assert child_node not in self.__info
    assert 0 <= index <= parent_node.number_rows
    parent_children = self.__info[parent_node].children
    existing_child = parent_children.get(index)
    if existing_child:
      self.__remove_node(existing_child)
    parent_children[index] = child_node
    self.__info[child_node] = self._NodeInfo(parent_node, index)

  def _remove_childless_node(self, pivot_node):
    assert not self.__info[pivot_node].children
    info = self.__info[pivot_node]
    del self.__info[pivot_node]
    del self.__info[info.parent].children[info.index_in_parent]

  def __remove_node(self, pivot_node):
    """Low-level pivot node removal"""
    assert self.__plugged
    for child in tuple(self.__info[pivot_node].children.values()):
      self.__remove_node(child)
    self._remove_childless_node(pivot_node)

  def __get_info(self, pivot_node):
    try:
      return self.__info[pivot_node]
    except KeyError:
      raise InvalidPivotModelOperation("node not in tree") from None

  @property
  def root(self):
    """The top-level PivotNode"""
    return self.__root

  @final
  def has_node_p(self, pivot_node):
    """Return whether PIVOT_NODE is in the tree"""
    return pivot_node in self.__info

  @final
  def get_parent(self, pivot_node):
    """Return the parent of PIVOT_JODE"""
    return self.__get_info(pivot_node).parent

  @final
  def get_index(self, pivot_node):
    """Return the row at which we inserted PIVOT_NODE in its parent"""
    return self.__get_info(pivot_node).index_in_parent

  @final
  def get_children(self, pivot_node):
    """Return an iterator over children of PIVOT_NODE

    Do not modify the tree while iterating.
    """
    return self.__get_info(pivot_node).children.values()

  @final
  def get_children_full(self, pivot_node):
    """Return iterator over (INDEX, CHILD) of the children of PIVOT_NODE

    Do not modify the tree iterating.
    """
    return self.__get_info(pivot_node).children.items()

  @final
  def generate_subtree(self, pivot_node):
    """Generate all children of PIVOT_NODE including PIVOT_NODE

    Do not modify the tree while iterating.
    """
    yield pivot_node
    for child in self.get_children(pivot_node):
      yield from self.generate_subtree(child)

  @final
  def insert_node(self, child_node, parent_node, index):
    """Add a node to the tree under an existing node.

    PARENT_NODE is the node to add. INDEX is the location in
    PARENT_NODE at which to add the new node. CHILD_NODE is the node
    to add.  """
    if self.has_node_p(child_node):
      raise InvalidPivotModelOperation("node already in tree")
    if not self.has_node_p(parent_node):
      raise InvalidPivotModelOperation("parent not already in tree")
    if not 0 <= index <= parent_node.number_rows:
      raise InvalidPivotModelOperation("insertion index out of bounds")
    with self:
      self._insert_node(child_node, parent_node, index)
      self._mark_changed()
    return child_node

  @final
  def remove_node(self, pivot_node):
    """Remove PIVOT_NODE from tree.

    All of PIVOT_NODE's children will also be removed.
    """
    if not self.has_node_p(pivot_node):
      raise InvalidPivotModelOperation("node not in tree")
    if pivot_node is self.root:
      raise InvalidPivotModelOperation("cannot remove root node")
    with self:
      self.__remove_node(pivot_node)
      self._mark_changed()

  def maybe_get_child_at(self, parent_node, index):
    """Maybe retrieve a child.

    Return the child at INDEX in PARENT_NODE (which must be in the
    tree) if it exists or None if PARENT_NODE has no child at INDEX.
    """
    return self.__get_info(parent_node).children.get(index)

  def maybe_remove_child_at(self, parent_node, index):
    """Maybe remove a child node.

    If the tree contains PARENT_NODE and PARENT_NODE has a child at
    INDEX, remove that child and return true.  Otherwise,
    return false.
    """
    if self.has_node_p(parent_node):
      child_node = self.maybe_get_child_at(parent_node, index)
      if child_node:
        self.remove_node(child_node)
        return True
    return False

  @final
  def remove_nodes_if(self, predicate):
    """Remove all nodes matching PREDICATE

    PREDICATE is a function of one argument, a PivotNode; it returns
    a boolean indicating whether to remove that PivotNode.
    """
    with self:
      doomed_nodes = [
        pivot_node for pivot_node in self.__info
        if pivot_node is not self.__root and predicate(pivot_node)
      ]
      for pivot_node in doomed_nodes:
        if self.has_node_p(pivot_node):
          self.remove_node(pivot_node)

@final
class _FuturePivotNode(IdentityHashedImmutable):
  future = iattr(ChainableFuture, kwonly=True)
  parent = iattr(ChainableFuture, kwonly=True)
  index = iattr(ChainableFuture, kwonly=True)
  type = iattr(type, kwonly=True)
  attributes = iattr(dict, kwonly=True)
  sequence_number = iattr(int, kwonly=True)

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    assert issubclass(self.type, PivotNode)
    assert assert_map_type(dict, str, ChainableFuture, self.attributes)

  @property
  def all_futures(self):
    """All futures we depend on"""
    futures = (self.parent, self.index) + \
      tuple(self.attributes.values())
    assert assert_seq_type(tuple, ChainableFuture, futures)
    return futures

class StaleNodeError(InvalidPivotModelOperation):
  """Exception raised after a stale insertion in an AsynchronousPivotModel"""

@final
class AsynchronousPivotModelInfo(ExplicitInheritance):
  """State that AsynchronousPivotModel needs for deferred operations"""

  @override
  def __init__(self, model, auto_cancel):
    self.__model = the(AsynchronousPivotModel, model)
    self.__auto_cancel = auto_cancel
    self.__clear_memo = {model.root: 0}
    self.__insert_memo = {}
    self.__need_cancellation_scan = False
    self.__pending_inserts = set()
    self.__ready_inserts = set()
    self.__sequence_number = count(1)
    self.__sequence_number_override = None

  def on_unplug(self):
    """Called by AsynchronousPivotModel just before unplug"""
    while self.__need_cancellation_scan or self.__ready_inserts:
      self.__flush_ready_inserts()
      self.__do_cancellation_scan()

  def __gen_sequence_number(self):
    return next(self.__sequence_number)

  def __look_up_sequence_number_for_insert(self, parent_node, index):
    sequence_number = max(self.__insert_memo.get((parent_node, index), 0),
                          self.__clear_memo.get(parent_node, 0))
    # If parent_node actually has a child at index, the active insert
    # operation (our caller) will delete that child and all its
    # children by side effect, so in this case, the correct sequence
    # number to use for the staleness check is the maximum sequence
    # number from the node's subtree.
    clobbered = self.__model.has_node_p(parent_node) and \
      self.__model.maybe_get_child_at(parent_node, index)
    if clobbered:
      for child_index, _child_node in \
          self.__model.get_children_full(clobbered):
        child_sequence_number = \
          self.__look_up_sequence_number_for_insert(clobbered, child_index)
        sequence_number = max(sequence_number, child_sequence_number)
    return sequence_number

  def cancel_pending_inserts_at(self, parent_node, index):
    """Record staleness information"""
    assert self.__model.plugged
    sequence = self.__sequence_number_override or self.__gen_sequence_number()

    def _xcancel(dlog, key, sequence):
      assert dlog.get(key, 0) <= sequence
      dlog[key] = sequence

    _xcancel(self.__insert_memo, (parent_node, index), sequence)
    child_node = self.__model.maybe_get_child_at(parent_node, index)
    if child_node:
      _xcancel(self.__clear_memo, child_node, sequence)
    if self.__auto_cancel:
      self.__need_cancellation_scan = True

  def __do_cancellation_scan_1(self, pending_insert):
    if pending_insert in self.__ready_inserts:
      return  # No point in canceling it now
    assert not (pending_insert.future.done() and
                pending_insert in self.__pending_inserts), \
                "pi {} shouldn't be done".format(id(pending_insert.future))
    parent_node = future_result_now(pending_insert.parent)
    if not parent_node:
      return  # Can't check yet
    index = future_result_now(pending_insert.index)
    if index is None:
      old_sequence_number = self.__clear_memo.get(parent_node, 0)
    else:
      old_sequence_number = \
        self.__look_up_sequence_number_for_insert(parent_node, index)
    if old_sequence_number > pending_insert.sequence_number:
      # Setting the pending_insert.future to an error state will
      # cancel the internal all_ready_future, which will move this
      # latter future into a completed state and put pending_insert
      # on the ready queue, where unplug will pick it up.  We don't
      # have to, and shouldn't, call
      # self.__remove_pending_insert_from_pending() here!
      pending_insert.future.set_exception(StaleNodeError())

  def __do_cancellation_scan(self):
    assert self.__model.plugged
    if self.__need_cancellation_scan:
      self.__need_cancellation_scan = False
      for pending_insert in tuple(self.__pending_inserts):
        self.__do_cancellation_scan_1(pending_insert)

  def __remove_pending_insert_from_pending(self, pending_insert):
    assert pending_insert.future.done()
    self.__pending_inserts.remove(pending_insert)

  def __finish_pending_insert(self, pending_insert):
    assert pending_insert in self.__pending_inserts
    assert pending_insert not in self.__ready_inserts
    assert not self.__sequence_number_override
    try:
      if not pending_insert.future.done():
        parent_node, index = (pending_insert.parent.result(),
                              pending_insert.index.result())
        old_sequence_number = \
          self.__look_up_sequence_number_for_insert(parent_node, index)
        if old_sequence_number > pending_insert.sequence_number:
          raise StaleNodeError()
        kwargs = valmap(lambda future: future.result(),
                        pending_insert.attributes)
        pivot_node = pending_insert.type(**kwargs)
        self.__sequence_number_override = pending_insert.sequence_number
        self.__model.insert_node(pivot_node, parent_node, index)
        self.__sequence_number_override = None
        pending_insert.future.set_result(pivot_node)
    except Exception as ex:
      pending_insert.future.set_exception(ex)
    finally:
      self.__sequence_number_override = None
      self.__remove_pending_insert_from_pending(pending_insert)

  def __flush_ready_inserts(self):
    assert self.__model.plugged
    while self.__ready_inserts:
      self.__finish_pending_insert(self.__ready_inserts.pop())

  def __on_pending_insert_ready(self, all_ready_future, pending_insert):
    assert all_ready_future.done()
    assert pending_insert not in self.__ready_inserts
    with self.__model:
      if pending_insert not in self.__pending_inserts:
        pending_insert.future.set_exception(StaleNodeError())
      elif future_exception(all_ready_future):
        pending_insert.future.set_exception(future_exception(all_ready_future))
        self.__remove_pending_insert_from_pending(pending_insert)
      else:
        self.__ready_inserts.add(pending_insert)
        # Unplug will deal with newly-ready inserts by side effect

  def __queue_cancellation_scan(self):
    with self.__model:
      self.__need_cancellation_scan = True

  @property
  def number_pending_inserts(self):
    """Number of ongoing insertions"""
    return len(self.__pending_inserts)

  def defer_insert(self,
                   parent_node,
                   index,
                   pivot_node_type,
                   attribute_futures):
    """See AsynchronousPivotModel.defer_insert"""
    pending_insert = _FuturePivotNode(
      future=ChainableFuture(),
      parent=parent_node,
      index=index,
      type=pivot_node_type,
      attributes=attribute_futures,
      sequence_number=self.__gen_sequence_number(),
    )
    self.__pending_inserts.add(pending_insert)
    if self.__auto_cancel:
      # Wire up parent_and_index_ready_future before the just-parent
      # future so that we avoid a redundant scan.
      parent_and_index_ready_future = AllCompleteFuture(
        (pending_insert.parent, pending_insert.index))
      def _on_parent_and_index_ready(_future):
        self.__queue_cancellation_scan()
      parent_and_index_ready_future.add_done_callback(
        _on_parent_and_index_ready)
      def _on_parent_ready(_future):
        if not pending_insert.index.done():
          self.__queue_cancellation_scan()
      pending_insert.parent.add_done_callback(_on_parent_ready)

    all_ready_future = AllCompleteFuture(pending_insert.all_futures)
    def _on_all_ready(future):
      assert future is all_ready_future
      self.__on_pending_insert_ready(all_ready_future, pending_insert)
    # N.B. the callback might run synchronously inside
    # add_done_callback, so make sure state is consistent here.
    all_ready_future.add_done_callback(_on_all_ready)

    pivot_node_future = pending_insert.future
    def _on_pivot_node_future_done(future):
      if future_exception(future):
        all_ready_future.cancel()
      else:
        assert all_ready_future.done()
    pivot_node_future.add_done_callback(_on_pivot_node_future_done)
    return pivot_node_future

  def abort_all(self):
    """Force cancel all pending inserts"""
    assert self.__model.plugged
    for pending_insert in tuple(self.__pending_inserts):
      if pending_insert not in self.__ready_inserts:
        pending_insert.future.set_exception(
          InvalidPivotModelOperation("asynchronous inserts cancelled"))

@final
class AsynchronousPivotModel(PivotModel):
  """A pivot tree model that supports asynchronous operations"""

  @override
  def __init__(self):
    super().__init__()
    self.__async_info = None
    self.__auto_cancel = True

  @property
  def auto_cancel(self):
    """Whether we try to cancel insertions early on conflict"""
    return self.__auto_cancel

  @auto_cancel.setter
  def auto_cancel(self, enabled):
    self.__auto_cancel = bool(enabled)

  @override
  def unplug(self):
    if self.plugged == 1 and self.__async_info:
      self.__async_info.on_unplug()
      if not self.__async_info.number_pending_inserts:
        self.__async_info = None
    super().unplug()

  @override
  def _insert_node(self, child_node, parent_node, index):
    super()._insert_node(child_node, parent_node, index)
    if self.__async_info:
      self.__async_info.cancel_pending_inserts_at(parent_node, index)

  @override
  def _remove_childless_node(self, pivot_node):
    if self.__async_info:
      parent_node = self.get_parent(pivot_node)
      index = self.get_index(pivot_node)
      self.__async_info.cancel_pending_inserts_at(parent_node, index)
    super()._remove_childless_node(pivot_node)

  @override
  def maybe_remove_child_at(self, parent_node, index):
    removed = super().maybe_remove_child_at(parent_node, index)
    if not removed and self.__async_info and self.has_node_p(parent_node):
      assert not self.maybe_get_child_at(parent_node, index)
      with self:
        self.__async_info.cancel_pending_inserts_at(parent_node, index)
    return removed

  def defer_insert(self,
                   *,
                   parent_node,
                   index,
                   pivot_node_type,
                   **attribute_futures):
    """Perform an insertion of a node, but later

    Insert a PivotNode of type PIVOT_NODE_TYPE into the model at INDEX
    in PARENT_NODE, passing ATTRIBUTE_FUTURES as the keyword arguments
    to the PIVOT_NODE_TYPE constructor.

    Return a future that yields the actual constructed node.

    PARENT_NODE, INDEX, and the ATTRIBUTE_FUTURES values are all
    ChainableFuture objects, not literal values.
    """
    if not self.__async_info:
      self.__async_info = \
        AsynchronousPivotModelInfo(self, self.__auto_cancel)
    return self.__async_info.defer_insert(
      parent_node, index, pivot_node_type, attribute_futures)

  def abort_all(self):
    """Cancel all outstanding async insert operations"""
    if self.__async_info:
      with self:
        self.__async_info.abort_all()

  @property
  def number_pending_inserts(self):
    """Number of pending insert operations"""
    return 0 if not self.__async_info else \
      self.__async_info.number_pending_inserts

class QueryEngine(ExplicitInheritance):
  """Interface for the query engine that the pivot table uses"""

  @abstract
  def query_async(self, query_bundle):
    """Issue a query asynchronously, returning a future.

    The future yields a numpy array.
    """
    raise NotImplementedError("abstract")

  def expedite(self):
    """Issue pending queries now without waiting"""

class QueryEngineDiedException(Exception):
  """Exception set when query engine dies with queries pending"""

class ErrorResult(Immutable, Placeholder):
  """Wrapper for exceptions in query results"""
  exception = iattr(BaseException)

  # TODO(dancol): add detail view allowing user to see the exception

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    # TODO(dancol): remove the console logging once we get an error
    # tooltip or something.
    log.warning("query failed with %s", self.exception)

  @override
  def __str__(self):
    return "wtf"

def _make_asyncio_timeout_future(timeout):
  return asyncio.ensure_future(asyncio.sleep(timeout))

class AsyncQueryEngine(QueryEngine):
  """Manages query pipelining and coalescing"""

  __query_gathering_window_ms = 50
  __max_concurrent_batches = 2

  @override
  def __init__(self,
               batch_query_async_function,
               *,
               make_timeout_future=_make_asyncio_timeout_future):
    # TODO(dancol): make_timeout_future should be a coroutine
    super().__init__()
    assert callable(make_timeout_future)
    self.__batch_query_async_function = batch_query_async_function
    self.__make_timeout_future = make_timeout_future
    self.__timeout_future = None
    self.__next_batch = []
    self.__pending_batch_futures = set()

  def __start_query_gathering_timer(self):
    if not self.__timeout_future:
      def _done_callback(future):
        assert future is self.__timeout_future
        self.__timeout_future = None
        if not future.cancelled():
          self.__issue_pending_lookups()
      self.__timeout_future = \
        self.__make_timeout_future(self.__query_gathering_window_ms / 1000.0)
      self.__timeout_future.add_done_callback(_done_callback)

  def __stop_query_timer(self):
    if self.__timeout_future:
      self.__timeout_future.cancel()
      assert not self.__timeout_future

  @override
  def expedite(self):
    super().expedite()
    self.__stop_query_timer()
    self.__issue_pending_lookups()

  @staticmethod
  def __on_raw_query_future_done(raw_query_future):
    query_future = raw_query_future.__query_future  # pylint: disable=protected-access
    ex = future_exception(raw_query_future)
    if ex:
      query_future.set_exception(ex)
    else:
      query_future.set_result(raw_query_future.result())

  def __on_batch_future_done(self,
                             raw_query_futures,
                             query_futures,
                             batch_future):
    if future_exception(batch_future):
      ex = future_exception(batch_future)
      for query_future in query_futures:
        query_future.set_exception(ex)
    else:
      for query_future in query_futures:
        if not query_future.done():
          log.warning(
            "batch future completed successfully without resolving query")
          query_future.set_exception(RuntimeError("query protocol violation"))
    for raw_query_future in raw_query_futures:
      raw_query_future.cancel()
    self.__pending_batch_futures.remove(batch_future)
    self.__issue_pending_lookups()

  def __issue_pending_lookups(self):
    if len(self.__pending_batch_futures) >= self.__max_concurrent_batches:
      return  # Will issue lookups when a batch future completes
    if not self.__next_batch:
      return  # Nothing to do
    query_bundles = []
    query_futures = []
    for query_bundle, query_future in self.__next_batch:
      query_bundles.append(query_bundle)
      query_futures.append(query_future)
    self.__next_batch.clear()
    # pylint: disable=protected-access
    batch_future, raw_query_futures = \
      self.__batch_query_async_function(query_bundles)
    assert len(query_futures) == len(raw_query_futures)
    for query_future, raw_query_future in \
        zip(query_futures, raw_query_futures):
      raw_query_future.__query_future = query_future
      raw_query_future.add_done_callback(self.__on_raw_query_future_done)
    self.__pending_batch_futures.add(batch_future)
    batch_future.add_done_callback(partial(self.__on_batch_future_done,
                                           tuple(raw_query_futures),
                                           tuple(query_futures)))

  @override
  def query_async(self, query_bundle):
    """Issue a query asynchronously, returning a future.

    Queries issuance to the underlying engine is batched and
    concurrent query load is limited by an internal factor.
    Nevertheless, the returned future acts normally.
    """
    assert isinstance(query_bundle, SqlBundle)
    future = ChainableFuture()
    # TODO(dancol): support cancellation
    future.set_running_or_notify_cancel()
    next_batch = self.__next_batch
    if next_batch is None:
      future.set_exception(QueryEngineDiedException())
    else:
      next_batch.append((query_bundle, future))
      self.__start_query_gathering_timer()
    return future

  def close(self):
    """Shut down the query engine; fail all futures"""
    if self.__next_batch is not None:
      self.__stop_query_timer()
      ex = QueryEngineDiedException()
      for _, query_future in self.__next_batch:
        query_future.set_exception(ex)
      self.__next_batch.clear()
      self.__next_batch = None
      for batch_future in tuple(self.__pending_batch_futures):
        batch_future.set_exception(ex)
      assert not self.__pending_batch_futures
      self.__batch_query_async_function = None
      self.__make_timeout_future = None

class PivotQueryEngine(GObject.GObject):
  """Manages asynchronously fetching data"""
  __gtype_name = "PivotQueryEngine"
  __gsignals__ = {
    "lookup-complete": (GObject.SignalFlags.RUN_FIRST, None, ()),
    "row-map-changed": (GObject.SignalFlags.RUN_FIRST, None, ()),
  }

  def __init__(self, *, query_engine, page_size=8192):
    super().__init__()

    assert the(int, page_size) >= 1
    self.__page_size = page_size

    # Data model
    self.__model = None
    self.__row_map = None
    self.model = AsynchronousPivotModel()

    # Caches
    self.__display_page_cache = DisplayPageCache(self)
    self.__weak_query_cache = WeakValueDictionary()

    # TODO(dancol): cap the size of the local integer cache
    self.__int_query_cache = {}

    self.__loading_placeholder_array = np.full(self.__page_size, LOADING)

    # Communication with the database backend
    self.__query_engine = the(QueryEngine, query_engine)
    self.__self_wr = weak_ref(self)

  @property
  def model(self):
    """The current model that this engine consults"""
    return self.__model

  @model.setter
  def model(self, model):
    old_model = self.__model
    if old_model:
      old_model.set_on_change_callback(ignore)
    if model:
      assert isinstance(model, AsynchronousPivotModel)
      model.set_on_change_callback(self.__on_model_changed)
    self.__model = model
    self.__on_model_changed()

  def __on_model_changed(self):
    if self.__model:
      self.__row_map = make_display_row_map(
        self.__model.root,
        self.__model.get_children_full)
      self.emit("row-map-changed")

  def query_int_async(self, query_bundle):
    """Query a scalar"""
    cache = self.__int_query_cache
    # TODO(dancol): cancel when unneeded?
    future = cache.get(query_bundle)
    if not future:
      future = self.__query_engine \
                   .query_async(query_bundle) \
                   .then(lambda v: int(v[0]))
      # TODO(dancol): fix cancellation
      future.set_running_or_notify_cancel()
      cache[query_bundle] = future
    return future

  def lookup_query_bundle_row(self,
                              query_bundle,
                              display_row_number,
                              query_row_number):
    """Look up a specific row of a query, possibly from cache.

    QUERY_BUNDLE is a query bundle of the kind SqlFromPart makes.
    QUERY_ROW_NUMBER is the specific row in QUERY's result set to
    return.  DISPLAY_ROW_NUMBER is the display row number for which
    we're making this query; it's used for cache pruning on scroll.

    Return a scalar result value or NO_DATA if the datum requested is
    not yet available.  In this latter case, schedule a query
    asynchronously.
    """
    assert isinstance(query_bundle, SqlBundle)
    assert the(int, query_row_number) >= 0
    assert the(int, display_row_number) >= 0

    page_size = self.__page_size
    display_page_number = display_row_number // page_size
    query_page_number = query_row_number // page_size
    row_number_in_query_page = query_row_number % page_size

    return self.__display_page_cache \
      [display_page_number] \
      [(query_bundle, query_page_number)] \
      [row_number_in_query_page]

  def debug_lookup_query_page(self, query_bundle,
                              display_row_number,
                              query_row_number):
    """For debugging and testing only: return QueryPage"""
    page_size = self.__page_size
    display_page_number = display_row_number // page_size
    query_page_number = query_row_number // page_size
    return self.__display_page_cache \
      [display_page_number] \
      [(query_bundle, query_page_number)] \

  def lookup_display_row_value(self, display_row_number, ca):
    """Look up the value for a column and display row

    Ordinarily, you should go through the PivotNode API.
    This function supports tests and general experimentation.
    """
    pivot_node, query_row_number = self.map_display_row(display_row_number)
    return pivot_node.lookup_column(self,
                                    display_row_number,
                                    query_row_number,
                                    ca)

  @property
  def total_display_rows(self):
    """Number of display rows across all queries and children"""
    return len(self.__row_map)

  def map_display_row(self, display_row_number):
    """Find the query node backing a display row

    DISPLAY_ROW_NUMBER is a display row number. Return a tuple
    (PIVOT_NODE, QUERY_ROW_NUMBER), where PIVOT_NODE is the PivotNode
    backing the input display node and QUERY_ROW_NUMBER is the query
    row number backing that query.

    If DISPLAY_ROW_NUMBER is out of range, raise KeyError.
    """
    pivot_node, chunk_query_start, chunk_display_start = (
      self.__row_map[display_row_number])
    row_offset = display_row_number - chunk_display_start
    return pivot_node, chunk_query_start + row_offset

  def load_query_page(self, query_bundle_and_page_number):
    """Return the QueryPage for a specific bundle and query.

    If the query page isn't available, start the load process
    asynchronously and return a QueryPage full of placeholders.

    The QueryPage is only weakly held, so the caller is responsible
    for keeping it alive (e.g., by insertion in some other cache).
    """
    page = self.__weak_query_cache.get(query_bundle_and_page_number)
    if not page:
      self.__weak_query_cache[query_bundle_and_page_number] = \
        page = \
        QueryPage(self.__loading_placeholder_array)
      self.__issue_async_page_query(query_bundle_and_page_number)
    return page

  def __issue_async_page_query(self, query_bundle_and_page_number):
    # pylint: disable=protected-access
    query_bundle, page_number = query_bundle_and_page_number
    self_wr = self.__self_wr
    def _on_done(completed_future):
      self_sr = self_wr()
      if self_sr:
        self_sr.__on_async_page_query_complete(
          query_bundle_and_page_number, completed_future)
      else:
        log.debug("async page query outlived PivotQueryEngine: ignoring")
    paginated_query_bundle = query_bundle.paginate(
      self.__page_size, page_number)
    future = self.__query_engine.query_async(paginated_query_bundle)
    future.add_done_callback(_on_done)
    # TODO(dancol): cancel when unneeded?

  def __on_async_page_query_complete(self,
                                     query_bundle_and_page_number,
                                     completed_future):
    page = self.__weak_query_cache.get(query_bundle_and_page_number)
    if not page:
      log.debug("stale query completed: ignoring")
      return
    if not page.loading:
      log.debug("multiple query completions for page: ignoring")
      return
    if future_exception(completed_future):
      array = np.full(self.__page_size,
                      ErrorResult(future_exception(completed_future)),
                      dtype="O")
    else:
      array = completed_future.result()
    # TODO(dancol): include invalidated display rows in emitted signal
    # so we don't have to redraw the whole table
    page.complete_load(array)
    self.emit("lookup-complete")

  @property
  def cached_display_pages(self):
    """Set of display pages for which we currently have a cache"""
    return frozenset(self.__display_page_cache)

  def keep_display_pages(self, pages_to_keep):
    """Discard all but given display page caches

    This function is useful when scrolling.  Pass it the
    currently-visible display pages and this function will discard
    the rest.
    """
    for page in self.cached_display_pages - frozenset(pages_to_keep):
      del self.__display_page_cache[page]

  def close(self):
    """Close this query engine"""
    if self.model:
      self.model.abort_all()
      self.model = None
    self.__display_page_cache = None  # Break circular references

  def __dump_weak_query_cache_to_cg(self, cg):
    qc = dict(self.__weak_query_cache)
    with cg.indent("Query cache ({} queries; referenced query page numbers by query):", len(qc)):
      pages_by_query = defaultdict(dict)
      for (query_bundle, page_number), page in qc.items():
        assert page_number not in pages_by_query[query_bundle]
        pages_by_query[query_bundle][page_number] = page
      for query_bundle in sorted(pages_by_query):
        with cg.indent("{}", query_bundle):
          page_by_number = pages_by_query[query_bundle]
          for page_number in sorted(page_by_number):
            cg("{:>5}: {}", page_number, page_by_number[page_number])

  def dump_to_cg(self, cg):
    """Dump to a dctv.util CodeGenerator CG and return CG"""
    with cg.indent("PivotQueryEngine (page_size={}):", self.__page_size):
      self.__display_page_cache.dump_to_cg(cg)
      self.__dump_weak_query_cache_to_cg(cg)
    return cg

  @override
  def __str__(self):
    return self.dump_to_cg(CodeGenerator()).get_source()
