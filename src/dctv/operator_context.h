// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <boost/intrusive/set.hpp>
#include <boost/intrusive/slist.hpp>
#include <functional>
#include <utility>

#include "map.h"
#include "pyutil.h"
#include "query.h"
#include "query_key.h"
#include "vector.h"
#include "perf.h"

namespace dctv {

struct QueryExecution;
struct OperatorContext;
struct InputChannel;
struct OutputChannel;

// The oplink namespace includes boost::intrusive in whole so we can
// avoid excessive typing while also not polluting the dctv namespace
// in a header.
namespace oplink {
using namespace boost::intrusive;
using ReScoreQueueLink = slist_member_hook<>;
using RunQueueLink = set_member_hook<optimize_size<true>>;
}  // namespace oplink

struct QueryDb {
  Map<QueryKey, OutputChannel*> producers;
};

struct OperatorContext final : BasePyObject, SupportsGcClear {
  enum class LinkState {
    ORPHANED,
    ON_RUN_QUEUE_VALID_SCORE,
    ON_RUN_QUEUE_NEED_RESCORE,
  };

  // N.B. The link_* members are modified exclusively by QueryExecution!
  oplink::RunQueueLink link_run_queue;
  oplink::ReScoreQueueLink link_re_score_queue;
  LinkState link_state = LinkState::ORPHANED;
  // Owner when state != ORPHANED
  QueryExecution* link_qe = nullptr;
  // Reference to self when state != ORPHANED
  unique_op_ref link_ref;
  // Valid score when state != ORPHANED
  Score link_score;

  // Transform Python reference to OperatorContext and wire up the new
  // OperatorContext into the query database.
  static void install(QueryExecution* qe,
                      int ordinal,
                      unique_pyref action,
                      QueryDb* query_db);

  // Execute the current action request and pump the coroutine for the
  // next request.
  unique_pyref turn_crank();

  // Call whenever the score might have changed.
  void invalidate_score() noexcept;

  // Actually recompute the score.  Doesn't change queue position or a
  // consult a cache: just performs the score computation and returns
  // the score.
  Score compute_score() const;

  inline unique_pyref get_query_action() const noexcept;
  Vector<QueryKey> get_declared_inputs() const;
  Vector<QueryKey> get_declared_outputs() const;

  // Signal that we may want to close the operator early because it's
  // not connected to anything.
  void on_output_channel_disconnected() noexcept;

  Score compute_basic_score() const;
  void remove_self() noexcept;
  void communicate(unique_pyref reply_to_prev_qe_call);

  inline npy_intp get_block_size() const noexcept;

  // Force a shutdown of this operator.
  void close() noexcept;

  // Called by OutputChannel to indicate that this operator context
  // has a deferred buffer resize waiting.
  void queue_pending_block_size_change();

  void make_buffer_resize_ops(Vector<IoSpec>* io_specs) const;

  void flush_perf_to_qe();

  // Needed for slamming all channels shut.
  ~OperatorContext();

  inline QueryExecution* get_qe() const;

  template<typename Functor>
  inline void enumerate_input_channels(Functor&& functor);

  // GC support
  int py_traverse(visitproc visit, void* arg) const noexcept;
  int py_clear() noexcept;

  static PyTypeObject pytype;

 private:
  friend struct QeCallSetup;

  void check_current_request() const;
  void send_current_exception();
  void communicate_1(std::pair<GenRet, unique_pyref> gen_ret);
  void reset_coroutine_state() noexcept;
  void run_setup(QueryExecution* qe, QueryDb* query_db);

  npy_intp block_size;
  int ordinal = 0;
  bool precious;
  mutable bool need_buffer_size_update = false;
  unique_pyref action;
  unique_pyref coroutine;
  unique_pyref send_function;

  mutable std::unique_ptr<QeCall> current_request;
  Vector<unique_obj_pyref<InputChannel>> input_channels;
  Vector<unique_obj_pyref<OutputChannel>> output_channels;

  perf::Sample accumulated_perf;
};

void init_operator_context(pyref m);

}  // namespace dctv

#include "operator_context-inl.h"
