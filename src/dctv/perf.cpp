// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "perf.h"

#include <time.h>

#include "pyseq.h"
#include "automethod.h"

namespace dctv {

static
uint64_t
clock_gettime_ns(int clk_id)
{
  struct timespec ts;
  if (clock_gettime(CLOCK_MONOTONIC, &ts))
    abort();
  uint64_t monotonic_ns = static_cast<uint64_t>(ts.tv_nsec);
  monotonic_ns += static_cast<uint64_t>(ts.tv_sec) * 1000000000UL;
  return monotonic_ns;
}

namespace perf {

Sampler::Sampler()
{
  // TODO(dancol): perf_event_open
}

Sample
Sampler::sample() const
{
  Sample sample;
  sample.monotonic_ns = clock_gettime_ns(CLOCK_MONOTONIC);
  return sample;
}

unique_pyref
Sample::to_py() const
{
  return pytuple::of(
      pytuple::of(make_pystr("monotonic_ns"),
                  make_pylong(this->monotonic_ns))

  );
}

}  // namespace perf

static PyMethodDef functions[] = {
  AUTOFUNCTION(clock_gettime_ns,
               "Get the specified clock value in nanoseconds",
               (int, clk_id)
  ),
  { 0 },
};

void
init_perf(pyref m)
{
  register_functions(m, functions);
}

}  // namespace dctv
