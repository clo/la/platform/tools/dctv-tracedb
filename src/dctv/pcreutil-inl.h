// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename T>
T
get_pcre2_info(int info, const pcre2_code* cpat)
{
  T result;
  int rc = pcre2_pattern_info(cpat, info, &result);
  if (rc < 0)
    throw_pcre2_error_nogil(rc, "pcre2_pattern_info");
  return result;
}

}  // namespace dctv
