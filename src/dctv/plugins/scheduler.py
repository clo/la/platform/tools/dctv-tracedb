# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Linux scheduler events"""
from ..event_parser import (
  CPU_NUMBER_TYPE,
  EventSchemaBuilder as esb,
  RAW_TID_TYPE,
)

def _make_wakeup_schema(name):
  return (esb(name,
              (r"comm=(?P<woken_comm>[^\n]*) "
               r"pid=(?P<woken_tid>[0-9]+) "
               r"prio=(?P<prio>[0-9]+) "
               r"target_cpu=(?P<target_cpu>[0-9]+)"))
          .column("woken_comm", "s")
          .column("woken_tid", RAW_TID_TYPE)
          .column("prio", "i")
          .column("target_cpu", CPU_NUMBER_TYPE)
          .build())

def dctv_plugin_get_schemas():
  """Return all schemas that we define"""
  return (
    _make_wakeup_schema("sched_wakeup"),
    _make_wakeup_schema("sched_waking"),
    (esb("sched_switch",
         (r"prev_comm=(?P<prev_comm>[^\n]*) "
          r"prev_pid=(?P<prev_tid>[0-9]+) "
          r"prev_prio=(?P<prev_prio>[0-9]+) "
          r"prev_state=(?P<prev_state>[SDTtZXxKWPNRI|]+\+?) "
          r"==> "
          r"next_comm=(?P<next_comm>[^\n]*) "
          r"next_pid=(?P<next_tid>[0-9]+) "
          r"next_prio=(?P<next_prio>[0-9]+)"))
     .column("prev_comm", "s")
     .column("prev_tid", RAW_TID_TYPE)
     .column("prev_prio", "i")
     .column("prev_state", "task_state")
     .column("next_comm", "s")
     .column("next_tid", RAW_TID_TYPE)
     .column("next_prio", "i")
     .build()),
    (esb("cpu_frequency",
         r"state=(?P<state>[0-9]+) "
         r"cpu_id=(?P<target_cpu>[0-9]+)")
     .column("state", "l", unit="(kilocpu_cycles)/second")
     .column("target_cpu", CPU_NUMBER_TYPE)
     .build()),
    (esb("cpu_idle",
         (r"state=(?P<state>[0-9]+) "
          r"cpu_id=(?P<target_cpu>[0-9]+)"))
     .column("state", "i")
     .column("target_cpu", CPU_NUMBER_TYPE)
     .build()),
    (esb("irq_handler_entry",
         (r"irq=(?P<irq>[0-9]+) "
          r"name=(?P<handler_name>.*)"))
     .column("irq", "i")
     .column("handler_name", "s")
     .build()),
    (esb("irq_handler_exit",
         (r"irq=(?P<irq>[0-9]+) "
          r"ret=(?P<handled>.*)"))
     .column("irq", "i")
     .column("handled", "s")
     .build()),
    (esb("sched_process_exit",
         (r"comm=(?P<comm2>[^\n]*) "
          r"pid=(?P<exiting_tid>[0-9]*) "
          r"prio=(?P<prio2>[0-9]*)"))
     .column("comm2", "ss")
     .column("exiting_tid", RAW_TID_TYPE)
     .column("prio2", "i")
     .build()),
    (esb("task_newtask",
         (r"pid=(?P<new_tid>[0-9]+) "
          r"comm=(?P<comm2>[^\n]*) "
          r"clone_flags=(?P<clone_flags>[0-9a-f]+) "
          r"oom_score_adj=(?P<oom_score_adj>-?[0-9]+)"))
     .column("new_tid", RAW_TID_TYPE)
     .column("comm2", "ss")
     .column("clone_flags", "lx", domain="clone_flags")
     .column("oom_score_adj", "i")
     .build()),
  )

# TODO(dancol): port to stdlib.sql; unhork

# def dctv_plugin_prepare_trace(augment_trace):
#   """Decorate a newly-mounted trace file with useful facilities"""

#   # augment_trace.table(
#   #   ["scheduler", "tid_mapping"],
#   #   """\
#   #   SELECT SPAN * FROM dctv.time_series_to_spans(
#   #     `partition`=>'raw_tid',
#   #     `sources`=>[
#   #       {
#   #         source=>(SELECT * FROM raw_events.task_newtask),
#   #       },
#   #      ],
#   #   """)

#   augment_trace.table(
#     ["scheduler", "timeslices_p_cpu"],
#     """\
#     SELECT SPAN * FROM dctv.time_series_to_spans(
#       `partition`=>'cpu',
#       sources=>[{
#         source=>(SELECT * FROM raw_events.sched_switch),
#         `partition`=>'cpu',
#        }],
#        columns=>[
#          {column=>'comm', source_column=>'next_comm'},
#          {column=>'tid', source_column=>'next_tid'},
#          {column=>'prio', source_column=>'next_prio'}
#        ])
#     """)

#   augment_trace.table(
#     ["scheduler", "raw_frequency_by_cpu"],
#     """\
#     SELECT _ts, state AS frequency, cpu
#     FROM raw_events.cpu_frequency
#     """)

#   augment_trace.table(
#     ["scheduler", "cpufreq_p_cpu"],
#     """\
#     SELECT SPAN * FROM dctv.time_series_to_spans(
#       `partition`=>'cpu',
#       sources=>[{
#         source=>scheduler.raw_frequency_by_cpu,
#         `partition`=>'cpu',
#        }],
#        columns=>['frequency'])
#     """)
