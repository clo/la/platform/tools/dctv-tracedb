// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename TFunctor>
auto
npy_type_dispatch(pyref py_dtype, TFunctor&& functor)
{
  return npy_type_dispatch_code(py_dtype_to_code(py_dtype),
                                AUTOFWD(functor));
}

template<typename TFunctor>
auto
npy_type_dispatch_code(char code, TFunctor&& functor)
{
  switch (code) {
    case '?':
      return AUTOFWD(functor)(bool());
    case 'b':
      return AUTOFWD(functor)(int8_t());
    case 'B':
      return AUTOFWD(functor)(uint8_t());
    case 'h':
      return AUTOFWD(functor)(int16_t());
    case 'H':
      return AUTOFWD(functor)(uint16_t());
    case 'i':
      return AUTOFWD(functor)(int32_t());
    case 'I':
      return AUTOFWD(functor)(uint32_t());
    case 'l':
      return AUTOFWD(functor)(int64_t());
    case 'L':
      return AUTOFWD(functor)(uint64_t());
    case 'q':
      return AUTOFWD(functor)(int64_t());
    case 'Q':
      return AUTOFWD(functor)(uint64_t());
    case 'f':
      return AUTOFWD(functor)(float());
    case 'd':
      return AUTOFWD(functor)(double());
    default:
      _throw_pyerr_fmt(PyExc_TypeError, "unsupported numpy type %d", code);
  }
}

template<typename T>
unique_dtype
type_descr()
{
  if constexpr (std::is_same_v<T, long long> &&
                !std::is_same_v<long long, int64_t>) {
    return type_descr<int64_t>();
  } else {  // NOLINT
    return type_descr_from_type(PyStructForType<T>::nptype);
  }
}

unique_pyarray
make_empty_pyarray(unique_dtype dtype)
{
  return make_uninit_pyarray(std::move(dtype), 0);
}

npy_intp
npy_size(pyarray_ref array) noexcept
{
  return PyArray_SIZE(array.get());  // NOLINT
}

npy_intp npy_size1d(pyarray_ref array) noexcept
{
  assume(npy_ndim(array) == 1);
  return npy_dims(array)[0];
}

dtype_ref
npy_dtype(pyarray_ref array) noexcept
{
  return PyArray_DTYPE(array.get());
}

pyref
npy_base(pyarray_ref array) noexcept
{
  return PyArray_BASE(array.get());
}

int
npy_ndim(pyarray_ref array) noexcept
{
  return PyArray_NDIM(array.get());
}

npy_intp*
npy_dims(pyarray_ref array) noexcept
{
  return PyArray_DIMS(array.get());
}

npy_intp*
npy_strides(pyarray_ref array) noexcept
{
  return PyArray_STRIDES(array.get());
}

void*
npy_data_raw(pyarray_ref array) noexcept
{
  return PyArray_DATA(array.get());
}

template<typename T>
T*
npy_data(pyarray_ref array) noexcept
{
  return reinterpret_cast<T*>(npy_data_raw(array));
}

int
npy_flags(pyarray_ref array) noexcept
{
  return PyArray_FLAGS(array.get());
}

bool
npy_writeable_p(pyarray_ref array) noexcept
{
  return static_cast<bool>(npy_flags(array) & NPY_ARRAY_WRITEABLE);
}

template<typename... Args>
Vector<unique_dtype>
make_dtype_vector()
{
  Vector<unique_dtype> ret;
  ret.reserve(sizeof...(Args));
  (..., ret.push_back(type_descr<Args>()));
  return ret;
}

bool
is_base_ndarray(pyref obj)
{
  return isinstance_exact(obj, pytype_of<PyArrayObject>());
}

pyarray_ref
check_base_ndarray(pyref obj)
{
  assume(obj);
  check_pytype_exact(pytype_of<PyArrayObject>(), obj);
  return obj.as_unsafe<PyArrayObject>();
}

}  // namespace dctv
