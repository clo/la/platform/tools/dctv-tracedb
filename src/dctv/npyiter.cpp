// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "npyiter.h"

namespace dctv {

void
throw_npyiter_error_nogil(const char* errmsg)
{
  with_gil_acquired([&] {
    throw_pyerr_msg(PyExc_ValueError, errmsg);
  });
  DCTV_UNREACHABLE;
}

unique_pyarray
npy_iter_view(NpyIter* iter, int num)
{
  return adopt_check_as_unsafe<PyArrayObject>(
      NpyIter_GetIterView(iter, num));
}

void
init_npyiter(pyref m)
{
  register_constant(m, "DTYPE_INDEX", type_descr<Index>());
}

}  // namespace dctv
