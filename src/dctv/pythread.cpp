// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "pythread.h"

namespace dctv {

// TODO(dancol): avoid touching the mutex on the uncontended path by
// just synchronizing using the GIL.

void
Mutex::lock() noexcept
{
  assert_gil_held();
  if (!this->mutex.try_lock()) {
    with_gil_released([&] {
      this->mutex.lock();
    });
  }
}

void
Mutex::unlock() noexcept
{
  assert_gil_held();
  this->mutex.unlock();
}

void
Condition::notify_one() noexcept
{
  this->condition.notify_one();
}

void
Condition::notify_all() noexcept
{
  this->condition.notify_all();
}

}  // namespace dctv
