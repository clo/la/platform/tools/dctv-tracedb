// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "qe_call_resize_buffers.h"
#include "operator_context.h"

namespace dctv {

QeCallResizeBuffers::QeCallResizeBuffers(
    const OperatorContext* oc,
    std::unique_ptr<QeCall>* self_ptr,
    std::unique_ptr<QeCall> prev_qe_call)
    : self_ptr(self_ptr),
      prev_qe_call(std::move(prev_qe_call))
{
  this->refresh_resize_buffers(oc, self_ptr);
}

void
QeCallResizeBuffers::refresh_resize_buffers(
    const OperatorContext* oc,
    std::unique_ptr<QeCall>* self_ptr)
{
  assert(self_ptr == this->self_ptr);
  // TODO(dancol): do something smarter than refresh the whole list?
  // Probably not worth it unless profiles show this is a hotspot for
  // some reason.
  oc->make_buffer_resize_ops(&this->io_specs);
}

Score
QeCallResizeBuffers::compute_score(const OperatorContext* oc) const
{
  return compute_io_score(oc->compute_basic_score(),
                          this->io_specs.data(),
                          this->io_specs.size());
}

unique_pyref
QeCallResizeBuffers::do_it(OperatorContext* oc)
{
  bool more_to_do =
      do_writes_incremental(this->io_specs.data(),
                            this->io_specs.size());
  if (!more_to_do) {
    // We shouldn't have any non-write IO at this point, so we can just
    // drop our IO specs on the floor.  Where would we deliver
    // the results?
    // N.B. Resetting self_ptr will delete this!
    *this->self_ptr = std::move(this->prev_qe_call);
  }
  oc->invalidate_score();
  return {};
}

int
QeCallResizeBuffers::py_traverse(visitproc visit, void* arg)
    const noexcept
{
  for (const IoSpec& io_spec: this->io_specs)
    if (int ret = io_spec.py_traverse(visit, arg))
      return ret;
  if (int ret = this->prev_qe_call->py_traverse(visit, arg))
    return ret;
  return 0;
}

}  // namespace dctv
