// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <stdint.h>
#include <utility>

#include "npyiter.h"
#include "pyutil.h"
#include "result_buffer.h"
#include "span.h"

namespace dctv {

using CommandType = int64_t;

struct QueryExecution;

template<typename Command>
struct CommandOut final {
  inline CommandOut(unique_pyref cb, QueryExecution* qe);
  template<typename... Args>
  inline void emit(Command command, Args... args);
  void flush();
 private:
  ResultBuffer<CommandType> out;
};

struct CommandIn final {
  explicit inline CommandIn(unique_pyref cb);
  inline bool is_at_eof() const;
  template<typename Operand> inline Operand next();
 private:
  PythonChunkIterator<CommandType> iter;
};

void init_command_stream(pyref m);

}  // namespace dctv

#include "command_stream-inl.h"
