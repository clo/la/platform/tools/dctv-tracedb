// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#include "pyutil.h"
#include "span.h"

namespace dctv {

enum class SpCommand {
  SLURP_INPUT_PARTITION,
  CLEAR_INPUT_PARTITION,
  EMIT_OUTPUT_PARTITION,
  FORGET_OUTPUT_PARTITION,
};

void init_span_pivot(pyref m);

}  // namespace dctv
