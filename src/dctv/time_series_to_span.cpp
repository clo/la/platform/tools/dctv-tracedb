// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "time_series_to_span.h"

#include <tuple>

#include "autoevent.h"
#include "container_util.h"
#include "dctv.h"
#include "hash_table.h"
#include "pyerrfmt.h"
#include "pyiter.h"
#include "pyobj.h"
#include "pyparsetuple.h"
#include "pyparsetuplenpy.h"
#include "span.h"
#include "span_result_buffer.h"

namespace dctv {

enum class TstsSourceType {  // In processing order
  CLOSE_BROADCAST,
  CLOSE,
  OPEN,
};

constexpr auto valid_tsts_values = make_array(
    static_cast<int>(TstsSourceType::CLOSE_BROADCAST),
    static_cast<int>(TstsSourceType::CLOSE),
    static_cast<int>(TstsSourceType::OPEN)
);

using std::tie;

static
unique_pyref
time_series_to_span(PyObject*, PyObject* py_args)
{
  namespace ae = auto_event;
  using SourceNumber = int;

  auto args = PARSEPYARGS_V(
      (pyref, sources)
      (pyref, span_cb)
      (QueryExecution*, qe)
  )(py_args);

  struct Source final {
    unique_pyref py_iter;
    TstsSourceType type;
    int priority;
    ResultBuffer<Index /* rising */,
                 Index /* falling */> edge_index_out;
  };

  auto mk_source = [&](pyref py_spec) -> Source {
    // TODO(dancol): support masking
    auto spec = PARSEPYARGS_V(
        (pyref, iter)
        (pyref, edge_index_cb)
        (uint8_t, source_type)
        (int, priority)
    )(py_spec);

    if (!seq_contains(valid_tsts_values, spec.source_type))
      throw_pyerr_fmt(PyExc_ValueError,
                      "invalid source type %s",
                      spec.source_type);
    auto source_type = static_cast<TstsSourceType>(spec.source_type);
    if (spec.priority != 0 && source_type != TstsSourceType::OPEN)
      throw_invalid_query_fmt("priority meaningful only in OPEN source type");
    return {
      spec.iter.addref(),
      source_type,
      spec.priority,
      { spec.edge_index_cb.addref(), args.qe },
    };
  };

  Vector<Source> sources = py2vec(args.sources, mk_source);
  if (sources.size() >
      static_cast<size_t>(std::numeric_limits<int>::max()))
    throw_pyerr_msg(PyExc_OverflowError, "too many event sources");
  int nsources = static_cast<int>(sources.size());

  // While we support multiple open sources, only one source can be
  // open on a given partition at a time.  If multiple events occur
  // for a given partition at a given timestamp, the last one we
  // consult, in source order, "wins", ensuring that we never generate
  // zero-duration spans.

  struct PartitionState final {
    TimeStamp last_open_ts;
    Index index_at_open;
    Index index_at_close;
    Index new_index_at_open;
    int opener_source_id = -1;
    int closer_source_id = -1;
    int new_opener_source_id = -1;
  };

  Vector<Partition> changed_partitions;
  HashTable<Partition, PartitionState> partitions;
  SpanTableResultBuffer span_out(args.span_cb.addref(),
                                 args.qe,
                                 /*using_partition=*/true);

  auto get_or_create_ps = [&](Partition partition) {
    return &partitions.try_emplace(partition).first->second;
  };

  auto maybe_get_ps = [&](Partition partition)
      -> PartitionState* {
    auto it = partitions.find(partition);
    if (it == partitions.end())
      return nullptr;
    return &it->second;
  };

  auto get_ps = [&](Partition partition) {
    PartitionState* ps = maybe_get_ps(partition);
    assume(ps);
    return ps;
  };

  auto is_ps_open = [&](const PartitionState* ps) {
    return ps->opener_source_id >= 0;
  };

  auto is_ps_marked_for_closing = [&](const PartitionState* ps) {
    return ps->closer_source_id >= 0;
  };

  auto is_ps_marked_for_opening = [&](const PartitionState* ps) {
    return ps->new_opener_source_id >= 0;
  };

  auto is_ps_marked_changed = [&](const PartitionState* ps) {
    return is_ps_marked_for_opening(ps) ||
        is_ps_marked_for_closing(ps);
  };

  auto mark_ps_for_closing = [&](PartitionState* ps,
                                 Index index_at_close,
                                 int closer_source_id) {
    assume(index_at_close >= 0);
    assume(closer_source_id >= 0);
    assume(is_ps_open(ps));
    if (!is_ps_marked_for_closing(ps)) {  // First wins
      ps->index_at_close = index_at_close;
      ps->closer_source_id = closer_source_id;
      assume(is_ps_marked_for_closing(ps));
    }
  };

  auto mark_ps_for_opening = [&](PartitionState* ps,
                                 Index index_at_open,
                                 int opener_source_id) {
    assume(index_at_open >= 0);
    assume(opener_source_id >= 0);
    ps->new_opener_source_id = opener_source_id;
    ps->new_index_at_open = index_at_open;
    assume(is_ps_marked_for_opening(ps));
  };

  // TODO(dancol): fancier tiebreaking for simultaneous events

  // Even if timestamps are identical, we should be able to break ties
  // by looking at trace file position, under the assumption that
  // events in the trace are emitted in the order in which
  // they occurr.

  auto open_partition = [&](Partition partition,
                            PartitionState* ps,
                            Index index_at_open,
                            int opener_source_id) {
    assume(index_at_open >= 0);
    assume(opener_source_id >= 0);
    if (!is_ps_marked_changed(ps))
      changed_partitions.push_back(partition);
    if (is_ps_open(ps))
      mark_ps_for_closing(ps, index_at_open, opener_source_id);
    mark_ps_for_opening(ps, index_at_open, opener_source_id);
    assume(is_ps_marked_changed(ps));
  };

  auto close_partition = [&](Partition partition,
                             PartitionState* ps,
                             Index index_at_close,
                             int closer_source_id) {
    assume(index_at_close >= 0);
    assume(closer_source_id >= 0);
    if (is_ps_open(ps)) {
      if (!is_ps_marked_changed(ps))
        changed_partitions.push_back(partition);
      mark_ps_for_closing(ps, index_at_close, closer_source_id);
      assume(is_ps_marked_changed(ps));
    }
  };

  auto process_changed_partition = [&](TimeStamp now,
                                       Partition partition) {
    PartitionState* ps = get_ps(partition);
    assume(is_ps_marked_changed(ps));
    if (!is_ps_open(ps)) {
      assume(!is_ps_marked_for_closing(ps));
    } else {
      assume(is_ps_marked_for_closing(ps));
      assume(ps->last_open_ts < now);
      span_out.add(ps->last_open_ts, now - ps->last_open_ts, partition);
      for (int source_id = 0; source_id < nsources; ++source_id)
        sources[source_id].edge_index_out.add(
            source_id == ps->opener_source_id ? ps->index_at_open : -1,
            source_id == ps->closer_source_id ? ps->index_at_close : -1);
      ps->opener_source_id = -1;
      ps->index_at_open = -1;
      ps->closer_source_id = -1;
      ps->index_at_close = -1;
      assume(!is_ps_open(ps));
      assume(!is_ps_marked_for_closing(ps));
    }
    if (is_ps_marked_for_opening(ps)) {
      ps->last_open_ts = now;
      ps->opener_source_id = ps->new_opener_source_id;
      ps->index_at_open = ps->new_index_at_open;
      ps->new_opener_source_id = -1;
      ps->new_index_at_open = -1;
      assume(is_ps_open(ps));
      assume(!is_ps_marked_for_opening(ps));
    }
    assume(!is_ps_marked_changed(ps));
  };

  AUTOEVENT_DECLARE_EVENT(
      Event,
      (Partition, partition, ae::partition)
  );

  auto process_changed_partitions = [&](TimeStamp now) {
    std::sort(changed_partitions.begin(), changed_partitions.end());
    for (Partition partition : changed_partitions)
      process_changed_partition(now, partition);
    changed_partitions.clear();
  };

  auto handle_event = [&]
      (SourceNumber source_number,
       auto&& ae,
       TimeStamp ts,
       const Event& event,
       Index index)
  {
    Source& source = sources[source_number];
    switch (source.type) {
      case TstsSourceType::CLOSE_BROADCAST: {
        for (auto& [partition, ps] : partitions)
          close_partition(partition,
                          &ps,
                          index,
                          source_number);
        break;
      }
      case TstsSourceType::CLOSE: {
        PartitionState* ps = maybe_get_ps(event.partition);
        if (ps)
          close_partition(event.partition,
                          ps,
                          index,
                          source_number);
        break;
      }
      case TstsSourceType::OPEN: {
        PartitionState* ps = maybe_get_ps(event.partition);
        if (ps) {
          bool already_higher_prio =
              (is_ps_open(ps) &&
               !is_ps_marked_for_closing(ps) &&
               (sources[ps->opener_source_id].priority < source.priority))
              ||
              (is_ps_marked_for_opening(ps) &&
               (sources[ps->new_opener_source_id].priority <
                source.priority));
          if (already_higher_prio)
            break;
        }
        open_partition(
            event.partition,
            get_or_create_ps(event.partition),
            index,
            source_number);
        break;
      }
    }
  };

  auto event_tiebreaker = [&](
      SourceNumber left_source_number,
      SourceNumber right_source_number,
      const Event& left,
      const Event& right,
      Index left_index,
      Index right_index)
  {
    // Include source_id in the event ordering as a tiebreaker so that
    // if events occur "simultaneously", we get predictable
    // prioritization.  No need to include partition, since we sort
    // the changed partitions before each output.
    return
        tie(sources[left_source_number].type,
            left_source_number,
            left_index)
        <
        tie(sources[right_source_number].type,
            right_source_number,
            right_index);
  };

  ae::process(
      ae::ts_batch_end_hook([&](auto&& ae, TimeStamp ts) {
        process_changed_partitions(ts);
      }),
      ae::multi_input<Event, SourceNumber>(
          [&]{
            Vector<pyref> py_iterators;
            py_iterators.reserve(nsources);
            for (Source& source : sources)
              py_iterators.push_back(source.py_iter);
            return py_iterators;
          }(),
          handle_event,
          ae::add_index_argument,
          ae::ordering_tiebreaker(event_tiebreaker)
      )
  );

  assume(changed_partitions.empty());
  span_out.flush();
  for (Source& es : sources)
    es.edge_index_out.flush();
  return addref(Py_None);
}



static PyMethodDef functions[] = {
  make_methoddef("time_series_to_span",
                 wraperr<time_series_to_span>(),
                 METH_VARARGS,
                 "Convert time series data to spans"),
  { 0 }
};

void
init_time_series_to_span(pyref m)
{
  register_constant(m, "TSTS_CLOSE_BROADCAST",
                    static_cast<int>(TstsSourceType::CLOSE_BROADCAST));
  register_constant(m, "TSTS_CLOSE",
                    static_cast<int>(TstsSourceType::CLOSE));
  register_constant(m, "TSTS_OPEN",
                    static_cast<int>(TstsSourceType::OPEN));
  register_functions(m, functions);
}

}  // namespace dctv
