// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <boost/intrusive/list.hpp>
#include <functional>
#include <type_traits>

#include "mmap.h"
#include "npy.h"
#include "optional.h"
#include "pyutil.h"
#include "simple_variant.h"
#include "unique_fd.h"
#include "vector.h"

// One-dimensional low-level backing storage for DCTV blocks

namespace dctv {

struct Hunk;
struct QueryCache;
struct HunkPinner;

using unique_hunk = unique_obj_pyref<Hunk>;

using HunkScore = double;
struct SpillRecord;

struct HunkExtraDebug {
  inline HunkExtraDebug(HunkScore new_hunk_score_override) noexcept;
  inline HunkScore get_score_override() const noexcept;
  void set_score_override(HunkScore score_override);
 private:
  HunkScore score_override;
};

struct HunkExtraOpt {
  inline HunkExtraOpt(HunkScore new_hunk_score_override) noexcept;
  inline HunkScore get_score_override() const noexcept;
  void set_score_override(HunkScore score_override);
};

using HunkExtra = std::conditional_t<safe_mode,
                                     HunkExtraDebug,
                                     HunkExtraOpt>;

struct Hunk final : BasePyObject, SupportsGc, private HunkExtra
{
  // ARRAY must be a hunk-backed array; return the backing hunk.
  static unique_hunk retrieve_backing_hunk(pyarray_ref array) noexcept;

  // Make a hunk view of ARRAY.  If ARRAY is already hunk-backed and
  // ARRAY is exactly equivalent to its backing hunk (i.e., not a
  // view), return that hunk directly (freezing the hunk by side
  // effect).  Otherwise, make a new hunk.
  static unique_hunk convert_pyarray(QueryCache* qc, unique_pyarray array);
  static unique_hunk make_uninit(QueryCache* qc,
                                 unique_dtype dtype,
                                 npy_intp size);
  static unique_hunk make_empty(QueryCache* qc, unique_dtype dtype);
  static unique_hunk broadcast_false(QueryCache* qc, npy_intp nelem);
  static unique_hunk /* nullable */ try_merge(Hunk* left, Hunk* right);

  unique_pyarray inflate();
  void resize(npy_intp new_nelem);
  inline unique_hunk view(npy_intp low, npy_intp high) const;

  using WalkCb = std::function<void(Hunk*)>;
  inline void enumerate_children(const WalkCb& cb) const;

  // Ensure that hunk contents and size cannot change after this point.
  inline void freeze();

  // Whether this hunk is frozen against future modification.
  inline bool is_frozen() const noexcept;

  // Whether someone has a memory view on this hunk.
  inline bool is_pinned() const noexcept;

  // Reduce RAM use.  May release GIL!
  void deflate();

  // For testing: set an explicit hunk score.
  void override_score(HunkScore score_override);

  // Get a string (statically allocated) naming the hunk core in use.
  const char* get_core_name() const noexcept;

  // Set a score override for testing.
  void set_score_override(HunkScore score_override);
  using HunkExtra::get_score_override;

  struct CopyOrPin final : NoCopy {
    CopyOrPin(npy_intp nelem, unique_dtype dtype);
    int py_traverse(visitproc visit, void* arg) const noexcept;
    void copy_and_advance(pyarray_ref array);
    void add_pinned_array(unique_pyarray array);
    uint8_t* get_start_ptr();
    uint8_t* get_cur_ptr();
    npy_intp get_cur_elem_pos();
    size_t get_cur_byte_pos();
    void copy_bcast_and_advance(
        pyarray_ref bcaster, npy_intp nelem);
    void advance_nelem(npy_intp nelem);
    void note_shared_bytes(npy_intp nbytes);
    npy_intp get_unshared_nbytes() const noexcept;
   private:
    Mmap reservation;
    unique_pyarray view;
    npy_intp pos;
    Vector<unique_pyarray> pins;
    npy_intp shared_nbytes = 0;
  };

  inline dtype_ref get_dtype() const noexcept;
  inline npy_intp get_size() const noexcept;
  inline bool is_broadcasted() const noexcept;
  inline bool is_composite() const noexcept;

  struct ResourceUse final {
    size_t memory_nbytes = 0;
    size_t disk_nbytes = 0;
  };

  inline ResourceUse get_resource_use() const noexcept;

  ~Hunk() noexcept;

  static PyTypeObject pytype;

  // Private interface begins here. Public clause below aren't
  // really public.

 private:
  friend struct HunkListAccess;
  friend struct HunkPinner;
  friend struct HunkReblocker;
  struct Core;

  // The hunk interface has many implementations, each one being a
  // derivative of CoreBase.  All functions in ICore that take a
  // SELF pointer are called with SELF's core set to the hunk being
  // called.  The hunk functions below are allowed to change the hunk
  // core through the SELF pointer.  In this way, Hunk is a kind of
  // FSM, with each state being a CoreBase derivative.

  // ICore is a pure virtual class so that we can skip inheriting
  // from it in optimized builds.  In debug builds, we use virtual inheritance
  // to benefit from C++'s override and signature checking.

  struct ICore {
    // Support Python GC.
    VARIANT_VIRTUAL
    int py_traverse(visitproc visit, void* arg)
        const noexcept VARIANT_ABSTRACT;

    // The number of elements described by this hunk.
    VARIANT_VIRTUAL
    npy_intp get_size(const Hunk* self)
        const noexcept VARIANT_ABSTRACT;

    // Get the dtype of elements in this hunk.
    VARIANT_VIRTUAL
    dtype_ref get_dtype(const Hunk* self)
        const noexcept VARIANT_ABSTRACT;

    // Make a Python array providing access to the contents of the
    // hunk.  While this array lives, the hunk core cannot be changed.
    VARIANT_VIRTUAL
    unique_pyarray inflate(Hunk* self) VARIANT_ABSTRACT;

    // Return whether this hunk is a broadcasting hunk.
    VARIANT_VIRTUAL
    bool is_broadcasted(const Hunk* self)
        const noexcept VARIANT_ABSTRACT;

    // Return a reference to the value suitable for broadcasting to a
    // different shape or the empty reference if this hunk cannot
    // be broadcast.
    VARIANT_VIRTUAL
    pyarray_ref get_broadcast_base(const Hunk* self)
        const noexcept VARIANT_ABSTRACT;

    // Change the amount of space available for this hunk.
    // If growing, any new space is uninitialized.  If shrinking, the
    // tail of the hunk is discarded.  Otherwise, contents of the hunk
    // are preserved.  May change the core type.
    VARIANT_VIRTUAL
    void resize(Hunk* self, npy_intp new_nelem) VARIANT_ABSTRACT;

    // Called by the HunkPinner destructor when an array returned by
    // inflate() is deallocated.  May discard core-internal memory.
    VARIANT_VIRTUAL
    void on_unpin(const Hunk* self)
        const noexcept VARIANT_ABSTRACT;

    // Participate in assembling an inflated MegaHunk.
    VARIANT_VIRTUAL
    void copy_or_pin(Hunk* self, CopyOrPin* cop,
                     npy_intp offset, npy_intp nelem) VARIANT_ABSTRACT;

    // Return whether this hunk references other hunks.
    VARIANT_VIRTUAL
    bool is_composite(const Hunk* self)
        const noexcept VARIANT_ABSTRACT;

    // Enumerate the hunk's children.
    VARIANT_VIRTUAL
    void enumerate_children(const Hunk* self, const WalkCb& cb)
        const VARIANT_ABSTRACT;

    // Called when this core is separated from its owning hunk.
    // Unlike other HunkCore functions, this one does _not_ take a
    // self pointer: by the time this function is called, the hunk may
    // be destroyed or may have a different core.  QC is, however, the
    // QueryCache associated with the hunk to which the hunk core
    // previously belonged.  After this function is called, the only
    // other function called on this core will be the destructor.
    VARIANT_VIRTUAL
    void on_core_detached(QueryCache* qc)
        noexcept VARIANT_ABSTRACT;

    // Create a hunk that's a view on the current hunk.  Creating a view
    // of a view creates a view on the original object.
    VARIANT_VIRTUAL
    unique_hunk view(const Hunk* self, npy_intp low, npy_intp high)
        const VARIANT_ABSTRACT;

    // Accumulate resource use into MU.
    VARIANT_VIRTUAL
    void add_resource_use(const Hunk* self, ResourceUse* mu)
        const noexcept VARIANT_ABSTRACT;

    // Attempt to reduce resource use.  Illegal if the hunk is pinned.
    // May replace the core of the hunk.
    VARIANT_VIRTUAL
    void deflate(Hunk* self) VARIANT_ABSTRACT;

    // Get a string (statically allocated) naming the hunk core in use.
    VARIANT_VIRTUAL
    const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_ABSTRACT;
  };

  // CoreBase contains default implementations of many of the
  // interfaces in ICore.
  struct CoreBase VARIANT_WHEN_DEBUG(: virtual ICore) {
    inline bool is_broadcasted(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    inline pyarray_ref get_broadcast_base(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    void resize(Hunk* self, npy_intp new_nelem)
        VARIANT_OVERRIDE;
    inline void on_unpin(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    void copy_or_pin(Hunk* self, CopyOrPin* cop,
                     npy_intp offset, npy_intp nelem)
        VARIANT_OVERRIDE;
    inline bool is_composite(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    void enumerate_children(const Hunk* self, const WalkCb& cb)
        const VARIANT_OVERRIDE;
    inline void on_core_detached(QueryCache* qc)
        noexcept VARIANT_OVERRIDE;
    unique_hunk view(const Hunk* self, npy_intp low, npy_intp high)
        const VARIANT_OVERRIDE;
  };

  // Use cache-managed memory-mapped chunks as backing store.
  // Allows us to play VM games to make contiguous megahunks
  // without copies.
  struct MmapCoreBase : CoreBase {
    int py_traverse(visitproc visit, void* arg)
        const noexcept VARIANT_OVERRIDE;
    inline npy_intp get_size(const Hunk* self) const noexcept
        VARIANT_OVERRIDE;
    inline dtype_ref get_dtype(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    unique_pyarray inflate(Hunk* self) VARIANT_OVERRIDE;
    void copy_or_pin(Hunk* self, CopyOrPin* cop,
                     npy_intp offset, npy_intp nelem)
        VARIANT_OVERRIDE;
    void on_core_detached(QueryCache* qc) noexcept VARIANT_OVERRIDE;
    void add_resource_use(const Hunk* self, ResourceUse* mu)
        const noexcept VARIANT_OVERRIDE;

   protected:
    inline MmapCoreBase(MmapShared storage,
                        unique_dtype dtype,
                        npy_intp nelem) noexcept;
    unique_pyarray view_of_the_mmap(bool writeable);
    npy_intp nelem;
    unique_dtype dtype;
    MmapShared storage;
  };

  struct MmapCore final : MmapCoreBase {
    MmapCore(MmapShared storage,
             unique_dtype dtype,
             npy_intp nelem) noexcept;
    void deflate(Hunk* self) VARIANT_OVERRIDE;
    void resize(Hunk* self, npy_intp new_nelem) VARIANT_OVERRIDE;
    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
  };

  struct WithSpillMixin VARIANT_WHEN_DEBUG(: virtual ICore) {
    WithSpillMixin(std::unique_ptr<SpillRecord> spill) noexcept;
    void deflate(Hunk* self) VARIANT_OVERRIDE;
    void add_resource_use(const Hunk* self, ResourceUse* mu)
        const noexcept VARIANT_OVERRIDE;
    void on_core_detached(QueryCache* qc) noexcept VARIANT_OVERRIDE;
   protected:
    Core core_from_spill_record(Hunk* self);
   private:
    std::unique_ptr<SpillRecord> spill;
  };

  struct MmapCoreWithSpill final : MmapCoreBase, WithSpillMixin {
    MmapCoreWithSpill(
        MmapShared storage,
        unique_dtype dtype,
        npy_intp nelem,
        std::unique_ptr<SpillRecord> spill) noexcept;
    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    void add_resource_use(const Hunk* self, ResourceUse* mu)
        const noexcept VARIANT_OVERRIDE;
    void on_core_detached(QueryCache* qc) noexcept VARIANT_OVERRIDE;
  };

  // Stitch several hunks into one logical core.  Useful for creating
  // linear views of enormous sequences of smaller hunks.  Very small
  // hunks may be copied into private memory owned by the MegaCore.
  struct MegaCore final : CoreBase {
    explicit MegaCore(Vector<unique_hunk> hunks);
    int py_traverse(visitproc visit, void* arg)
        const noexcept VARIANT_OVERRIDE;
    npy_intp get_size(const Hunk* self) const noexcept VARIANT_OVERRIDE;
    dtype_ref get_dtype(const Hunk* self) const noexcept VARIANT_OVERRIDE;
    unique_pyarray inflate(Hunk* self) VARIANT_OVERRIDE;
    void on_unpin(const Hunk* self) const noexcept VARIANT_OVERRIDE;
    void copy_or_pin(Hunk* self, CopyOrPin* cop,
                     npy_intp offset, npy_intp nelem)
        VARIANT_OVERRIDE;
    inline bool is_composite(const Hunk* self) const noexcept
        VARIANT_OVERRIDE;
    void enumerate_children(const Hunk* self, const WalkCb& cb)
        const VARIANT_OVERRIDE;
    unique_hunk view(const Hunk* self,
                     npy_intp low, npy_intp high) const
        VARIANT_OVERRIDE;

    void add_resource_use(const Hunk* self, ResourceUse* mu) const noexcept
        VARIANT_OVERRIDE;

    void deflate(Hunk* self) VARIANT_OVERRIDE;

    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;

   private:
    CopyOrPin stitch(Hunk* self);
    Vector<unique_hunk> sub_hunks;
    mutable optional<CopyOrPin> pin_info;
  };

  // Normal numpy array as backing store: we get this core when we
  // import an unmanaged array into DCTV.  There's no point converting
  // to a MmapCore if we're going to use the raw numpy array once and
  // throw it away.
  struct NumpyCoreBase : CoreBase {
    int py_traverse(visitproc visit, void* arg)
        const noexcept VARIANT_OVERRIDE;
    inline npy_intp get_size(const Hunk* self) const noexcept
        VARIANT_OVERRIDE;
    inline dtype_ref get_dtype(const Hunk* self) const noexcept
        VARIANT_OVERRIDE;
    inline pyarray_ref get_broadcast_base(const Hunk* self)
        const noexcept
        VARIANT_OVERRIDE;
    unique_pyarray inflate(Hunk* self) VARIANT_OVERRIDE;

    void add_resource_use(const Hunk* self, ResourceUse* mu) const noexcept
        VARIANT_OVERRIDE;

   protected:
    explicit inline NumpyCoreBase(unique_pyarray array) noexcept;
    unique_pyarray backing_store;
  };

  struct NumpyCore final : NumpyCoreBase {
    explicit NumpyCore(unique_pyarray array) noexcept;
    void deflate(Hunk* self) VARIANT_OVERRIDE;
    void resize(Hunk* self, npy_intp new_nelem) VARIANT_OVERRIDE;
    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
  };

  struct NumpyCoreWithSpill final : NumpyCoreBase, WithSpillMixin {
    NumpyCoreWithSpill(unique_pyarray array,
                       std::unique_ptr<SpillRecord> spill) noexcept;
    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    void on_core_detached(QueryCache* qc) noexcept VARIANT_OVERRIDE;
    void add_resource_use(const Hunk* self, ResourceUse* mu) const noexcept
        VARIANT_OVERRIDE;
  };

  struct BroadcastCore final : CoreBase {
    BroadcastCore(unique_pyarray broadcaster, npy_intp nelem);
    int py_traverse(visitproc visit, void* arg)
        const noexcept VARIANT_OVERRIDE;
    inline npy_intp get_size(const Hunk* self) const noexcept
        VARIANT_OVERRIDE;
    inline dtype_ref get_dtype(const Hunk* self) const noexcept
        VARIANT_OVERRIDE;
    inline pyarray_ref get_broadcast_base(const Hunk* self)
        const noexcept
        VARIANT_OVERRIDE;
    unique_pyarray inflate(Hunk* self) VARIANT_OVERRIDE;
    inline bool is_broadcasted(const Hunk* /*self*/) const noexcept
        VARIANT_OVERRIDE
    { return true; };
    void copy_or_pin(Hunk* self,
                     CopyOrPin* cop,
                     npy_intp offset,
                     npy_intp nelem) VARIANT_OVERRIDE;
    unique_hunk view(const Hunk* self, npy_intp low, npy_intp high)
        const VARIANT_OVERRIDE;

    void add_resource_use(const Hunk* self, ResourceUse* mu) const noexcept
        VARIANT_OVERRIDE;

    void deflate(Hunk* self) VARIANT_OVERRIDE;

    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;

   private:
    npy_intp nelem;
    unique_pyarray broadcaster;
  };

  struct SubCore final : CoreBase {
    SubCore(unique_hunk base,
            npy_intp offset,
            npy_intp nelem);
    int py_traverse(visitproc visit, void* arg)
        const noexcept VARIANT_OVERRIDE;
    inline npy_intp get_size(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    inline dtype_ref get_dtype(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;
    unique_pyarray inflate(Hunk* self) VARIANT_OVERRIDE;
    void on_unpin(const Hunk* self) const noexcept VARIANT_OVERRIDE;
    void copy_or_pin(Hunk* self,
                     CopyOrPin* cop,
                     npy_intp offset,
                     npy_intp nelem) VARIANT_OVERRIDE;
    unique_hunk view(const Hunk* self, npy_intp low, npy_intp high)
        const VARIANT_OVERRIDE;

    void add_resource_use(const Hunk* self, ResourceUse* mu) const noexcept
        VARIANT_OVERRIDE;

    void deflate(Hunk* self) VARIANT_OVERRIDE;

    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;

   private:
    npy_intp nelem;
    npy_intp offset;
    unique_hunk base;
    mutable unique_pyarray inflated_base;
  };

  struct SpilledCore final : CoreBase, WithSpillMixin {
    SpilledCore(npy_intp nelem,
                unique_dtype dtype,
                std::unique_ptr<SpillRecord> spill) noexcept;

    int py_traverse(visitproc visit, void* arg)
        const noexcept VARIANT_OVERRIDE;

    unique_pyarray inflate(Hunk* self) VARIANT_OVERRIDE;

    inline npy_intp get_size(const Hunk* self) const noexcept
        VARIANT_OVERRIDE;
    inline dtype_ref get_dtype(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;

    void on_core_detached(QueryCache* qc) noexcept VARIANT_OVERRIDE;
    inline const char* get_core_name(const Hunk* self)
        const noexcept VARIANT_OVERRIDE;

   private:
    npy_intp nelem;
    unique_dtype dtype;
  };

  using HunkVariant = SimpleVariant<
    MmapCore,
    MmapCoreWithSpill,
    MegaCore,
    NumpyCore,
    NumpyCoreWithSpill,
    BroadcastCore,
    SubCore,
    SpilledCore>;

  struct Core final : private HunkVariant {
    using HunkVariant::HunkVariant;
    using HunkVariant::operator=;
    using HunkVariant::get_if;
    using variant_base = ICore;
    VARIANT_FORWARD(py_traverse);
    VARIANT_FORWARD(get_size);
    VARIANT_FORWARD(get_dtype);
    VARIANT_FORWARD(inflate);
    VARIANT_FORWARD(is_broadcasted);
    VARIANT_FORWARD(get_broadcast_base);
    VARIANT_FORWARD(resize);
    VARIANT_FORWARD(on_unpin);
    VARIANT_FORWARD(copy_or_pin);
    VARIANT_FORWARD(is_composite);
    VARIANT_FORWARD(enumerate_children);
    VARIANT_FORWARD(on_core_detached);
    VARIANT_FORWARD(view);
    VARIANT_FORWARD(add_resource_use);
    VARIANT_FORWARD(deflate);
    VARIANT_FORWARD(get_core_name);
  };

  using HunkListLink =
      boost::intrusive::list_member_hook<
    boost::intrusive::link_mode<
      boost::intrusive::normal_link>>;

  static unique_hunk try_get_equiv_hunk(
      QueryCache* qc, pyarray_ref array);
  static optional<Core> try_broadcast(pyarray_ref array);
  static optional<Core> try_subcore_or_view(
      QueryCache* qc,
      unique_pyarray& array);
  static optional<Core> try_forward(unique_pyarray& array);

  inline void copy_or_pin(CopyOrPin* cop,
                          npy_intp offset,
                          npy_intp nelem);

 public:  // Only for make_pyobj
  explicit Hunk(QueryCache* qc, Core&& core);
  int py_traverse(visitproc visit, void* arg) const noexcept;

 private:
  // Make a hunk from a core, taking care to detach the core if
  // constructing the hunk object fails.
  static inline unique_hunk from_core(QueryCache* qc, Core core);

  // Change the core of the hunk.  Call from HunkCore implementations
  // only.  Does memory accounting internally.
  template<typename Functor>
  inline void change_core(Functor&& make_new_core);

  // Compress (possibly into disk storage) and change the core to a
  // spilled core.
  void deflate_memory_change_core(const void* mem,
                                  npy_intp nelem,
                                  dtype_ref dtype);
  std::unique_ptr<SpillRecord> spill_1(QueryCache* qc,
                                       const void* const in_buffer,
                                       const npy_intp chunk_nelem,
                                       const npy_intp elsize);

  // Properly account for changing resource use.
  void after_resource_use_changed(ResourceUse old_mu, ResourceUse new_mu);

  unique_obj_pyref<HunkPinner> get_pinner();
  unique_pyarray pin_new_naked_view(unique_pyarray view);
  unique_pyarray pin_naked_view_of(pyarray_ref array);

  static PyGetSetDef pygetset[];
  static PyMethodDef pymethods[];

  const unique_obj_pyref<QueryCache> qc;
  HunkPinner* pin = nullptr;  // Weak reference
  Core core;
  HunkListLink link;
  bool frozen = true;
};

struct HunkReblocker {
  inline HunkReblocker(npy_intp low, npy_intp high);
  inline npy_intp nelem_remaining() const noexcept;
  inline bool done() const noexcept;
  unique_hunk add(Hunk* sh);
  unique_hunk /* nullable */ finish(QueryCache* qc);
 private:
  void add_1(unique_hunk hunk);
  npy_intp skip;
  npy_intp nelem;
  Vector<unique_hunk> new_sub_hunks;
};

void init_hunk(pyref m);

}  // namespace dctv

#include "hunk-inl.h"
