// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "hunk.h"

#include <blosc.h>
#include <utility>

#include "automethod.h"
#include "fmt.h"
#include "fsutil.h"
#include "mmap.h"
#include "npy.h"
#include "pyerr.h"
#include "pyerrfmt.h"
#include "pyobj.h"
#include "query.h"
#include "query_cache.h"
#include "vector.h"

#include "pylog.h"

namespace dctv {

// We copy instead of create views of a hunk when the copy would have
// fewer than this many items.  It doesn't make sense to hold on to a
// huge hunk if we're only going to use a tiny portion of it.
static const npy_intp view_min_elements = 5;



struct BloscArgs final {
  static constexpr int clevel = 5;  // ???
  static constexpr int doshuffle = 1;
  static constexpr char compressor[] = "blosclz";
  static constexpr int blocksize = 0;  // Use default
  static constexpr int numinternalthreads = 1;  // Single-threaded
};

struct SpillRecordBase {
  VARIANT_VIRTUAL
  void add_resource_use(Hunk::ResourceUse* mu)
      const noexcept VARIANT_ABSTRACT;
  VARIANT_VIRTUAL
  void obliterate(obj_pyref<QueryCache> qc)
      noexcept VARIANT_ABSTRACT;

};

struct SpillRecordDisk final : SpillRecordBase {
  SpillRecordDisk(size_t compressed_nbytes,
                  String compressed_file_name);
  template<typename Functor>
  auto with_compressed_bytes(obj_pyref<QueryCache> qc,
                             Functor&& functor) const;
  void add_resource_use(Hunk::ResourceUse* mu)
      const noexcept VARIANT_OVERRIDE;
  void obliterate(obj_pyref<QueryCache> qc)
      noexcept VARIANT_OVERRIDE;
 private:
  size_t compressed_nbytes;
  String compressed_file_name;
};

struct SpillRecordMemory final : SpillRecordBase {
  SpillRecordMemory(size_t compressed_nbytes);
  ~SpillRecordMemory() noexcept;
  SpillRecordMemory(SpillRecordMemory&& other) noexcept;
  SpillRecordMemory& operator=(SpillRecordMemory&& other) noexcept;
  template<typename Functor>
  auto with_compressed_bytes(obj_pyref<QueryCache> qc,
                             Functor&& functor) const;
  void add_resource_use(Hunk::ResourceUse* mu)
      const noexcept VARIANT_OVERRIDE;
  void obliterate(obj_pyref<QueryCache> qc)
      noexcept VARIANT_OVERRIDE;
  void* get_mem() noexcept;
  void shrink(size_t new_compressed_nbytes);
 private:
  size_t compressed_nbytes;
  void* compressed_bytes;
};

using SpillRecordVariant = SimpleVariant<
  SpillRecordDisk,
  SpillRecordMemory>;

struct SpillRecord final : private SpillRecordVariant {
  using SpillRecordVariant::SpillRecordVariant;
  using SpillRecordVariant::operator=;
  using variant_base = SpillRecordBase;
  VARIANT_FORWARD(with_compressed_bytes);
  VARIANT_FORWARD(add_resource_use);
  VARIANT_FORWARD(obliterate);
};

SpillRecordDisk::SpillRecordDisk(size_t compressed_nbytes,
                                 String compressed_file_name)
    : compressed_nbytes(compressed_nbytes),
      compressed_file_name(std::move(compressed_file_name))
{}

template<typename Functor>
auto
SpillRecordDisk::with_compressed_bytes(
    obj_pyref<QueryCache> qc,
    Functor&& functor) const
{
  const auto compressed_buffer =
      std::make_unique<uint8_t[]>(this->compressed_nbytes);
  UniqueFd compressed_fd =
      qc->open_cache_file(this->compressed_file_name, O_RDONLY);
  size_t nr_bytes_read =
      xread_all(compressed_fd.get(),
                compressed_buffer.get(),
                compressed_nbytes);
  assume(nr_bytes_read <= compressed_nbytes);
  if (nr_bytes_read < compressed_nbytes)
    throw_pyerr_msg(PyExc_RuntimeError, "truncated spill file");
  return AUTOFWD(functor)(compressed_buffer.get(), compressed_nbytes);
}

void
SpillRecordDisk::add_resource_use(Hunk::ResourceUse* mu) const noexcept
{
  mu->disk_nbytes += this->compressed_nbytes;
}

void
SpillRecordDisk::obliterate(obj_pyref<QueryCache> qc) noexcept
{
  qc->delete_cache_file(this->compressed_file_name);
}

SpillRecordMemory::SpillRecordMemory(size_t compressed_nbytes)
    : compressed_nbytes(compressed_nbytes),
      compressed_bytes(malloc(this->compressed_nbytes))
{
  if (!this->compressed_bytes)
    throw std::bad_alloc();
}

void*
SpillRecordMemory::get_mem() noexcept
{
  return this->compressed_bytes;
}

void
SpillRecordMemory::shrink(size_t nbytes)
{
  void* new_bytes = realloc(this->compressed_bytes, nbytes);
  if (!new_bytes)
    throw std::bad_alloc();
  this->compressed_bytes = new_bytes;
  this->compressed_nbytes = nbytes;
}

template<typename Functor>
auto
SpillRecordMemory::with_compressed_bytes(
    obj_pyref<QueryCache> /*qc*/,
    Functor&& functor) const
{
  return AUTOFWD(functor)(this->compressed_bytes, this->compressed_nbytes);
}

void
SpillRecordMemory::add_resource_use(Hunk::ResourceUse* mu) const noexcept
{
  mu->memory_nbytes += this->compressed_nbytes;
}

void
SpillRecordMemory::obliterate(obj_pyref<QueryCache> qc) noexcept
{
  // Do nothing
}

SpillRecordMemory::~SpillRecordMemory() noexcept
{
  free(this->compressed_bytes);
}

SpillRecordMemory::SpillRecordMemory(SpillRecordMemory&& other) noexcept
    : compressed_nbytes(other.compressed_nbytes),
      compressed_bytes(other.compressed_bytes)
{
  other.compressed_bytes = nullptr;
}

SpillRecordMemory&
SpillRecordMemory::operator=(SpillRecordMemory&& other) noexcept
{
  if (&other != this) {
    free(this->compressed_bytes);
    this->compressed_bytes = other.compressed_bytes;
    this->compressed_nbytes = other.compressed_nbytes;
    other.compressed_bytes = nullptr;
  }
  return *this;
}



// Make a new view of a ARRAY that's not set up to regard ARRAY as the
// view's base object.  It's the caller's job to make sure ARRAY (or
// its own backing storage) lives as long as the returned view does,
// usually via HunkPinner.
static
unique_pyarray
make_naked_view(pyarray_ref array, bool can_write)
{
  unique_dtype dtype = npy_dtype(array).notnull().addref();
  int flags = npy_flags(array);
  if (!can_write)
    flags &= ~NPY_ARRAY_WRITEABLE;
  else if ((flags & NPY_ARRAY_WRITEABLE) == 0)
    throw_pyerr_msg(PyExc_RuntimeError,
                    "cannot enable write mode on read-only view");
  unique_pyarray ret = adopt_check_as_unsafe<PyArrayObject>(
      PyArray_NewFromDescr(
          &PyArray_Type,
          dtype.release() /* sic */,
          npy_ndim(array),
          npy_dims(array),
          npy_strides(array),
          npy_data_raw(array),
          flags,
          /*obj=*/nullptr
      ));
  assert(npy_writeable_p(ret) == can_write);
  return ret;
}

static
unique_pyarray
make_memory_view(void* mem,
                 npy_intp nelem,
                 unique_dtype dtype,
                 bool writeable=false)
{
  assume(nelem >= 0);
  assume(dtype);
  assume(mem);
  constexpr int ndim = 1;
  npy_intp dims[ndim] = { nelem };
  return adopt_check_as_unsafe<PyArrayObject>(
      PyArray_NewFromDescr(
          &PyArray_Type,
          dtype.release() /* sic*/,
          ndim,
          dims,
          /*strides=*/nullptr,
          mem,
          writeable ? NPY_ARRAY_WRITEABLE : 0,
          /*obj=*/nullptr
      ));
}



struct HunkPinner final : BasePyObject, HasRepr {
  friend struct Hunk;
  explicit HunkPinner(unique_hunk hunk);
  ~HunkPinner() noexcept;
  explicit operator String() const;
  inline friend Hunk* maybe_pinned_hunk(pyref obj) noexcept;
  inline unique_hunk hunk_addref() noexcept;
  static PyTypeObject pytype;
 private:
  unique_hunk hunk;
  static PyMemberDef pymembers[];
};

Hunk*
maybe_pinned_hunk(pyref obj) noexcept
{
  if (!obj)
    return {};
  if (!isinstance_exact(obj, pytype_of<HunkPinner>()))
    return {};
  return obj.as_unsafe<HunkPinner>()->hunk.get();
}

HunkPinner::HunkPinner(unique_hunk hunk)
    : hunk(std::move(hunk.notnull()))
{
  assume(!this->hunk->is_pinned());
  assume(!this->hunk->pin);
  this->hunk->pin = this;
  assume(this->hunk->is_pinned());
}

HunkPinner::~HunkPinner() noexcept
{
  assume(this->hunk->is_pinned());
  assume(this->hunk->pin == this);
  this->hunk->core.on_unpin(this->hunk.get());
  this->hunk->pin = nullptr;
  assume(!this->hunk->is_pinned());
}

HunkPinner::operator String() const
{
  return fmt("<HunkPinner hunk=%s>", repr(this->hunk));
}

unique_hunk
HunkPinner::hunk_addref() noexcept
{
  return this->hunk.notnull().addref();
}



unique_hunk
Hunk::make_empty(QueryCache* qc, unique_dtype dtype)
{
  return Hunk::from_core(
      qc,
      NumpyCore(make_uninit_pyarray(std::move(dtype), /*nelem=*/0)));
}

unique_hunk
Hunk::broadcast_false(QueryCache* qc, npy_intp nelem)
{
  return Hunk::from_core(qc, BroadcastCore(make_false_array(), nelem));
}

unique_hunk /* nullable */
Hunk::try_merge(Hunk* left, Hunk* right)
{
  assume(left->qc == right->qc);
  if (left->get_dtype() != right->get_dtype())
    return {};
  pyarray_ref left_bcast =
      left->core.get_broadcast_base(left);
  pyarray_ref right_bcast =
      right->core.get_broadcast_base(right);
  if (left_bcast && right_bcast) {
    assume(npy_size1d(left_bcast) == 1);
    assume(npy_size1d(right_bcast) == 1);
    if (memcmp(npy_data_raw(left_bcast),
               npy_data_raw(right_bcast),
               left->get_dtype()->elsize) == 0)
      return Hunk::from_core(
          left->qc.get(),
          BroadcastCore(left_bcast.addref(),
                        add_raise_on_overflow(
                            left->get_size(),
                            right->get_size())));
  }
  return {};
}

void
Hunk::resize(npy_intp new_nelem)
{
  if (this->is_frozen())
    throw_pyerr_msg(PyExc_RuntimeError, "cannot resize frozen hunk");
  if (this->is_pinned())
    throw_pyerr_msg(PyExc_RuntimeError, "cannot resize pinned hunk");
  this->core.resize(this, new_nelem);  // May resize
  assert(this->get_size() == new_nelem);
}

void
Hunk::deflate_memory_change_core(const void* const mem,
                                 const npy_intp nelem,
                                 dtype_ref dtype)
{
  // Blosc APIs accept a size_t, but the library has various 32-bit
  // limits, so limit compression to what we can fit in a signed
  // int32.  If we're compressing more than we can fit in an int32,
  // make a composite block instead.  We also split when the current
  // block is bigger than the QueryCache block size.
  assume(dtype);
  const int dirfd = this->qc->cachedir.get();
  Vector<std::unique_ptr<SpillRecord>> spilled_chunks;
  const npy_intp elsize = dtype->elsize;
  assume(elsize > 0);
  FINALLY({
      for (auto& sr : spilled_chunks)
        sr->obliterate(this->qc);
    });

  // Figure out how many rows we can fit in each chunk.  Almost all
  // the time, we're going to limit the per-row chunk count to the
  // query block size instead of being bound by int32 limits, but
  // let's be correct in all cases.
  const npy_intp max_rows_by_byte_count =
      (std::numeric_limits<int32_t>::max() - BLOSC_MAX_OVERHEAD)
      / elsize;
  const npy_intp max_chunk_rows =
      std::min(qc->get_block_size(),
               max_rows_by_byte_count);
  assume(max_chunk_rows > 0);

  // Actually compress into a sequence of blocks.
  for (npy_intp offset = 0; offset < nelem;) {
    const npy_intp chunk_nelem = std::min(nelem - offset, max_chunk_rows);
    const void* const in_buffer =
        reinterpret_cast<const uint8_t*>(mem) +
        (static_cast<size_t>(offset) * elsize);
    // Make sure the push_back can't fail.
    spilled_chunks.reserve(spilled_chunks.size() + 1);
    // Actually make the chunk.
    spilled_chunks.push_back(
        this->spill_1(this->qc.get(), in_buffer, chunk_nelem, elsize));
    offset += chunk_nelem;
  }

  if (spilled_chunks.empty()) {
    // Nothing to do.
  } else if (spilled_chunks.size() == 1) {
    // We compressed to one block, so we can just make this hunk a
    // spilled hunk.
    std::unique_ptr<SpillRecord> spill = std::move(spilled_chunks[0]);
    spilled_chunks.clear();
    // change_core can throw, but only after installing the new core.
    // SpilledCore's constructor can't throw, so we can't leak a spill
    // file here.
    this->change_core([&]{
      return SpilledCore(nelem, dtype.addref(), std::move(spill));
    });
  } else {
    // We had to split our data into multiple hunks, so turn this hunk
    // into a composite hunk.
    Vector<unique_hunk> sub_hunks;
    std::reverse(spilled_chunks.begin(), spilled_chunks.end());
    for (npy_intp offset = 0; offset < nelem;) {
      const npy_intp chunk_nelem = std::min(nelem - offset, max_chunk_rows);
      assume(!spilled_chunks.empty());
      std::unique_ptr<SpillRecord> spill = std::move(spilled_chunks.back());
      spilled_chunks.pop_back();
      sub_hunks.push_back(
          Hunk::from_core(this->qc.get(),
                          SpilledCore(nelem,
                                      dtype.addref(),
                                      std::move(spill))));
      offset += chunk_nelem;
    }
    this->change_core([&]{
      return MegaCore(std::move(sub_hunks));
    });
  }
}

std::unique_ptr<SpillRecord>
Hunk::spill_1(QueryCache* qc,
              const void* const in_buffer,
              const npy_intp chunk_nelem,
              const npy_intp elsize)
{
  const size_t chunk_in_bytes = static_cast<size_t>(chunk_nelem) *
      static_cast<size_t>(elsize);
  const size_t chunk_out_max_bytes =
      chunk_in_bytes + BLOSC_MAX_OVERHEAD;
  assume(chunk_in_bytes <=
         static_cast<size_t>(std::numeric_limits<int>::max()));

  // If our worst-case compressed size is small enough for memory
  // caching, just compress into a memory buffer right away.
  // Otherwise, compress into a disk file, and if the disk compression
  // ends up producing only a few bytes, copy into a memory chunk
  // after all.

  optional<SpillRecordMemory> mem_sr;
  optional<MmapShared> disk_storage;
  String data_file_name;
  UniqueFd data_file_fd;
  void* out_buffer;

  FINALLY({
      if (!data_file_name.empty()) {
        // Be friendly to Windows and close before unlink.
        data_file_fd.reset();
        qc->delete_cache_file(data_file_name);
      }
    });

  if (qc->should_spill_to_memory(chunk_out_max_bytes)) {
    mem_sr = SpillRecordMemory(chunk_out_max_bytes);
    out_buffer = mem_sr->get_mem();
  } else {
    assert(data_file_name.empty());
    std::tie(data_file_name, data_file_fd) = qc->make_cache_file("hunk");
    assert(!data_file_name.empty());
    xtruncate(data_file_fd.get(), chunk_out_max_bytes);
    disk_storage = MmapShared::of_file(
        data_file_fd.get(),
        chunk_out_max_bytes,
        PROT_READ | PROT_WRITE);
    out_buffer = disk_storage->addr();
  }

  int out_nbytes = blosc_compress_ctx(
      BloscArgs::clevel,
      BloscArgs::doshuffle,
      /*typesize=*/elsize,
      /*nbytes=*/chunk_in_bytes,
      /*src=*/in_buffer,
      /*dest=*/out_buffer,
      /*destsize=*/chunk_out_max_bytes,
      /*compressor=*/BloscArgs::compressor,
      /*blocksize=*/BloscArgs::blocksize,
      /*numinternalthreads=*/BloscArgs::numinternalthreads);

  if (out_nbytes < 0)
    throw_pyerr_msg(PyExc_RuntimeError, "spill compression failed");
  if (mem_sr) {
    mem_sr->shrink(out_nbytes);
  } else if (!mem_sr && qc->should_spill_to_memory(out_nbytes)) {
    mem_sr = SpillRecordMemory(out_nbytes);
    memcpy(mem_sr->get_mem(),
           out_buffer,
           out_nbytes);
  }

  if (!mem_sr && data_file_fd && chunk_out_max_bytes != out_nbytes) {
    xtruncate(data_file_fd.get(), out_nbytes);
  }

  std::unique_ptr<SpillRecord> ret;
  if (mem_sr) {
    ret = std::make_unique<SpillRecord>(std::move(*mem_sr));
  } else {
    ret = std::make_unique<SpillRecord>(SpillRecordDisk(
        out_nbytes,
        std::move(data_file_name)));
    data_file_name.clear();
  }
  assume(ret);
  return ret;

}

void
Hunk::CoreBase::resize(Hunk* /*self*/, npy_intp /*new_nelem*/)
{
  throw_pyerr_msg(PyExc_RuntimeError, "hunk does not support resizing");
}

void
Hunk::CoreBase::copy_or_pin(Hunk* const self,
                            CopyOrPin* const cop,
                            const npy_intp offset,
                            const npy_intp nelem)
{
  // Generic implementation that just copies.
  npy_intp self_nelem = static_cast<size_t>(self->get_size());
  assert(0 <= nelem && nelem <= self_nelem);
  if (nelem == 0)
    return;
  unique_pyarray array = self->inflate();
  if (offset != 0 || nelem < self_nelem) {
    array = get_npy_slice(array, offset, offset + nelem);
  }
  assert(npy_size1d(array) == nelem);
  cop->copy_and_advance(array);
}

unique_hunk
Hunk::CoreBase::view(const Hunk* self, npy_intp low, npy_intp high)
    const
{
  return Hunk::from_core(
      self->qc.get(),
      SubCore(xaddref(self), low, high - low));
}

void
Hunk::CoreBase::enumerate_children(const Hunk* /*self*/,
                                   const WalkCb& /*cb*/)
    const
{
  DCTV_UNREACHABLE;  // No children
}



Hunk::CopyOrPin::CopyOrPin(npy_intp nelem, unique_dtype dtype)
    : reservation(Mmap::anonymous(nelem * dtype->elsize)),
      view(make_memory_view(reservation.addr(),
                            nelem,
                            std::move(dtype),
                            /*writeable=*/true)),
      pos(0)
{}

int
Hunk::CopyOrPin::py_traverse(visitproc visit, void* arg) const noexcept
{
  Py_VISIT(this->view.get());
  for (auto& h : this->pins)
    Py_VISIT(h.get());
  return 0;
}
void
Hunk::CopyOrPin::copy_and_advance(pyarray_ref array)
{
  set_slice(this->view,
            this->get_cur_elem_pos(),
            this->get_cur_elem_pos() + npy_size1d(array),
            array);
  this->advance_nelem(npy_size1d(array));
}

void
Hunk::CopyOrPin::add_pinned_array(unique_pyarray array)
{
  this->pins.emplace_back(std::move(array));
}

uint8_t*
Hunk::CopyOrPin::get_start_ptr()
{
  return static_cast<uint8_t*>(this->reservation.addr());
}

size_t
Hunk::CopyOrPin::get_cur_byte_pos()
{
  return static_cast<size_t>(this->get_cur_elem_pos()) *
      npy_dtype(this->view)->elsize;
}

npy_intp
Hunk::CopyOrPin::get_cur_elem_pos()
{
  return this->pos;
}

uint8_t*
Hunk::CopyOrPin::get_cur_ptr()
{
  return this->get_start_ptr() + this->get_cur_byte_pos();
}

void
Hunk::CopyOrPin::copy_bcast_and_advance(
    pyarray_ref bcaster, npy_intp nelem)
{
  assume(nelem >= 0);
  set_slice(this->view,
            this->get_cur_elem_pos(),
            this->get_cur_elem_pos() + nelem,
            bcaster);
  this->advance_nelem(nelem);
}

void
Hunk::CopyOrPin::advance_nelem(npy_intp nelem)
{
  assume(nelem >= 0);
  assume(nelem <= npy_size1d(this->view) - this->pos);
  this->pos += nelem;
}

void
Hunk::CopyOrPin::note_shared_bytes(npy_intp nbytes)
{
  assume(nbytes >= 0);
  this->shared_nbytes += nbytes;
}

npy_intp
Hunk::CopyOrPin::get_unshared_nbytes() const noexcept
{
  return this->reservation.nbytes() - this->shared_nbytes;
}



Hunk::WithSpillMixin::WithSpillMixin(
    std::unique_ptr<SpillRecord> spill) noexcept
    : spill(std::move(spill))
{
  assume(this->spill);
}

void
Hunk::WithSpillMixin::add_resource_use(const Hunk* /*self*/, ResourceUse* mu)
    const noexcept
{
  this->spill->add_resource_use(mu);
}

void
Hunk::WithSpillMixin::on_core_detached(QueryCache* qc) noexcept
{
  // We can lose the spill record if we move the spill record into a
  // new core.
  if (this->spill)
    this->spill->obliterate(qc);
}

void
Hunk::WithSpillMixin::deflate(Hunk* self)
{
  assume(this->spill);
  self->change_core([&]{
    return SpilledCore(self->get_size(),
                       self->get_dtype().addref(),
                       std::move(this->spill));
  });
}

Hunk::MmapCoreBase::MmapCoreBase(
    MmapShared storage,
    unique_dtype dtype,
    npy_intp nelem) noexcept
    : nelem(nelem),
      dtype(std::move(dtype)),
      storage(std::move(storage))
{
  size_t elsize = static_cast<size_t>(this->dtype->elsize);
  size_t xnelem = static_cast<size_t>(nelem);
  size_t nbytes = mul_raise_on_overflow_safe_only(elsize, xnelem);
  assume(nbytes <= this->storage.nbytes());
}

int
Hunk::MmapCoreBase::py_traverse(visitproc visit, void* arg) const noexcept
{
  Py_VISIT(this->dtype.get());
  return 0;
}

unique_pyarray
Hunk::MmapCoreBase::view_of_the_mmap(bool writeable)
{
  return make_memory_view(
      this->storage.addr(),
      this->nelem,
      this->dtype.addref(),
      writeable);
}

unique_pyarray
Hunk::MmapCoreBase::inflate(Hunk* self)
{
  return self->pin_new_naked_view(
      this->view_of_the_mmap(/*writeable=*/!self->is_frozen()));
}

Hunk::MmapCore::MmapCore(
    MmapShared storage,
    unique_dtype dtype,
    npy_intp nelem) noexcept
    : MmapCoreBase(std::move(storage),
                   std::move(dtype),
                   nelem)
{}

void
Hunk::MmapCore::deflate(Hunk* self)
{
  self->deflate_memory_change_core(
      this->storage.addr(),
      this->nelem,
      this->dtype);
}

Hunk::MmapCoreWithSpill::MmapCoreWithSpill(
    MmapShared storage,
    unique_dtype dtype,
    npy_intp nelem,
    std::unique_ptr<SpillRecord> spill) noexcept
    : MmapCoreBase(std::move(storage),
                   std::move(dtype),
                   nelem),
      WithSpillMixin(std::move(spill))
{}

void
Hunk::MmapCoreWithSpill::add_resource_use(const Hunk* self,
                                          ResourceUse* mu)
    const noexcept
{
  this->MmapCoreBase::add_resource_use(self, mu);
  this->WithSpillMixin::add_resource_use(self, mu);
}

void
Hunk::MmapCoreWithSpill::on_core_detached(QueryCache* qc) noexcept
{
  this->MmapCoreBase::on_core_detached(qc);
  this->WithSpillMixin::on_core_detached(qc);
}

void
Hunk::MmapCoreBase::copy_or_pin(Hunk* self,
                                CopyOrPin* cop,
                                const npy_intp offset,
                                const npy_intp nelem)
{
  // Can't deal with non-zero offset: mremap lets us change length,
  // but not starting position.
  if (offset != 0 || !mmap_aligned_p(cop->get_cur_byte_pos())) {
    this->CoreBase::copy_or_pin(self, cop, offset, nelem);
    return;
  }
  assume(0 <= nelem && nelem <= this->nelem);
  // TODO(dancol): more efficient division
  assume(page_size() % this->dtype->elsize == 0);
  const npy_intp nelem_per_page = page_size() / this->dtype->elsize;
  const npy_intp nelem_whole_pages = nelem / nelem_per_page;
  const npy_intp nelem_dup = nelem_whole_pages * nelem_per_page;
  const npy_intp nbytes_dup = nelem_dup * this->dtype->elsize;
  this->storage.dup(cop->get_cur_ptr(), nbytes_dup).release();
  cop->note_shared_bytes(nbytes_dup);
  cop->add_pinned_array(self->inflate());  // For bookkeeping
  cop->advance_nelem(nelem_dup);
  assume(nelem_dup <= nelem);
  if (nelem_dup < nelem)
    cop->copy_and_advance(
        get_npy_slice(
            this->view_of_the_mmap(/*writeable=*/false),
            nelem_dup, nelem));
}

void
Hunk::MmapCoreBase::on_core_detached(QueryCache* qc) noexcept
{
  qc->donate_mmap(std::move(this->storage));
}

void
Hunk::MmapCore::resize(Hunk* self, npy_intp new_nelem)
{
  assume(this == self->core.get_if<MmapCore>());
  assume(!self->is_pinned() && !self->is_frozen());
  // If this hunk is less than a third full (or whatever the QC ration
  // is), copy to a new hunk.  TODO(dancol): is this the
  // right heuristic?
  assume(new_nelem >= 0);

  QueryCache* qc = self->qc.get();
  assume(qc);
  bool full_enough;

  size_t new_nelem_x;
  full_enough =
      mul_overflow(static_cast<size_t>(new_nelem),
                   static_cast<size_t>(qc->get_config().fill_ratio),
                   &new_nelem_x)
      || new_nelem_x > this->storage.nbytes();

  if (full_enough) {
    this->nelem = new_nelem;
    return;
  };

  auto elsize = static_cast<size_t>(this->dtype->elsize);
  size_t new_nbytes =
      mul_raise_on_overflow_safe_only(
          elsize,
          static_cast<size_t>(new_nelem));

  if (qc->should_use_heap(new_nbytes)) {
    self->change_core([&]{
        return NumpyCore(
            copy_pyarray(
                get_npy_slice(
                    this->view_of_the_mmap(/*writeable=*/false),
                    0, new_nelem)));
    });
    return;
  }

  ResourceUse old_mu = self->get_resource_use();
  qc->resize_mmap(&this->storage, new_nbytes);
  this->nelem = new_nelem;
  ResourceUse new_mu = self->get_resource_use();
  self->after_resource_use_changed(old_mu, new_mu);
}

void
Hunk::MmapCoreBase::add_resource_use(const Hunk* self, ResourceUse* mu)
    const noexcept
{
  mu->memory_nbytes += this->storage.nbytes();
}



Hunk::MegaCore::MegaCore(Vector<unique_hunk> hunks)
    : sub_hunks(std::move(hunks))
{}

Hunk::CopyOrPin
Hunk::MegaCore::stitch(Hunk* self)
{
  dtype_ref dtype = this->get_dtype(self).notnull();
  npy_intp nelem = this->get_size(self);
  size_t nbytes = mul_raise_on_overflow(
      static_cast<size_t>(nelem),
      static_cast<size_t>(dtype->elsize));
  if (nbytes > std::numeric_limits<size_t>::max() / 2)
    _throw_pyerr_fmt(PyExc_ValueError,
                     "hunk too big: asked for %lld bytes",
                     static_cast<long long>(nbytes));
  CopyOrPin cop(nelem, dtype.addref());
  for (auto& sh : this->sub_hunks)
    sh->copy_or_pin(&cop, 0, sh->get_size());
  return cop;
}

int
Hunk::MegaCore::py_traverse(visitproc visit, void* arg) const noexcept
{
  for (auto& sh : this->sub_hunks)
    Py_VISIT(sh.get());
  if (this->pin_info)
    if (int ret = this->pin_info->py_traverse(visit, arg))
      return ret;
  return 0;
}

npy_intp
Hunk::MegaCore::get_size(const Hunk* /*self*/) const noexcept
{
  // We checked for overflow in initialization, so here, we can just
  // walk the just and add without checking here.  Resizing is limited
  // to shrinking.
  npy_intp size = 0;
  for (const auto& sh : this->sub_hunks)
    size += sh->get_size();
  return size;
}

dtype_ref
Hunk::MegaCore::get_dtype(const Hunk* /*self*/) const noexcept
{
  // We checked during initialization that we have consistent dtypes
  // and, that we have a non-empty sub-hunk list --- so here, we can
  // just grab the dtype of the first sub-hunk.
  return this->sub_hunks[0]->get_dtype();
}

unique_pyarray
Hunk::MegaCore::inflate(Hunk* self)
{
  // TODO(dancol): writeable support
  if (!self->is_frozen())
    throw_pyerr_msg(PyExc_RuntimeError,
                    "cannot inflate unfrozen MegaCore");

  if (!this->pin_info) {
    // We need to actually assign this->pin_info before
    // account_memory() so that a call to get_resource_use() from
    // inside qc->account_memory() reflects the additional memory
    // we allocated.
    this->pin_info = this->stitch(self);
    self->qc->account_memory(this->pin_info->get_unshared_nbytes(), self);
  }

  return self->pin_new_naked_view(
      make_memory_view(
          this->pin_info->get_start_ptr(),
          self->get_size(),
          self->get_dtype().addref()));
}

void
Hunk::MegaCore::on_unpin(const Hunk* self) const noexcept
{
  if (this->pin_info) {
    size_t private_nbytes = this->pin_info->get_unshared_nbytes();
    this->pin_info.reset();
    self->qc->unaccount_memory(private_nbytes);
  }
}

void
Hunk::MegaCore::copy_or_pin(Hunk*,
                            CopyOrPin* cop,
                            npy_intp offset,
                            npy_intp nelem)
{
  // We should never get here: we should have decomposed any
  // sub-mega-hunks during sub-hunk list creation.  But just in case,
  // DTRT here.
  for (auto& sh : this->sub_hunks) {
    if (nelem == 0)
      break;
    assume(nelem > 0);
    npy_intp sub_hunk_nelem = sh->get_size();
    npy_intp sub_hunk_offset = std::min(offset, sub_hunk_nelem);
    offset -= sub_hunk_offset;
    sub_hunk_nelem -= offset;
    if (sub_hunk_nelem) {
      sub_hunk_nelem = std::min(sh->get_size(), nelem);
      sh->copy_or_pin(cop, sub_hunk_offset, sub_hunk_nelem);
    }
    nelem -= sub_hunk_nelem;
  }
  assume(offset == 0);
  assume(nelem == 0);
}

void
Hunk::MegaCore::enumerate_children(const Hunk* /*self*/, const WalkCb& cb)
    const
{
  for (auto& sh : this->sub_hunks)
    cb(sh.get());
}

unique_hunk
Hunk::MegaCore::view(const Hunk* self, npy_intp low, npy_intp high)
    const
{
  HunkReblocker builder(low, high);
  for (auto& sh : this->sub_hunks) {
    if (builder.done())
      break;
    builder.add(sh.get());
  }
  if (!builder.done())
    throw_pyerr_msg(PyExc_RuntimeError, "impossible view");
  return builder.finish(self->qc.get());
}

void
Hunk::MegaCore::add_resource_use(const Hunk* self, ResourceUse* mu)
    const noexcept
{
  if (this->pin_info)
    mu->memory_nbytes += this->pin_info->get_unshared_nbytes();
}

void
Hunk::MegaCore::deflate(Hunk* self)
{
  // Do nothing: we deflate only the referenced cores
}



Hunk::NumpyCoreBase::NumpyCoreBase(unique_pyarray array) noexcept
    : backing_store(std::move(array.notnull()))
{}

int
Hunk::NumpyCoreBase::py_traverse(visitproc visit, void* arg)
    const noexcept
{
  Py_VISIT(this->backing_store.get());
  return 0;
}

unique_pyarray
Hunk::NumpyCoreBase::inflate(Hunk* self)
{
  return self->pin_naked_view_of(this->backing_store);
}

void
Hunk::NumpyCoreBase::add_resource_use(const Hunk* self, ResourceUse* mu)
    const noexcept
{
  mu->memory_nbytes +=
      npy_size1d(this->backing_store) *
      npy_dtype(this->backing_store)->elsize;
}

Hunk::NumpyCore::NumpyCore(unique_pyarray array) noexcept
    : NumpyCoreBase(std::move(array))
{}

void
Hunk::NumpyCore::deflate(Hunk* self)
{
  self->deflate_memory_change_core(
      npy_data_raw(this->backing_store),
      npy_size1d(this->backing_store),
      npy_dtype(this->backing_store));
}

void
Hunk::NumpyCore::resize(Hunk* self, npy_intp new_nelem)
{
  assume(!self->is_pinned() && !self->is_frozen());
  assume(new_nelem >= 0);
  size_t elsize = npy_dtype(this->backing_store)->elsize;
  npy_intp old_nelem = npy_size1d(this->backing_store);
  size_t old_nbytes =
      mul_raise_on_overflow(
          elsize,
          static_cast<size_t>(old_nelem));
  size_t new_nbytes =
      mul_raise_on_overflow_safe_only(
          elsize,
          static_cast<size_t>(new_nelem));

  QueryCache* qc = self->qc.get();
  if (qc->should_use_heap(new_nbytes)) {
    npy_resize(this->backing_store, new_nelem);
    if (old_nbytes < new_nbytes)
      qc->account_memory(new_nbytes - old_nbytes, self);
    else if (new_nbytes < old_nbytes)
      qc->unaccount_memory(old_nbytes - new_nbytes);
    return;
  }
  MmapShared storage = qc->allocate_mmap(new_nbytes);
  memcpy(storage.addr(),
         npy_data_raw(this->backing_store),
         old_nbytes);
  self->change_core([&]{
    return MmapCore(std::move(storage),
                    npy_dtype(this->backing_store).addref(),
                    new_nelem);
  });
}

Hunk::NumpyCoreWithSpill::NumpyCoreWithSpill(
    unique_pyarray array,
    std::unique_ptr<SpillRecord> spill) noexcept
    : NumpyCoreBase(std::move(array)),
      WithSpillMixin(std::move(spill))
{}

void
Hunk::NumpyCoreWithSpill::on_core_detached(QueryCache* qc) noexcept
{
  this->NumpyCoreBase::on_core_detached(qc);
  this->WithSpillMixin::on_core_detached(qc);
}

void
Hunk::NumpyCoreWithSpill::add_resource_use(const Hunk* self, ResourceUse* mu)
    const noexcept
{
  this->NumpyCoreBase::add_resource_use(self, mu);
  this->WithSpillMixin::add_resource_use(self, mu);
}



Hunk::BroadcastCore::BroadcastCore(unique_pyarray broadcaster,
                                   npy_intp nelem)
    : nelem(nelem),
      broadcaster(std::move(broadcaster.notnull()))
{
  assume(npy_ndim(this->broadcaster) == 1);
  assume(npy_size1d(this->broadcaster) == 1);
  assume(npy_flags(this->broadcaster) & NPY_ARRAY_OWNDATA);
}

int
Hunk::BroadcastCore::py_traverse(visitproc visit, void* arg)
    const noexcept
{
  Py_VISIT(this->broadcaster.get());
  return 0;
}

unique_pyarray
Hunk::BroadcastCore::inflate(Hunk* self)
{
  // Don't special-case nelem==1 to return broadcaster: that can
  // return an unexpectedly mutable array.
  assume(self->is_frozen());
  return npy_broadcast_to(
      self->pin_naked_view_of(this->broadcaster),
      this->nelem);
}

void
Hunk::BroadcastCore::copy_or_pin(Hunk* /*self*/,
                                 CopyOrPin* cop,
                                 npy_intp offset,
                                 npy_intp nelem)
{
  // Hopefully, we always coalesce broadcasted hunks by doing more
  // broadcasting.  But in case we can't, we still have a trick up our
  // sleeve: make a memory mapping full of our broadcasted value and
  // map that repeatedly into the stitched array we're building.
  const size_t mirror_nbytes = 4 * page_size();  // TODO(dancol): bigger?
  const pyarray_ref bcaster = this->broadcaster;
  const dtype_ref dtype = npy_dtype(bcaster);
  const size_t elsize = static_cast<size_t>(dtype->elsize);

  // Offset is irrelevant: broadcasts are the same everywhere
  (void) offset;

  assume(nelem <= this->nelem);
  if (nelem) {
    // Can we pad to the next page boundary via copying?
    size_t cop_pos = cop->get_cur_byte_pos();
    size_t bytes_to_alignment = mmap_round_size_up(cop_pos) - cop_pos;
    npy_intp elem_to_alignment =
        static_cast<npy_intp>(bytes_to_alignment / elsize);
    npy_intp elem_to_copy = std::min(nelem, elem_to_alignment);
    if (elem_to_copy) {
      cop->copy_bcast_and_advance(bcaster, elem_to_copy);
      nelem -= elem_to_copy;
    }
  }
  if (nelem * elsize >= mirror_nbytes) {
    MmapShared mirror = MmapShared::make(mirror_nbytes,
                                         cop->get_cur_ptr());
    FINALLY(mirror.release());
    npy_intp mirror_nelem = mirror_nbytes / elsize;
    unique_pyarray mirror_view = make_memory_view(
        mirror.addr(),
        mirror_nelem,
        dtype.addref());
    set_slice(mirror_view, 0, mirror_nelem, bcaster);
    cop->advance_nelem(mirror_nelem);
    nelem -= mirror_nelem;
    while (nelem >= mirror_nelem) {
      assume(mmap_aligned_p(cop->get_cur_byte_pos()));
      MmapShared mirror2 = mirror.dup(cop->get_cur_ptr());
      cop->note_shared_bytes(mirror2.nbytes());
      cop->advance_nelem(mirror_nelem);
      nelem -= mirror_nelem;
      mirror2.release();
    }
  }
  if (nelem)
    cop->copy_bcast_and_advance(bcaster, nelem);
}

unique_hunk
Hunk::BroadcastCore::view(const Hunk* self, npy_intp low, npy_intp high)
    const
{
  assume(0 <= low);
  assume(low <= high);
  assert(high <= self->get_size());
  npy_intp wanted_nelem = high - low;
  if (wanted_nelem == 1)
    return Hunk::convert_pyarray(self->qc.get(),
                                 this->broadcaster.notnull().addref());
  return Hunk::from_core(
      self->qc.get(),
      BroadcastCore(this->broadcaster.notnull().addref(), wanted_nelem));
}

void
Hunk::BroadcastCore::add_resource_use(const Hunk* self, ResourceUse* mu)
    const noexcept
{
  mu->memory_nbytes += npy_dtype(this->broadcaster)->elsize;
}

void
Hunk::BroadcastCore::deflate(Hunk* self)
{
  // Do nothing
}



Hunk::SubCore::SubCore(unique_hunk base,
                       npy_intp offset,
                       npy_intp nelem)
    : nelem(nelem),
      offset(offset),
      base(std::move(base.notnull()))
{
  if (safe_mode && !this->base->is_frozen())
    throw_pyerr_msg(PyExc_RuntimeError,
                    "cannot create view of unfrozen hunk");
  assume(this->offset >= 0);
  assume(this->nelem >= 0);
  assert(add_raise_on_overflow(this->offset, this->nelem)
         <= this->base->get_size());
}

int
Hunk::SubCore::py_traverse(visitproc visit, void* arg)
    const noexcept
{
  Py_VISIT(this->base.get());
  Py_VISIT(this->inflated_base.get());
  return 0;
}

unique_pyarray
Hunk::SubCore::inflate(Hunk* self)
{
  if (!self->is_frozen())
    throw_pyerr_msg(PyExc_RuntimeError,
                    "SubCore should never be unfrozen");
  bool kill_inflated_base = false;
  FINALLY(if (kill_inflated_base) this->inflated_base.reset());
  if (!this->inflated_base) {
    kill_inflated_base = true;
    unique_pyarray whole_inflated_base = this->base->inflate();
    this->inflated_base = get_npy_slice(
        whole_inflated_base,
        this->offset,
        this->offset + this->nelem);
    assert(npy_size1d(this->inflated_base) == this->nelem);
  }
  unique_pyarray wrapped = self->pin_naked_view_of(this->inflated_base);
  kill_inflated_base = false;
  return wrapped;
}

void
Hunk::SubCore::on_unpin(const Hunk* /*self*/) const noexcept
{
  this->inflated_base.reset();
}

void
Hunk::SubCore::copy_or_pin(Hunk* /*self*/,
                           CopyOrPin* cop,
                           npy_intp offset,
                           npy_intp nelem)
{
  assume(nelem <= this->nelem);
  this->base->copy_or_pin(cop, offset + this->offset, nelem);
}

unique_hunk
Hunk::SubCore::view(const Hunk* self, npy_intp low, npy_intp high)
    const
{
  assume(0 <= low);
  assume(low <= high);
  assume(high <= self->get_size());
  return Hunk::from_core(
      self->qc.get(),
      SubCore(this->base.addref(),
              this->offset + low,
              high - low));
}

void
Hunk::SubCore::add_resource_use(const Hunk* self, ResourceUse* mu)
    const noexcept
{
  // Do nothing.
}

void
Hunk::SubCore::deflate(Hunk* self)
{
  // Do nothing.
}



Hunk::Core
Hunk::WithSpillMixin::core_from_spill_record(Hunk* self)
{
  assume(this->spill);
  const npy_intp nelem = self->get_size();
  const dtype_ref dtype = self->get_dtype();
  assume(dtype);
  const size_t uncompressed_nbytes =
      static_cast<size_t>(nelem) *
      static_cast<size_t>(dtype->elsize);

  return this->spill->with_compressed_bytes(self->qc, [&](
      const void* compressed_buffer,
      size_t compressed_nbytes) {
    unique_pyarray out_array;
    optional<MmapShared> out_mmap;
    void* out_mem;

    if (self->qc->should_use_heap(uncompressed_nbytes)) {
      out_array = make_uninit_pyarray(dtype.addref(), nelem);
      out_mem = npy_data_raw(out_array);
    } else {
      out_mmap = self->qc->allocate_mmap(uncompressed_nbytes);
      out_mem = out_mmap->addr();
    }

    int ret = blosc_decompress_ctx(
        compressed_buffer,
        out_mem,
        uncompressed_nbytes,
        BloscArgs::numinternalthreads);
    if (ret <= 0)
      throw_pyerr_msg(PyExc_RuntimeError, "decompression error");
    if (ret < uncompressed_nbytes)
      throw_pyerr_msg(PyExc_RuntimeError, "decompression truncated");

    assert(!!out_array ^ !!out_mmap);

    return out_array
        ? Core(NumpyCoreWithSpill(std::move(out_array),
                                  std::move(this->spill)))
        : Core(MmapCoreWithSpill(std::move(*out_mmap),
                                 dtype.addref(),
                                 nelem,
                                 std::move(this->spill)));
  });
}

Hunk::SpilledCore::SpilledCore(npy_intp nelem,
                               unique_dtype dtype,
                               std::unique_ptr<SpillRecord> spill) noexcept
    : WithSpillMixin(std::move(spill)),
      nelem(nelem),
      dtype(std::move(dtype.notnull()))
{}

unique_pyarray
Hunk::SpilledCore::inflate(Hunk* self)
{
  self->change_core([&]{
    return this->core_from_spill_record(self);
  });
  return self->inflate();
}

void
Hunk::SpilledCore::on_core_detached(QueryCache* qc) noexcept
{
  this->CoreBase::on_core_detached(qc);
  this->WithSpillMixin::on_core_detached(qc);
}

int
Hunk::SpilledCore::py_traverse(visitproc visit, void* arg)
    const noexcept
{
  return 0;
}



unique_pyarray
Hunk::pin_new_naked_view(unique_pyarray view)
{
  if (!this->is_frozen() && !npy_writeable_p(view))
    throw_pyerr_msg(PyExc_RuntimeError,
                    "cannot pin readonly view in !frozen mode");
  assert(!npy_base(view));
  assert(!(npy_flags(view) & NPY_ARRAY_OWNDATA));
  npy_set_base(view, this->get_pinner());
  return view;
}

unique_pyarray
Hunk::pin_naked_view_of(pyarray_ref array)
{
  return this->pin_new_naked_view(
      make_naked_view(array, /*writeable=*/!this->is_frozen()));
}

unique_pyarray
Hunk::inflate()
{
  // N.B. the core's inflation routine may change the core type.
  unique_pyarray view = this->core.inflate(this);
  assume(npy_dtype(view) == this->get_dtype());
  assume(npy_size1d(view) == this->get_size());
  assume(npy_base(view) || (npy_flags(view) & NPY_ARRAY_OWNDATA));
  assume(npy_writeable_p(view) == !this->is_frozen());
  return view;
}

Hunk::Hunk(QueryCache* qc, Hunk::Core&& core)
    : HunkExtra(qc->new_hunk_score_override),
      qc(obj_pyref<QueryCache>(qc).notnull().addref()),
      core(std::move(core))
{
  // Add this hunk to the qc hunk list before doing memory accounting
  // so that we maintain the invariant that QC's memory counter is
  // always equal to the sum of the resource use of the
  // contained hunks.
  this->qc->hunks.push_back(*this);
  // qc->account_memory() can throw: if it does, make sure to remove
  // this hunk from QC's list of hunks, since in this case, our
  // destructor won't be run.
  bool success = false;
  FINALLY({
      if (!success)
        this->qc->hunks.erase(this->qc->hunks.iterator_to(*this));
    });
  qc->account_memory(this->get_resource_use().memory_nbytes, this);
  success = true;
}

Hunk::~Hunk() noexcept
{
  // Remove this hunk from the list of all hunks before unaccounting
  // memory: this way, we preserve the accounting invariant described
  // in Hunk::Hunk.
  ResourceUse mu = this->get_resource_use();
  this->qc->hunks.erase(this->qc->hunks.iterator_to(*this));
  qc->unaccount_memory(mu.memory_nbytes);
  this->core.on_core_detached(this->qc.get());
}

unique_hunk
Hunk::make_uninit(QueryCache* qc,
                  unique_dtype dtype,
                  npy_intp size)
{
  size_t nbytes = mul_raise_on_overflow(
      static_cast<size_t>(size),
      static_cast<size_t>(dtype->elsize));
  unique_hunk hunk =
      Hunk::from_core(
          qc,
          qc->should_use_heap(nbytes)
          ? Core(NumpyCore(make_uninit_pyarray(std::move(dtype), size)))
          : Core(MmapCore(qc->allocate_mmap(nbytes),
                          std::move(dtype),
                          size)));
  hunk->frozen = false;
  return hunk;
}

template<typename Functor>
void
Hunk::change_core(Functor&& make_new_core)
{
  using std::swap;
  ResourceUse old_mu = this->get_resource_use();
  Core new_core = make_new_core();
  swap(new_core, this->core);
  new_core.on_core_detached(this->qc.get());
  ResourceUse new_mu = this->get_resource_use();
  this->after_resource_use_changed(old_mu, new_mu);
}

unique_obj_pyref<HunkPinner>
Hunk::get_pinner()
{
  unique_obj_pyref<HunkPinner> pin = xaddref(this->pin);
  if (!pin) {
    bool success;
    FINALLY(if (!success) this->core.on_unpin(this));
    pin = make_pyobj<HunkPinner>(xaddref(this));
    this->pin = pin.get();
    success = true;
  }
  assume(this->is_pinned());
  return pin;
}

static
bool
contiguous_array_p(pyarray_ref array)
{
  int cflags = NPY_ARRAY_C_CONTIGUOUS | NPY_ARRAY_F_CONTIGUOUS;
  return (npy_flags(array) & cflags) == cflags;
}

// Try to find a hunk reference that is semantically equivalent to the
// whole array ARRAY.  In Hunk::convert_pyarray, if we find that our
// input array is just a proxy for a hunk, we return that hunk without
// doing other work.
unique_hunk
Hunk::try_get_equiv_hunk(QueryCache* qc, pyarray_ref array)
{
  // You might think that any intermediate view should get another
  // level of indirection, but you'd be wrong.  If we broadcast up to
  // nelem=2, the broadcast chain looks like V->N->H, where V is the
  // two-elementboradcast, N is the naked view, and H is the
  // HunkPinner.  If we rebroadcast this view down to one element, we
  // collapse it to N->H.
  assume(npy_ndim(array) == 1);
  if (Hunk* h = maybe_pinned_hunk(npy_base(array))) {
    if (h->qc != qc)
      return {};
    if (npy_size1d(array) == h->get_size() &&
        npy_dtype(array) == h->get_dtype() &&
        npy_strides(array)[0] == npy_dtype(array)->elsize) {
      h->freeze();
      return xaddref(h);
    }
  } else if (pyarray_ref bcaster = npy_get_broadcaster_raw(array)) {
    if (Hunk* bh = maybe_pinned_hunk(npy_base(bcaster))) {
      if (bh->qc != qc)
        return {};
      if (!bh->core.is_broadcasted(bh))
        return {};
      if (npy_size1d(array) != bh->get_size())
        return {};
      if (npy_strides(array)[0] != 0)
        return {};
      if (npy_dtype(array) != bh->get_dtype())
        return {};
      if (npy_data_raw(array) != npy_data_raw(bcaster))
        return {};
      assume(bh->is_frozen());  // Broadcast hunks cannot be mutated.
      return xaddref(bh);
    }
  }
  return {};
}

// If ARRAY is a broadcast, just copy the underlying broadcasted value
// into a new array.  It's not worth doing anything fancy for one row.
// If ARRAY is an exact handle to a broadcasted Hunk and not a
// sub-view of the broadcast, try_get_equiv_hunk should have already
// short-circuited us.
optional<Hunk::Core>
Hunk::try_broadcast(pyarray_ref array)
{
  pyarray_ref bcaster = npy_get_broadcaster_raw(array);
  if (!bcaster)
    return {};
  unique_pyarray own_bcaster;
  if (!own_bcaster && (npy_flags(bcaster) & NPY_ARRAY_OWNDATA))
    own_bcaster = bcaster.addref();
  if (!own_bcaster)
    if (pyref bcaster_base = npy_base(bcaster))
      if (Hunk* bcaster_hunk = maybe_pinned_hunk(bcaster_base))
        own_bcaster = bcaster_hunk->core.get_broadcast_base(
            bcaster_hunk).addref();
  if (!own_bcaster ||
      npy_ndim(bcaster) == 0 ||
      !(npy_flags(own_bcaster) & NPY_ARRAY_OWNDATA))
    own_bcaster = npy_flatten(bcaster);
  return BroadcastCore(std::move(own_bcaster), npy_size1d(array));
}

struct ViewInfo {
  pyarray_ref base;
  npy_intp offset_nelem;
};

optional<ViewInfo>
try_decode_view(pyarray_ref array)
{
  npy_intp asize = npy_size1d(array);
  if (!contiguous_array_p(array))
    return {};
  pyref base = npy_base(array);
  if (!base)
    return {};
  if (!is_base_ndarray(base))
    return {};
  pyarray_ref abase = base.as_unsafe<PyArrayObject>();
  if (npy_ndim(abase) != 1)
    return {};
  if (!contiguous_array_p(abase))
    return {};
  npy_intp stride = npy_strides(abase)[0];
  if (!stride || stride != npy_strides(array)[0])
    return {};
  assume(stride > 0);
  npy_intp bsize = npy_size1d(abase);
  assume(bsize >= asize);
  char* data = npy_data<char>(array);
  char* base_data = npy_data<char>(abase);
  npy_intp offset_bytes = data - base_data;
  assume(offset_bytes >= 0);
  assume(offset_bytes % stride == 0);
  npy_intp offset_nelem = offset_bytes / stride;
  assume(offset_nelem <= bsize - asize);
  return ViewInfo {
    abase,
    offset_nelem,
  };
}

// If ARRAY is a view of a hunk-backed array, make a hunk core that
// knows about the connection.
optional<Hunk::Core>
Hunk::try_subcore_or_view(QueryCache* qc,
                          unique_pyarray& array)
{
  npy_intp asize = npy_size1d(array);
  // We're better off just copying very small views.
  if (asize < view_min_elements)
    return {};
  optional<ViewInfo> vi = try_decode_view(array);
  if (!vi)
    return {};
  // Now we're looking at a view.
  assume(vi->base);
  assume(vi->offset_nelem >= 0);

  unique_hunk base_hunk = try_get_equiv_hunk(qc, vi->base);
  // If this view is backed by a hunk, use SubCore to properly work
  // with that hunk.
  if (base_hunk)
    return SubCore(std::move(base_hunk),
                   vi->offset_nelem,
                   npy_size1d(array));
  // If the view is backed by something weird, just copy.
  if (npy_base(vi->base))
    return {};
  // If the view would retain more than a whole block of data,
  // just copy.
  if (npy_size1d(vi->base) > qc->get_block_size())
    return {};
  // All the checks passed.  Let's treat this view like a
  // normal array.
  return NumpyCore(std::move(array));
}

// If ARRAY has no unusual backing storage, just use it as-is.
optional<Hunk::Core>
Hunk::try_forward(unique_pyarray& array)
{
  if (pyref base = npy_base(array)) {
    if (!base)
      return {};
    if (!is_base_ndarray(base))
      return {};
    pyarray_ref abase = base.as_unsafe<PyArrayObject>();
    if (npy_ndim(array) != 1 || npy_ndim(abase) != 1)
      return {};
    if (npy_size1d(abase) != npy_size1d(array))
      return {};
    if (!contiguous_array_p(array) || !contiguous_array_p(abase))
      return {};
    if (npy_dtype(abase) != npy_dtype(array) &&
        !dtype_equiv(npy_dtype(abase), npy_dtype(array)))
      return {};
  }
  return NumpyCore(std::move(array));

}

unique_hunk
Hunk::convert_pyarray(QueryCache* qc, unique_pyarray array)
{
  check_pytype_exact(pytype_of<PyArrayObject>(), array);
  array = collapse_useless_views(std::move(array));
  if (npy_ndim(array) != 1)
    throw_pyerr_msg(PyExc_TypeError, "hunks can store only 1d arrays");
  if (unique_hunk hunk = try_get_equiv_hunk(qc, array))
    return hunk;
  unique_dtype dtype = npy_dtype(array).notnull().addref();
  optional<Core> core;
  if (!core)
    core = try_broadcast(array);
  if (!core)
    core = try_subcore_or_view(qc, array);
  if (!core)
    core = try_forward(array);
  if (!core)
    core = NumpyCore(copy_pyarray(array));
  assume(core.has_value());
  return Hunk::from_core(qc, std::move(*core));
}

unique_hunk
Hunk::retrieve_backing_hunk(pyarray_ref array) noexcept
{
  pyref obj = npy_base(array);
  assert(obj);
  assert(isinstance_exact(obj, pytype_of<HunkPinner>()));
  unique_hunk h = obj.as_unsafe<HunkPinner>()->hunk_addref();
  assert(npy_size1d(array) == h->get_size());
  assert(npy_dtype(array) == h->get_dtype());
  assert(npy_strides(array)[0] == npy_dtype(array)->elsize);
  return h;
}

int
Hunk::py_traverse(visitproc visit, void* arg) const noexcept
{
  return this->core.py_traverse(visit, arg);
}

void
Hunk::after_resource_use_changed(ResourceUse old_mu, ResourceUse new_mu)
{
  if (old_mu.memory_nbytes < new_mu.memory_nbytes)
    qc->account_memory(new_mu.memory_nbytes - old_mu.memory_nbytes, this);
  else if (new_mu.memory_nbytes < old_mu.memory_nbytes)
    qc->unaccount_memory(old_mu.memory_nbytes - new_mu.memory_nbytes);
}

void
Hunk::deflate()
{
  assume(!this->is_pinned());
  this->core.deflate(this);
}

void
HunkExtraDebug::set_score_override(HunkScore score_override)
{
  this->score_override = score_override;
}

void
HunkExtraOpt::set_score_override(HunkScore /*score_override*/)
{
  throw_pyerr_msg(PyExc_AssertionError,
                  "score override not available in optimized builds");
}

void
Hunk::set_score_override(HunkScore score_override)
{
  this->HunkExtra::set_score_override(score_override);
}

const char*
Hunk::get_core_name() const noexcept
{
  return this->core.get_core_name(this);
}



unique_hunk /* nullable */
HunkReblocker::finish(QueryCache* qc)
{
  assert(!this->nelem_remaining());
  if (this->new_sub_hunks.empty())
    return {};
  if (this->new_sub_hunks.size() == 1)
    return std::move(this->new_sub_hunks[0]);
  return Hunk::from_core(qc,
                         Hunk::MegaCore(std::move(this->new_sub_hunks)));
}

void
HunkReblocker::add_1(unique_hunk hunk)
{
  // Don't create trees of hunks: decompose composites.
  if (hunk->is_composite()) {
    hunk->enumerate_children([&](Hunk* sub_hunk) {
      this->add_1(xaddref(sub_hunk));
    });
    return;
  }

  unique_hunk merged;
  if (!this->new_sub_hunks.empty() &&
      (merged = Hunk::try_merge(this->new_sub_hunks.back().get(),
                                hunk.get())))
  {
    this->new_sub_hunks.back() = std::move(merged);
  } else {
    this->new_sub_hunks.push_back(std::move(hunk));
  }
}

unique_hunk
HunkReblocker::add(Hunk* sh)
{
  unique_hunk out;
  const npy_intp sh_size = sh->get_size();
  assume(sh_size >= 0);
  assume(this->skip >= 0);
  assume(this->nelem >= 0);
  if (sh_size <= this->skip) {
    this->skip -= sh_size;
  } else {
    const npy_intp data_start = this->skip;
    const npy_intp data_avail = sh_size - data_start;
    const npy_intp data_used = std::min(data_avail, this->nelem);
    const npy_intp data_end = data_start + data_used;
    if (data_used) {
      this->add_1(sh->view(data_start, data_end));
      this->nelem -= data_used;
    }
    if (data_end < sh_size)
      out = sh->view(data_end, sh_size);
  }
  return out;
}



static
unique_pyref
Hunk_get_core_name(PyObject* self, void*)
{
  return make_pystr(pyref(self).as_unsafe<Hunk>()->get_core_name());
}

PyGetSetDef Hunk::pygetset[] = {
  make_getset("core_name",
              "Get the name of the currently-used core",
              wraperr<Hunk_get_core_name>()),
  { 0 }
};

PyMethodDef Hunk::pymethods[] = {
  AUTOMETHOD(&Hunk::set_score_override,
             "For testing, override QC spill core",
             (HunkScore, score_override)
  ),
  AUTOMETHOD(&Hunk::inflate,
             "Make an array out of a hunk",
  ),
  { 0 },
};

PyTypeObject Hunk::pytype =
    make_py_type<Hunk>(
        "dctv._native.Hunk",
        "Internal handle to block data",
        [](PyTypeObject* t) {
          t->tp_getset = Hunk::pygetset;
          t->tp_methods = Hunk::pymethods;
        });



PyMemberDef HunkPinner::pymembers[] = {
  make_memberdef("hunk",
                 T_OBJECT,
                 (offsetof(HunkPinner, hunk) +
                  unique_pyref::get_pyval_offset()),
                 READONLY,
                 "The hunk that this pinner pins"),
  {}
};

PyTypeObject HunkPinner::pytype =
    make_py_type<HunkPinner>(
        "dctv._native.HunkPinner",
        "Internal hunk migration preventor",
        [](PyTypeObject* t){
          t->tp_members = HunkPinner::pymembers;
        });

void
init_hunk(pyref m)
{
  register_type(m, &Hunk::pytype);
  register_type(m, &HunkPinner::pytype);
}

}  // namespace dctv
