// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>
#include <stdint.h>
#include <string_view>

namespace dctv {

template<auto FreeFunction>
struct PcreDeleter final {
  template<typename T>
  void operator()(T* thing) const noexcept {
    FreeFunction(thing);
  }
};

using unique_pcre2_code =
    std::unique_ptr<pcre2_code,
                    PcreDeleter<pcre2_code_free>>;
using unique_pcre2_general_context =
    std::unique_ptr<pcre2_general_context,
                    PcreDeleter<pcre2_general_context_free>>;
using unique_pcre2_match_data =
    std::unique_ptr<pcre2_match_data,
                    PcreDeleter<pcre2_match_data_free>>;
using unique_pcre2_compile_context =
    std::unique_ptr<pcre2_compile_context,
                    PcreDeleter<pcre2_compile_context_free>>;
using unique_pcre2_match_context =
    std::unique_ptr<pcre2_match_context,
                    PcreDeleter<pcre2_match_context_free>>;
using unique_pcre2_jit_stack =
    std::unique_ptr<pcre2_jit_stack,
                    PcreDeleter<pcre2_jit_stack_free>>;

template<typename T>
T get_pcre2_info(uint32_t what, const pcre2_code* cpat);
unique_pcre2_code compile_pcre2_pattern(std::string_view pattern,
                                        uint32_t options);
void jit_compile_pcre2(pcre2_code* cpat,
                       uint32_t options=PCRE2_JIT_COMPLETE);

unique_pcre2_match_data make_pcre2_match_data(
    const pcre2_code* cpat);

DCTV_NORETURN_ERROR
void throw_pcre2_error_nogil(
    int errorcode,
    std::string_view api,
    const std::string_view* pattern = nullptr,
    const size_t* erroroffset = nullptr);

}  // namespace dctv

#include "pcreutil-inl.h"
