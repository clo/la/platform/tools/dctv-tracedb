// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "mmap.h"

#include <sys/mman.h>

#include "pyutil.h"  // For exceptions

namespace dctv {

const size_t cached_page_size = []{
  size_t page_size = sysconf(_SC_PAGESIZE);
  if (!is_pow2(page_size))
    abort();
  return page_size;
}();

void
Mmap::do_unmap() noexcept
{
  assume(this->nbytes_);
  if (munmap(this->addr_, this->nbytes_))
    abort();  // Might run out of VMAs?
  this->nbytes_ = 0;
  this->addr_ = nullptr;
}

Mmap
Mmap::map(void* addr,
          size_t nbytes,
          int prot,
          int flags,
          int fd,
          off_t offset)
{
  nbytes = mmap_round_size_up(nbytes);
  void* map = mmap(addr, nbytes, prot, flags, fd, offset);
  if (map == MAP_FAILED)
    throw_from_errno();
  return Mmap(map, nbytes);
}

Mmap
Mmap::anonymous(size_t nbytes)
{
  Mmap map = Mmap::map(
      /*addr=*/nullptr,
      nbytes,
      PROT_READ | PROT_WRITE,
      MAP_PRIVATE | MAP_ANONYMOUS,
      /*fd=*/-1,
      /*offset=*/0);
  if (safe_mode && is_py_debug())
    memset(map.addr(), map.nbytes(), 0xFC);
  return map;
}

MmapShared
MmapShared::make(size_t nbytes, void* new_addr)
{
  return MmapShared(
      Mmap::map(new_addr,
                nbytes,
                PROT_READ | PROT_WRITE,
                MAP_SHARED | MAP_ANONYMOUS |
                (new_addr ? MAP_FIXED : 0),
                /*fd=*/-1,
                /*offset=*/0));
}

MmapShared
MmapShared::of_file(int fd, size_t nbytes, int prot)
{
  return MmapShared(
      Mmap::map(/*addr=*/nullptr,
                nbytes,
                prot,
                MAP_SHARED,
                fd,
                /*offset=*/0));
}

MmapShared
MmapShared::dup(void* new_addr, size_t new_nbytes)
{
  if (!this->nbytes())
    throw_pyerr_msg(PyExc_ValueError, "memory mapping invalid");
  // TODO(dancol): non-Linux equivalent (macOS: Mach?)
  if (!new_nbytes)
    new_nbytes = this->nbytes();
  // This operation "dups" the map into a new spot in memory; it's as
  // if we had an explicit memfd associated with the mapping and
  // called mmap() on that FD a second time.
  void* map = mremap(this->addr(),
                     /*old_size=*/0,  // NOLINT
                     /*new_size=*/new_nbytes,  // NOLINT
                     (MREMAP_MAYMOVE |
                      (new_addr ? MREMAP_FIXED : 0)),
                     new_addr);
  if (map == MAP_FAILED)
    throw_from_errno();
  return MmapShared(map, this->nbytes());
}

void
MmapShared::resize(size_t new_nbytes)
{
  new_nbytes = mmap_round_size_up(new_nbytes);
  // TODO(dancol): non-Linux equivalent (macOS: Mach?)
  void* map = mremap(this->addr(),
                     this->nbytes(),
                     new_nbytes,
                     MREMAP_MAYMOVE);
  if (map == MAP_FAILED)
    throw_from_errno();
  this->addr_ = map;
  this->nbytes_ = new_nbytes;
}

void
init_mmap_early()
{}

}  // namespace dctv
