// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename T>
PyRefSpan<T>::PyRefSpan() noexcept
    : data_(nullptr), size_(0)
{}

template<typename T>
PyRefSpan<T>::PyRefSpan(T* const* data_, size_t size) noexcept
    : data_(data_), size_(size)
{}

template<typename T>
PyRefSpan<T>::PyRefSpan(T* const* begin, T* const* end) noexcept
    : data_(begin), size_(assume(end >= begin), end - begin)
{}

template<typename T>
obj_pyref<T>
PyRefSpan<T>::operator[](size_t idx) const noexcept
{
  return this->data_[idx];
}

template<typename T>
size_t
PyRefSpan<T>::size() const noexcept
{
  return this->size_;
}

template<typename T>
T* const*
PyRefSpan<T>::data() const noexcept
{
  return this->data_;
}

// These functions declared in pyutil.h because C++ still doesn't have
// extension classes.

template<typename T>
PyRefSpan<T>
unique_obj_pyref<T>::as_span() const noexcept
{
  return PyRefSpan(&this->ref, 1);
}

template<typename T>
PyRefSpan<T>
obj_pyref<T>::as_span() const noexcept
{
  return PyRefSpan(&this->ref, 1);
}

}  // namespace dctv
