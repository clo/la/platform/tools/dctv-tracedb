// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <iterator>
#include <type_traits>
#include <utility>

#include "pyutil.h"

namespace dctv {

template<typename Sequence>
struct pyseq {
  using ref = obj_pyref<Sequence>;
  using unique_ref = unique_obj_pyref<Sequence>;

  static unique_ref make_uninit(size_t);

  static inline pyref get(ref, Py_ssize_t idx) noexcept;
  static inline void set_unsafe(ref, Py_ssize_t idx, unique_pyref value)
      noexcept;
  static inline void set_uninit(ref, Py_ssize_t idx, unique_pyref value)
      noexcept;
  static inline Py_ssize_t size_raw(ref) noexcept;
  static inline size_t size(ref) noexcept;

  static inline void set(ref, Py_ssize_t idx, unique_pyref value)
      noexcept;

  template<typename... Args>
  static unique_ref of(Args&&... args);

  template<typename Iterator, typename Transform>
  static unique_ref make(Iterator it,
                         const Iterator& end,
                         Transform&& transform);

  template<typename Iterator>
  static unique_ref make(Iterator it, const Iterator& end);

  template<typename Container, typename Transform>
  static unique_ref from(const Container& container,
                         Transform&& transform);
  template<typename Container>
  static unique_ref from(const Container& container);

  template<typename Container, typename Transform>
  static unique_ref from(Container& container,
                         Transform&& transform);
  template<typename Container>
  static unique_ref from(Container& container);

  template<typename Container, typename Transform>
  static unique_ref from(Container&& container,
                         Transform&& transform);
  template<typename Container>
  static unique_ref from(Container&& container);

};

DEFINE_PYTRAITS(PyTupleObject, PyTuple_Type);
DEFINE_PYTRAITS(PyListObject, PyList_Type);

using pytuple = pyseq<PyTupleObject>;
using pylist = pyseq<PyListObject>;

inline void init_pyseq(pyref m) {}

}  // namespace dctv

#include "pyseq-inl.h"
