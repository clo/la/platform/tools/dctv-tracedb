// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "span.h"

#include "fmt.h"
#include "npy.h"

namespace dctv {

String
ts2tracets(TimeStamp ts)
{
  int64_t ns_in_second = 1000L * 1000L * 1000L;

  return fmt("%06lu.%06lu/%lld",
             ts / ns_in_second,
             (ts % ns_in_second) / 1000,
             static_cast<long long>(ts));
}

void
init_span(pyref m)
{
  register_constant(m, "DTYPE_TS", type_descr<TimeStamp>());
  register_constant(m, "DTYPE_DURATION", type_descr<TimeStamp>());
  register_constant(m, "DTYPE_CLONE_FLAGS", type_descr<CloneFlags>());
  register_constant(m, "DTYPE_ID_VALUE", type_descr<IdValue>());
  register_constant(m, "DTYPE_OOM_SCORE", type_descr<OomScore>());
  register_constant(m, "DTYPE_SNAPSHOT_NUMBER",
                    type_descr<SnapshotNumber>());
  register_constant(m, "DTYPE_MEMORY_SIZE", type_descr<MemorySize>());
  register_constant(m, "DTYPE_RAW_THREAD_STATE_FLAGS",
                    type_descr<RawThreadStateFlags>());
  register_constant(m, "DTYPE_CPU_NUMBER",
                    type_descr<CpuNumber>());
}

}  // namespace dctv
