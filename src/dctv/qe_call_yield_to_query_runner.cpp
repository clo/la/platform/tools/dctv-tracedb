// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "qe_call_yield_to_query_runner.h"

#include "operator_context.h"
#include "pylog.h"
#include "query_execution.h"

namespace dctv {

struct QeCallYieldToQueryRunner final : QeCall {
  explicit inline QeCallYieldToQueryRunner(unique_pyref what) noexcept;
  unique_pyref do_it(OperatorContext* oc) override;
  Score compute_score(const OperatorContext* oc) const override;
  int py_traverse(visitproc visit, void* arg) const noexcept override;
 private:
  unique_pyref what;
};

QeCallYieldToQueryRunner::QeCallYieldToQueryRunner(unique_pyref what)
    noexcept
    : what(std::move(what))
{}

unique_pyref
QeCallYieldToQueryRunner::do_it(OperatorContext* oc)
{
  unique_pyref yield_value = std::move(this->what);
  oc->communicate(addref(Py_None));  // May free *this!
  return yield_value;
}

Score
QeCallYieldToQueryRunner::compute_score(const OperatorContext* oc) const
{
  Score score = oc->compute_basic_score();
  score.state = OperatorState::YIELDING;
  return score;
}

int
QeCallYieldToQueryRunner::py_traverse(visitproc visit, void* arg)
    const noexcept
{
  Py_VISIT(this->what.get());
  return 0;
}

unique_pyref
make_qe_call_yield_to_query_runner(unique_pyref what)
{
  return make_qe_call(
      std::make_unique<QeCallYieldToQueryRunner>(std::move(what)));
}

}  // namespace dctv
