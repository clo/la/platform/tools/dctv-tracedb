// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"

#include <condition_variable>
#include <mutex>

#include "pyutil.h"

// Utilities for Python threading.

namespace dctv {

template<typename Functor>
inline auto with_gil_acquired(Functor&& functor)
    noexcept(noexcept(AUTOFWD(functor)()));

template<typename Functor>
inline auto with_gil_released(Functor&& functor)
    noexcept(noexcept(AUTOFWD(functor)()));

template<typename Functor>
inline auto with_gil_released_if(bool condition,
                                 Functor&& functor)
    noexcept(noexcept(AUTOFWD(functor)()));

inline void assert_gil_held() noexcept;

struct Mutex {
  void lock() noexcept;
  void unlock() noexcept;

 private:
  friend struct UniqueLock;
  std::mutex mutex;
};

struct UniqueLock {
  explicit inline UniqueLock(Mutex& mutex) noexcept;
 private:
  friend struct Condition;
  std::unique_lock<std::mutex> lock;
};

struct Condition {
  template<typename Predicate>
  void wait_for(UniqueLock& lock,
                Predicate&& predicate) noexcept;

  void notify_all() noexcept;
  void notify_one() noexcept;

 private:
  std::condition_variable condition;
};

// Check (in debug mode) that the GIL is held during an object's birth
// and death.
struct GilHeldCtorDtor {
  inline GilHeldCtorDtor() noexcept;
  inline ~GilHeldCtorDtor() noexcept;
};

template<typename T>
String repr_nogil(T&& arg);

inline void init_pythread(pyref m) {}

}  // namespace dctv

#include "pythread-inl.h"
