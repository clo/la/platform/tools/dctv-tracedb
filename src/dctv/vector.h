// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include "dctv.h"
#include <vector>

namespace dctv {

template<typename T>
struct VectorHelper final {
  using type = std::vector<T, PyAllocator<T>>;
};

struct BoolWrapper final {
  BoolWrapper() noexcept = default;
  BoolWrapper(bool value) noexcept : value(value) {}  // NOLINT
  operator bool() const { return this->value; }  // NOLINT
 private:
  bool value = false;
};
static_assert(sizeof (BoolWrapper) == sizeof (bool));
static_assert(alignof (BoolWrapper) == alignof (bool));

template<>
struct VectorHelper<bool> final {
  using type = typename VectorHelper<BoolWrapper>::type;
};
template<typename T>
using Vector = typename VectorHelper<T>::type;

}  // namespace dctv
