// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename Sequence>
pyref
pyseq<Sequence>::get(ref seq, Py_ssize_t idx) noexcept
{
  if constexpr(std::is_same_v<Sequence, PyTupleObject>) {
    return PyTuple_GET_ITEM(seq.get(), idx);
  } else if constexpr(std::is_same_v<Sequence, PyListObject>) {  // NOLINT
    return PyList_GET_ITEM(seq.get(), idx);
  } else {  // NOLINT
    return typename errhack<Sequence>::bad();
  }
}

template<typename Sequence>
void
pyseq<Sequence>::set_unsafe(ref seq, Py_ssize_t idx, unique_pyref val)
    noexcept
{
  if constexpr(std::is_same_v<Sequence, PyTupleObject>) {
    PyTuple_SET_ITEM(seq.get(), idx, val.release());
  } else if constexpr(std::is_same_v<Sequence, PyListObject>) {  // NOLINT
    PyList_SET_ITEM(seq.get(), idx, val.release());
  } else {  // NOLINT
    typename errhack<Sequence>::bad();
  }
}

template<typename Sequence>
Py_ssize_t
pyseq<Sequence>::size_raw(ref seq) noexcept
{
  if constexpr(std::is_same_v<Sequence, PyTupleObject>) {
    return PyTuple_GET_SIZE(seq.get());
  } else if constexpr(std::is_same_v<Sequence, PyListObject>) {  // NOLINT
    return PyList_GET_SIZE(seq.get());
  } else {  // NOLINT
    typename errhack<Sequence>::bad();
  }
}

template<typename Sequence>
size_t
pyseq<Sequence>::size(ref seq) noexcept
{
  Py_ssize_t size = size_raw(seq);
  assume(size >= 0);
  return static_cast<size_t>(size);
}

template<typename Sequence>
void
pyseq<Sequence>::set_uninit(ref seq, Py_ssize_t idx, unique_pyref val)
    noexcept
{
  assume(!get(seq, idx));
  set_unsafe(seq, idx, std::move(val.notnull()));
}

template<typename Sequence>
void
pyseq<Sequence>::set(ref seq, Py_ssize_t idx, unique_pyref val)
    noexcept
{
  adopt(get(seq, idx).get());
  set_unsafe(seq, idx, std::move(val));
}

template<typename Sequence>
template<typename... Args>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::of(Args&&... args)
{
  unique_ref seq = make_uninit(sizeof...(Args));
  size_t i = 0;
  (... ,
   set_uninit(seq, i++,
              addref(std::forward<Args>(args)).notnull()));
  return seq;
}

template<typename Sequence>
template<typename Iterator, typename Transform>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::make(Iterator it,
                      const Iterator& end,
                      Transform&& transform)
{
  unique_ref seq = make_uninit(std::distance(it, end));
  for (Py_ssize_t idx = 0; it != end; ++it) {
    unique_pyref ref = addref(transform(*it)).notnull();
    set_uninit(seq, idx++, std::move(ref));
  }
  return seq;
}

template<typename Sequence>
template<typename Iterator>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::make(Iterator it, const Iterator& end)
{
  unique_ref seq = make_uninit(std::distance(it, end));
  for (Py_ssize_t idx = 0; it != end; ++it) {
    unique_pyref ref = addref(*it).notnull();
    set_uninit(seq, idx++, std::move(ref));
  }
  return seq;
}

template<typename Sequence>
template<typename Container, typename Transform>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::from(const Container& container,
                      Transform&& transform)
{
  return make(container.begin(),
              container.end(),
              std::forward<Transform>(transform));
}

template<typename Sequence>
template<typename Container>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::from(const Container& container)
{
  return make(container.begin(), container.end());
}

template<typename Sequence>
template<typename Container, typename Transform>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::from(Container& container,
                      Transform&& transform)
{
  return make(container.begin(),
              container.end(),
              std::forward<Transform>(transform));
}

template<typename Sequence>
template<typename Container>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::from(Container& container)
{
  return make(container.begin(), container.end());
}

template<typename Sequence>
template<typename Container, typename Transform>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::from(Container&& container,
                      Transform&& transform)
{
  auto ret = make(std::make_move_iterator(container.begin()),
                  std::make_move_iterator(container.end()),
                  std::forward<Transform>(transform));
  container.clear();
  return ret;
}

template<typename Sequence>
template<typename Container>
typename pyseq<Sequence>::unique_ref
pyseq<Sequence>::from(Container&& container)
{
  auto ret = make(std::make_move_iterator(container.begin()),
                  std::make_move_iterator(container.end()));
  container.clear();
  return ret;
}

}  // namespace dctv
