// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

template<typename Command>
CommandOut<Command>::CommandOut(unique_pyref cb,
                                QueryExecution* qe)
    : out(std::move(cb), qe)
{}

template<typename Command>
template<typename... Args>
void
CommandOut<Command>::emit(Command command, Args... args) {
  this->out.add(static_cast<CommandType>(command));
  (..., this->out.add(static_cast<CommandType>(args)));
}

template<typename Command>
void
CommandOut<Command>::flush()
{
  this->out.flush();
}

CommandIn::CommandIn(unique_pyref cb)
    : iter(std::move(cb))
{}

template<typename Operand>
Operand
CommandIn::next()
{
  static_assert(sizeof (Operand) <= sizeof (CommandType));
  if (this->iter.is_at_eof())
    throw_pyerr_msg(PyExc_AssertionError, "command stream underflow");
  CommandType value = std::get<0>(this->iter.get());
  this->iter.advance();
  return static_cast<Operand>(value);
}

bool
CommandIn::is_at_eof() const
{
  return this->iter.is_at_eof();
}

}  // namespace dctv
