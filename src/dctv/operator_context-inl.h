// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace dctv {

unique_pyref
OperatorContext::get_query_action() const noexcept
{
  return addref(this->action);
}

npy_intp
OperatorContext::get_block_size() const noexcept
{
  return this->block_size;
}

QueryExecution*
OperatorContext::get_qe() const
{
  assume(this->link_qe);
  return this->link_qe;
}

template<typename Functor>
void
OperatorContext::enumerate_input_channels(Functor&& functor)
{
  for (auto& ic : this->input_channels)
    functor(ic.get());
}

}  // namespace dctv
