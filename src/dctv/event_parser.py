# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:#www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Parses an ftrace event dump"""

# pylint: disable=protected-access

import logging
import re

from functools import partial, lru_cache
from itertools import chain

from cytoolz import first
import numpy as np
import attr

from modernmp.util import (
  assert_seq_type,
  exceptions_are_fatal,
  ignore,
  once,
  the,
)

from ._native import (
  DTYPE_CPU_NUMBER,
  EventParser,
  QueryCache,
  QueryExecution,
  StringTable,
  UserEventMatcher,
  index_event_chunk_native,
  str_fast_take,
)

from .query import (
  QueryAction,
  QueryNode,
  QuerySchema,
  QuerySchemaKind,
  QueryTable,
  STRING_SCHEMA,
  TableKind,
  TableSchema,
  TableSorting,
  UNIQUE,
)

from .queryop import (
  AutoMultiQuery,
)

from .util import (
  DeferredSynchronousFuture,
  EqImmutable,
  ExplicitInheritance,
  INT32,
  INT64,
  NoneType,
  Timed,
  UINT64,
  all_same,
  cached_property,
  consume_list,
  final,
  future_exception,
  iattr,
  override,
  passivize_re,
  sattr,
  tattr,
  ureg,
)

from .util import get_cpu_count

from .trace_file_util import MappedTrace, ParseFailureError

log = logging.getLogger(__name__)

# Schema encoding file positions
POS_SCHEMA = QuerySchema(UINT64, unit=ureg().bytes, domain="_pos")

@once()
def _get_max_jobs():
  """Return the number of pending threaded jobs desired"""
  return get_cpu_count() + 2

# Segment size for initial trace indexing.  The smaller this value,
# the more parallelism we can extract in indexing and event parsing,
# but the more time we spend on per-segment overhead like searching
# for line endings.
INDEX_SEGMENT_BYTES = 8 * 1024 * 1024

# Number of events to give to each threaded worker when doing
# event parsing.
INDEX_SUBCHUNK_SIZE = 65536



def _throttle_futures(future_generator, max_pending):
  """Maintain a fixed number of jobs running in the background

  FUTURE_GENERATOR is an iterator yielding futures to run.  We keep
  MAX_PENDING futures running.

  Yields pairs (COMPLETED_FUTURE, IS_LAST_FUTURE), where
  COMPLETED_FUTURE is one of the futures that we made and
  IS_LAST_FUTURE is true iff COMPLETED_FUTURE is the last future in
  the series.
  """
  futures = []
  eof = False
  try:
    while True:
      while len(futures) < max_pending and not eof:
        try:
          futures.append(next(future_generator))
        except StopIteration:
          eof = True
      if not futures:
        break
      # Wait for completion before popping so that if we
      # raise, we'll cancel and wait on futures[0].
      future_exception(futures[0])  # Wait for completion
      future = futures.pop(0)
      yield future, (eof and not futures)
  except:
    with exceptions_are_fatal("draining throttled futures"):
      for future in futures:
        future.cancel()
      for future in consume_list(futures):
        future_exception(future)  # Wait for completion or cancellation
    raise

async def _throttle_futures_async(async_future_generator, max_pending):
  """Maintain a fixed number of jobs running in the background

  See _throttle_futures() for the interface.  This function is the
  coroutine version of that function.  We need this coroutine version
  so that ASYNC_FUTURE_GENERATOR can await.
  """
  futures = []
  eof = False
  try:
    while True:
      while len(futures) < max_pending and not eof:
        try:
          futures.append(await async_future_generator.asend(None))
        except StopAsyncIteration:
          eof = True
      if not futures:
        break
      # Wait for completion before popping so that if we
      # raise, we'll cancel and wait on futures[0].
      future_exception(futures[0])  # Wait for completion
      future = futures.pop(0)
      yield future, (eof and not future)
  except:
    with exceptions_are_fatal("draining throttled futures"):
      for future in futures:
        future.cancel()
      for future in consume_list(futures):
        future_exception(future)  # Wait for completion or cancellation
    raise



@attr.s(frozen=True, hash=True, cmp=True)
class SchemaColumnInfo(object):
  """Information for a column in an EventSchema"""
  converter = attr.ib(type=(str, NoneType))
  unit = attr.ib(type=(str, NoneType))
  domain = attr.ib(type=(str, NoneType))

  @property
  def has_interned_string(self):
    """Return whether this column contains a string"""
    return self.converter in ("s", "ss")

  @cached_property
  def dtype(self):
    """Numpy dtype object"""
    return np.dtype(EventParser.get_converter_dtype(self.converter))

class EventSchema(EqImmutable):
  """Information about an event type"""

  name = iattr(str)
  re_str = iattr(str)
  columns = tattr()
  is_user_event = iattr(bool)

  @override
  def __new__(cls, builder):
    # pylint: disable=protected-access
    assert isinstance(builder, EventSchemaBuilder)
    return cls._do_new(cls,
                       name=builder._name,
                       re_str=builder._re_str,
                       columns=tuple(builder._columns.items()),
                       is_user_event=builder._is_user_event)

  def make_parser(self, wanted_columns, base_schema):
    """Make an EventParser"""
    assert assert_seq_type(frozenset, str, wanted_columns)
    assert isinstance(base_schema, (EventSchema, NoneType))
    re_str = self.re_str
    if self.is_user_event:
      # Ignore weird trace-cmd event payload prefix.
      re_str = r"(?:0x[0-9a-f]+s: )?" + re_str
    column_items = self.columns
    if base_schema:
      re_str = "(?:{})(?:{})\n".format(base_schema.re_str, re_str)
      column_items = base_schema.columns + column_items
    munged_re_str = passivize_re(
      re_str,
      lambda capture_name: capture_name in wanted_columns)
    field_specs = tuple(
      (name, info.converter)
      for name, info in column_items
      if name in wanted_columns)
    return EventParser(munged_re_str, field_specs)

  @override
  def __repr__(self):
    return "EventSchema({!r})".format(self.name)

  @cached_property
  def column_name_set(self):
    """Set of all column names in this schema"""
    return frozenset(name for name, _ in self.columns)

  def get_regular_column_names(self, base_schema):
    """Return a set of non-special column names supplied"""
    return self.column_name_set | base_schema.column_name_set

VALID_COLUMN_RE = re.compile(r"[a-zA-Z_][a-zA-Z_0-9]*")

class EventSchemaBuilder(object):
  """Builds an EventSchema"""

  _is_user_event = False
  """True for events from trace marker writes"""

  def __init__(self, name, payload_re_str):
    assert isinstance(name, str)
    self._name = name
    self._re_str = payload_re_str
    self._columns = {}

  def column(self, name, converter, unit=None, domain=None):
    """Add a column to the schema description"""
    if not name:
      return self
    if not VALID_COLUMN_RE.fullmatch(name):
      raise ValueError("invalid column name: {!r}".format(name))
    assert isinstance(name, str)
    # TODO(dancol): check that converter exists!
    assert isinstance(converter, (str, SchemaColumnInfo))
    if name in self._columns:
      raise KeyError("column {!r} already defined".format(name))
    # TODO(dancol): do we just want to accept a QuerySchema instead of
    # having the column schema encode the QuerySchema's
    # component parts?
    if isinstance(converter, SchemaColumnInfo):
      assert unit is None
      assert domain is None
      info = converter
    else:
      info = SchemaColumnInfo(
        converter=converter,
        unit=unit,
        domain=domain,
      )
    self._columns[name] = info
    return self

  def user_event(self):
    """Mark this schema as belonging to a user event"""
    self._is_user_event = True
    return self

  def build(self):
    """Construct the schema this object describes"""
    es = EventSchema(self)
    if __debug__:
      # The parser already checks that all mentioned
      # fields are present in the RE, so we just have
      # to check for missing ones.  This code also has
      # the effect of checking the RE for validity.
      parser = es.make_parser(es.column_name_set, None)
      re_groups = frozenset(parser.get_named_captures())
      missing_groups = re_groups - es.column_name_set
      if missing_groups:
        raise ValueError("unused RE groups: {!r}"
                         .format(sorted(missing_groups)))
    return es

HEADER_STRIP_RE = re.compile(
  rb"".join([
    rb"^#(?:",
    rb"|".join([
      rb".*entries-in-buffer.*",
      rb".*tracer.*",
      rb"[ \t]*",
      rb"#.*",
    ]),
    rb")\n"]),
  re.MULTILINE)

def _make_generic_event_schema(event_type):
  return EventSchemaBuilder(event_type, r"[^\n]*").build()

_CONVERTER_BY_DTYPE = {
  INT32: "i",
  INT64: "l",
}

CPU_NUMBER_TYPE = SchemaColumnInfo(
  converter=_CONVERTER_BY_DTYPE[DTYPE_CPU_NUMBER],
  domain="cpu",
  unit=None,
)

RAW_TID_TYPE = SchemaColumnInfo(
  converter="i",
  domain="raw_tid",
  unit=None,
)

def _make_base_schema(*, tgid):
  return (
    EventSchemaBuilder(
      "<base>",
      "".join([
        r"(?P<comm>(?:.{16}|<\.\.\.>))-(?P<tid>[0-9]+) *",
        r"\([ -]*(?P<tgid>[0-9]*)\) *" if tgid else "",
        r"\[(?P<cpu>[0-9a-f]+)\] *",
        r"(?P<irqs_off>[dX.])",
        r"(?P<need_resched>[nN.])",
        r"(?P<hardirq>[hsH.])",
        r"(?P<preempt_depth>[0-9a-zA-Z.]+) *",
        r"(?P<_ts>[0-9]+\.[0-9]+): *",
        r"(?P<function>[a-zA-Z0-9_]+): *",
      ]))
    .column("comm", "ss")
    .column("tid", "i", domain="raw_tid")
    .column(bool(tgid) and "tgid", RAW_TID_TYPE)
    .column("cpu", CPU_NUMBER_TYPE)
    .column("irqs_off", "ord", domain="irqs_off")
    .column("need_resched", "ord", domain="need_resched")
    .column("hardirq", "ord", domain="hardirq")
    .column("preempt_depth", "b", domain="preempt_depth")
    .column("_ts", "timestamp", unit="ns")
    .column("function", "s")
    .build())

EXPECTED_HEADER_WITH_TGID = b"""\
#                                      _-----=> irqs-off
#                                     / _----=> need-resched
#                                    | / _---=> hardirq/softirq
#                                    || / _--=> preempt-depth
#                                    ||| /     delay
#           TASK-PID    TGID   CPU#  ||||    TIMESTAMP  FUNCTION
#              | |        |      |   ||||       |         |
"""

# TODO: actually verify that this header is right
EXPECTED_HEADER_WITHOUT_TGID = b"""\
#                              _-----=> irqs-off
#                             / _----=> need-resched
#                            | / _---=> hardirq/softirq
#                            || / _--=> preempt-depth
#                            ||| /     delay
#           TASK-PID   CPU#  ||||    TIMESTAMP  FUNCTION
#              | |       |   ||||       |         |
"""

# In the event that we don't have a header, we try parsing the first
# event in the order declared here and use the first matching schema.
SCHEMAS = {
  EXPECTED_HEADER_WITH_TGID: _make_base_schema(tgid=True),
  EXPECTED_HEADER_WITHOUT_TGID: _make_base_schema(tgid=False),
}

def _parse_header(events_mapping, pos=0):
  header_line_re = re.compile(rb"^#.*[^\n]*\n", re.MULTILINE)
  header_lines = []
  while True:
    m = header_line_re.match(events_mapping, pos)
    if not m:
      break
    header_lines.append(m.group(0))
    pos = m.end()
  header = b"".join(header_lines)
  return HEADER_STRIP_RE.sub(b"", header), pos

def _gen_segments(
    events_mapping,
    pos,
    events_end,
    chunk_size):
  """Generate coherent segments of a mapped trace file.

  Make sure that segment boundaries are aligned on event boundaries.

  EVENTS_MAPPING is a mapped trace.  POS is the offset within the
  trace mapping at which events start.  EVENTS_END is the offset at
  which events end.  CHUNK_SIZE is the target chunk size *in bytes*.
  The actual event chunks are snapped to event boundaries are their
  sizes may only approximate CHUNK_SIZE.

  This function is a generator and we don't fault in trace
  segments unnecessarily.
  """
  assert pos <= events_end
  index_re = INDEX_RE
  segment_start = pos
  while segment_start < events_end:
    segment_end = segment_start + chunk_size
    m = index_re.search(events_mapping, segment_end, events_end)
    if m is None:
      segment_end = events_end
    else:
      segment_end = m.start()
    yield (segment_start, segment_end)
    segment_start = segment_end

# This expression should be the simplest possible regex that matches
# ftrace event lines and that rejects stack trace lines. We use it to
# snap index chunk boundaries to event boundaries.
INDEX_RE = re.compile((rb"^(?:.{16}|<\.\.\.>)-[^\n]*: "
                       rb"([a-zA-Z0-9_]+): [^\n]*\n"),
                      re.MULTILINE)

@final
class IndexEvents(AutoMultiQuery):
  """Build an index of events from a trace file"""

  trace_id = iattr(kwonly=True)

  class Meta(AutoMultiQuery.AutoMeta):
    """Metadata selector"""
    _POS = POS_SCHEMA
    EVENT_TYPE = STRING_SCHEMA

  @override
  def compute_inputs(self):
    return ()  # Look! No hands!

  @override
  def compute_outputs(self):
    return [self.metaq(meta) for meta in self.Meta]

  @override
  async def run_async(self, qe):
    Meta = self.Meta
    [], [oc_pos, oc_event_type] = await qe.async_setup(
      (), (self.metaq(Meta._POS), self.metaq(Meta.EVENT_TYPE)))
    async def _deliver_output_async(arrays, is_eof):
      positions, event_types = arrays
      assert len(positions) == len(event_types)
      await qe.async_io(oc_pos.write(positions, is_eof),
                        oc_event_type.write(event_types, is_eof))
    await IndexedEvents.from_qe(qe, self.trace_id).index_events_async(
      qe=qe, deliver_output_async=_deliver_output_async)

  def pos_for_event_type(self, event_type):
    """Return a query yielding positions for an event type"""
    Meta = self.Meta
    pos_q = self.metaq(Meta._POS)
    event_type_q = self.metaq(Meta.EVENT_TYPE)
    return pos_q[event_type_q.eq(event_type)]

# TODO(dancol): increase limit
@lru_cache(1)
def _get_event_parser(base_schema, event_schema, wanted_columns):
  return event_schema.make_parser(wanted_columns, base_schema)

def _parse_event_chunk(
    *,
    mapped_trace,
    positions,
    event_schema,
    base_schema,
    wanted_columns,
    st,
    qe):
  assert all(positions >= 0)
  parser = _get_event_parser(base_schema, event_schema, wanted_columns)
  events = {}
  def _mk_event_receiver(name):
    def _event_receiver(blob):
      assert name not in events
      events[name] = np.asarray(blob)
    return _event_receiver
  receivers = tuple(_mk_event_receiver(name) for name, _
                    in chain(base_schema.columns,
                             event_schema.columns)
                    if name in wanted_columns)
  parser.parse(st,
               mapped_trace.mapping,
               iter([(positions,)]),
               receivers,
               qe)
  return st, events

def _index_event_chunk(
    *,
    mapped_trace,
    segment_start,
    segment_end,
    st,
    um,
    qe):
  """Index events in a chunk of a trace file.

  MAPPED_TRACE is a mapped trace. SEGMENT_START and SEGMENT_END
  describe the region inside the mapped trace to index.

  UM is the UserEventMatcher for recognizing user events.
  QE is the QueryExecution.

  Return a tuple (EVENT_POSITIONS_BY_TYPE, LAST_EVENT_END), where
  EVENT_POSITIONS_BY_TYPE is a dictionary mapping event types to file
  positions where events of that type occur and LAST_EVENT_END is
  the file position at which the last event ends.

  If no event matches, return ({}, POS).
  """
  outputs = []
  def _result_cb(*buffers):
    outputs.append(list(buffers))
  with memoryview(mapped_trace.mapping) as whole_view:
    with whole_view[:segment_end] as events_mapping:
      index_event_chunk_native(events_mapping,
                               segment_start,
                               st,
                               um,
                               _result_cb,
                               qe)
  return st, outputs

class MagicSchemaDict(dict):
  """Create generic schema descriptions as needed"""
  def __missing__(self, key):
    self[key] = schema = _make_generic_event_schema(key)
    return schema
  __slots__ = ()

def _get_aux_string_table(aux_string_tables):
  assert isinstance(aux_string_tables, list)
  if aux_string_tables:
    return aux_string_tables.pop()
  aux_string_table = StringTable()
  aux_string_table.my_seqno = 0
  return aux_string_table

def _translate_chunk_strings(aux_string_table,
                             st,
                             arrays):
  assert aux_string_table is not st
  if aux_string_table.my_seqno == aux_string_table.seqno:
    tx_table = aux_string_table.to_st_tx_table
  else:
    aux_string_table.to_st_tx_table = tx_table = \
      aux_string_table.make_translation_table(st)
    aux_string_table.my_seqno = aux_string_table.seqno
  for array in arrays:
    str_fast_take(array, array, tx_table)

@final
class IndexedEvents(ExplicitInheritance):
  """Loads events from a trace file on demand"""

  @override
  def __init__(self,
               *,
               mapped_trace,
               events_start,
               events_end,
               time_basis_override=None,
               event_schemas=(),
               progress_callback=None):
    assert isinstance(mapped_trace, MappedTrace)
    assert the(int, events_start) >= 0
    assert the(int, events_end) >= events_start

    if not progress_callback:
      progress_callback = ignore

    with Timed("parse header"):
      progress_callback("Parsing event header")
      header, pos = _parse_header(mapped_trace.mapping, events_start)
      with mapped_trace.augment_parse_errors():
        base_schema, time_basis = _find_base_schema_and_time_basis(
          mapped_trace.mapping, header, pos)
    if time_basis_override is None:
      time_basis = int(time_basis)
    else:
      time_basis = the(int, time_basis_override)

    self.__mapped_trace = mapped_trace
    self.__events_start = pos
    self.__events_end = events_end
    self.__base_schema = the(EventSchema, base_schema)
    self.__time_basis = time_basis

    self.__event_schemas = MagicSchemaDict({
      schema.name: the(EventSchema, schema)
      for schema in event_schemas
    })

  def get_event_ts_at(self, pos):
    """Get timestamp as position"""
    return _get_event_nsecs_at(
      self.__mapped_trace.mapping,
      pos,
      self.__base_schema) - self.__time_basis

  @property
  def event_schemas(self):
    """Mapping from event schema name to event schema

    Do not modify.
    """
    return self.__event_schemas

  def make_event_query_table(self, event_type, trace_id):
    """Make a QueryTable for a specific event type"""
    # Unfortunately, event ordering is not quite guaranteed even in
    # direct reads from trace_pipe.  so we need to mark the event
    # tables as unsorted and do our own sorting pass.
    schemas = self.get_event_query_schemas(event_type)
    return TraceEventQueryTable(
      schemas=schemas,
      event_type=event_type,
      trace_id=trace_id,
      table_schema=TableSchema(TableKind.EVENT, None, TableSorting.NONE))

  def get_event_query_schemas(self, event_type):
    """Get the query schema describing the given event.

    EVENT_TYPE must have been registered with this parser instance.

    The return value is a dict mapping column name in the event to
    that column's query schema.
    """
    event_schema = self.__event_schemas[event_type]
    query_schema = {}
    for name, column_info in chain(
        self.__base_schema.columns,
        event_schema.columns):
      query_schema[name] = \
        QuerySchema(column_info.dtype,
                    kind=(QuerySchemaKind.STRING if
                          column_info.has_interned_string
                          else QuerySchemaKind.NORMAL),
                    domain=column_info.domain,
                    unit=(ureg().parse_units(column_info.unit)
                          if column_info.unit else None))
    return query_schema

  async def index_events_async(self,
                               *,
                               deliver_output_async,
                               qe):
    """Index the trace file

    DELIVER_OUTPUT_ASYNC is a coroutine we await whenever we have an
    index chunk.  It's called with two arguments: POSITIONS and
    EVENT_TYPES.  POSITIONS is a uint64 array of file positions;
    EVENT_TYPES is a string code array describing the event type at
    each position.  QE is the QueryExecution.
    """

    um_pat = r"\A(?:{})\Z".format(
      r"|".join(
        "(?P<{}>{})".format(schema.name, passivize_re(schema.re_str))
        for schema in self.__event_schemas.values()
        if schema.is_user_event))
    um = UserEventMatcher(um_pat)

    # TODO: maybe support indexing in subprocess for
    # easier cancellation.

    get_tp = qe.env.get("get_tp")
    tp = get_tp() if get_tp else None
    aux_string_tables = []
    st = qe.st

    if tp:
      log.info("Indexing using threads")
      def make_index_chunk_future(**kwargs):
        return tp.submit(_index_event_chunk,
                         st=_get_aux_string_table(aux_string_tables),
                         um=um,
                         qe=qe,
                         **kwargs)
    else:
      make_index_chunk_future = partial(
        DeferredSynchronousFuture,
        _index_event_chunk,
        st=st, um=um, qe=qe)

    def _generate_index_futures():
      for segment_start, segment_end in _gen_segments(
          self.__mapped_trace.mapping,
          self.__events_start,
          self.__events_end,
          INDEX_SEGMENT_BYTES):
        yield make_index_chunk_future(
          mapped_trace=self.__mapped_trace,
          segment_start=segment_start,
          segment_end=segment_end)

    with self.__mapped_trace.augment_parse_errors():
      for future, is_last_future in _throttle_futures(
          _generate_index_futures(),
          _get_max_jobs()):
        result_st, result_arrays = future.result()
        for positions_array, event_type_array \
            in consume_list(result_arrays):
          if result_st is not st:
            _translate_chunk_strings(result_st, st, (event_type_array,))
          await deliver_output_async((positions_array, event_type_array),
                                     is_last_future)
        if result_st is not st:
          aux_string_tables.append(result_st)
        del result_st

  async def parse_event_type_async(self,
                                   *,
                                   event_type,
                                   get_index_chunk_async,
                                   deliver_output_async,
                                   qe,
                                   wanted_columns=(),
                                   progress_callback=None):
    """Parse some fields of an event type.

    EVENT_TYPE is a string naming the event type to parse.

    GET_INDEX_CHUNK_ASYNC is an coroutine function that gives us
    arrays of file positions at which we can find the event we want to
    parse.  The sizes of the index chunks determine the sizes of the
    parsed event chunks.

    DELIVER_OUTPUT_ASYNC is a coroutine function to await every time
    we have a chunk of output data.  It's called with two arguments:
    1) a sequence of (COLUMN_NAME, COLUMN_ARRAY) pairs providing the
    chunk column data, and 2) an IS_EOF flag indicating whether the
    chunk being delivered is the last one.  The return value
    is ignored.

    QE is the QueryExecution running this index operation.

    WANTED_COLUMNS is a sequence of column names to parse.
    PROGRESS_CALLBACK is a progress callback which we'll tell about
    what we're doing to parse.
    """
    progress_callback = progress_callback or ignore
    wanted_columns = frozenset(wanted_columns)
    assert "_pos" not in wanted_columns
    schema = self.__event_schemas[event_type]
    regular_columns = schema.get_regular_column_names(self.__base_schema)
    assert wanted_columns.issubset(regular_columns), \
      "columns not provided by schema: {}".format(
        wanted_columns - regular_columns)

    base_schema = self.__base_schema
    conflicting_columns = (schema.column_name_set &
                           base_schema.column_name_set)
    if conflicting_columns:
      raise ValueError("conflicting column names between schema "
                       "{} and base schema: {!r}".format(
                         schema.name,
                         conflicting_columns))

    interned_string_columns = [
      name for name, info
      in chain(self.__base_schema.columns, schema.columns)
      if info.has_interned_string
    ]

    get_tp = qe.env.get("get_tp")
    tp = get_tp() if get_tp else None
    aux_string_tables = []
    st = qe.st
    if tp:
      def make_parse_event_chunk_future(**kwargs):
        return tp.submit(_parse_event_chunk,
                         st=_get_aux_string_table(aux_string_tables),
                         qe=qe,
                         **kwargs)
    else:
      make_parse_event_chunk_future = \
        partial(DeferredSynchronousFuture,
                _parse_event_chunk, st=st, qe=qe)

    def _fixup_ts(chunk_field_arrays, *, time_basis=self.__time_basis):
      # TODO(dancol): do time basis adjustment in SQL!
      if time_basis is not None and "_ts" in chunk_field_arrays:
        chunk_ts = chunk_field_arrays["_ts"]
        fixed_chunk_ts = qe.qc.make_uninit_array(
          chunk_ts.dtype, len(chunk_ts))
        np.subtract(chunk_ts, time_basis, out=fixed_chunk_ts)
        chunk_field_arrays["_ts"] = fixed_chunk_ts

    def _fixup_strings(chunk_st, chunk_field_arrays):
      assert chunk_st is not st
      string_arrays = []
      for column_name in chunk_field_arrays:
        if column_name in interned_string_columns:
          string_arrays.append(chunk_field_arrays[column_name])
      _translate_chunk_strings(chunk_st, st, string_arrays)

    progress_message = "Parsing {}".format(schema.name)
    progress_callback(progress_message)

    async def _generate_parse_futures_async():
      async for index_chunk in get_index_chunk_async():
        for index_subchunk in np.array_split(
            index_chunk,
            range(0, len(index_chunk), INDEX_SUBCHUNK_SIZE)):
          yield make_parse_event_chunk_future(
            mapped_trace=self.__mapped_trace,
            positions=index_subchunk,
            event_schema=schema,
            base_schema=self.__base_schema,
            wanted_columns=wanted_columns)

    with self.__mapped_trace.augment_parse_errors():
      async for resolved_parse_future, is_last_future in (
          _throttle_futures_async(_generate_parse_futures_async(),
                                  _get_max_jobs())):
        chunk_st, chunk_field_arrays = resolved_parse_future.result()
        _fixup_ts(chunk_field_arrays)
        if chunk_st is not st:
          _fixup_strings(chunk_st, chunk_field_arrays)
          aux_string_tables.append(chunk_st)
        del chunk_st
        assert all_same(len(array)
                        for array in chunk_field_arrays.values())
        await deliver_output_async(chunk_field_arrays.items(),
                                   is_last_future)
        del chunk_field_arrays

  @staticmethod
  def from_qe(qe, trace_id):
    """Find the IndexedEvents object from a QueryExecution"""
    # For us, the trace context is always a FileTraceContext
    return qe.env["get_trace_context_by_trace_id"](trace_id).events

def _get_event_nsecs_at(trace_mapping, pos, base_schema):
  """Find the timestamp of the event at POS"""
  ts = None
  def _recv_ts(ts_blob):
    nonlocal ts
    ts_array = np.asarray(ts_blob)
    assert len(ts_array) <= 1
    if len(ts_array):  # pylint: disable=len-as-condition
      assert ts is None
      ts = ts_array[0]
  generic_event_schema = _make_generic_event_schema("generic")
  parser = generic_event_schema.make_parser(frozenset(["_ts"]), base_schema)
  upos = np.uint64(pos)
  parser.parse(StringTable(), trace_mapping,
               iter([(upos,)]), (_recv_ts,),
               QueryExecution.make(plan=(),
                                   qc=QueryCache(block_size=1)))
  assert ts is not None
  return ts

def _find_base_schema_and_time_basis(trace_mapping, header, pos):
  # If we have a header, it tells us the former.
  if header:
    base_schema = SCHEMAS.get(header)
    if not base_schema:
      raise ParseFailureError(0, "header present but unrecognized")
    time_basis = _get_event_nsecs_at(trace_mapping, pos, base_schema)
    return base_schema, time_basis
  # We don't have a header, so guess.
  for candidate_base_schema in SCHEMAS.values():
    try:
      time_basis = _get_event_nsecs_at(
        trace_mapping, pos, candidate_base_schema)
    except ParseFailureError:
      pass
    else:
      return candidate_base_schema, time_basis
  raise ParseFailureError(0, "cannot detect trace format")

@final
class TraceEventQuery(QueryNode):
  """Query that resolves to a field in a raw event table"""

  _query_schema = iattr(QuerySchema, name="query_schema", kwonly=True)
  trace_id = iattr(kwonly=True)
  event_type = iattr(str, kwonly=True)
  column = iattr(str, kwonly=True)

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    assert UNIQUE not in self.schema.constraints

  @override
  def _compute_schema(self):
    return self._query_schema

  @override
  def make_action(self):
    return TraceEventAction((self,))

  @override
  def _no_print_keys(self):
    return super()._no_print_keys() | {"_schema"}

@final
class TraceEventAction(QueryAction):
  """Query action for parsing regular columns out of trace events"""

  queries = sattr(TraceEventQuery)

  @override
  def _post_init_assert(self):
    super()._post_init_assert()
    queries = self.queries
    assert queries, "cannot query zero fields"
    assert all(isinstance(sq, TraceEventQuery) for sq in queries)
    assert all_same(sq.event_type for sq in queries)
    assert all_same(sq.trace_id for sq in queries)
    # Queries differ only in column name!

  @cached_property
  def __positions_query(self):
    query = first(self.queries)
    config = IndexEvents(trace_id=query.trace_id)
    return config.pos_for_event_type(query.event_type)

  @override
  def _compute_inputs(self):
    return (self.__positions_query,)

  @override
  def _compute_outputs(self):
    return self.queries

  @override
  async def run_async(self, qe):
    first_query = first(self.queries)
    event_type = first_query.event_type
    wanted_columns = frozenset(sq.column for sq in self.queries)
    assert len(wanted_columns) == len(self.queries)
    events = IndexedEvents.from_qe(qe, first_query.trace_id)
    [ic_positions], ocs = await qe.async_setup(
      [self.__positions_query],
      self.queries)
    ocs_by_column_name = {oc.query.column: oc for oc in ocs}
    async def _get_index_chunk_async():
      block = await ic_positions.read(qe.block_size)
      while block:
        yield block.as_array()
        block = await ic_positions.read(qe.block_size)
    async def _deliver_output_async(parse_chunk, is_eof):
      await qe.async_io(*[
        ocs_by_column_name[column_name].write(column_array, is_eof)
        for column_name, column_array in parse_chunk])
    await events.parse_event_type_async(
      event_type=event_type,
      get_index_chunk_async=_get_index_chunk_async,
      deliver_output_async=_deliver_output_async,
      wanted_columns=wanted_columns,
      qe=qe,
      progress_callback=qe.progress_callback or ignore)

  @override
  def _compute_merge_key(self):
    query = first(self.queries)
    return type(self), query.event_type, query.trace_id

  @override
  def merge(self, others):
    queries = self.queries.union(*(other.queries for other in others))
    return TraceEventAction(queries)

@final
class TraceEventQueryTable(QueryTable):
  """QueryTable for generating trace event queries"""

  _schemas = iattr(dict, name="schemas")
  _trace_id = iattr(name="trace_id")
  _event_type = iattr(str, name="event_type")

  @override
  def _no_print_keys(self):
    return super()._no_print_keys() | {"_schemas"}

  @override
  def _make_column_tuple(self):
    return ("_pos",) + tuple(self._schemas)

  @override
  def _make_column_query(self, column):
    # Special case: just filter the global index
    if column == "_pos":
      return IndexEvents(trace_id=self._trace_id)\
        .pos_for_event_type(self._event_type)
    return TraceEventQuery(
      event_type=self._event_type,
      column=column,
      trace_id=self._trace_id,
      query_schema=self._schemas[column])
