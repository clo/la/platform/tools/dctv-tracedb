// Copyright (C) 2020 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "fsutil.h"

#include <stdint.h>
#include <limits>

#include "pyutil.h"

namespace dctv {

UniqueFd
xopen(const char* name, int flags, mode_t mode)
{
  int fd = open(name, flags | O_CLOEXEC, mode);
  if (fd < 0)
    throw_from_errno_with_filename(name);
  return UniqueFd(fd);
}

UniqueFd
xopenat(int dirfd, const char* name, int flags, mode_t mode)
{
  int fd = openat(dirfd, name, flags | O_CLOEXEC, mode);
  if (fd < 0)
    throw_from_errno_with_filename(name);
  return UniqueFd(fd);
}

size_t
xread_all(const int fd, void* const mem, const size_t nbytes)
{
  size_t total_nbytes_read = 0;
  const auto pos = static_cast<uint8_t*>(mem);
  while (total_nbytes_read < nbytes) {
    const size_t nbytes_to_read =
        std::min(nbytes - total_nbytes_read,
                 static_cast<size_t>(
                     std::numeric_limits<ssize_t>::max()));
    ssize_t nbytes_read = read(fd,
                               pos + total_nbytes_read,
                               nbytes_to_read);
    if (nbytes_read < 0 && errno == EINTR)
      continue;
    if (nbytes_read < 0)
      throw_from_errno();
    assume(nbytes_read <= nbytes_to_read);
    if (nbytes_read == 0)
      break;
    total_nbytes_read += nbytes_read;
  }
  return total_nbytes_read;
}

void
xwrite_all(const int fd,
           const void* const mem,
           size_t nbytes)
{
  auto pos = static_cast<const uint8_t*>(mem);
  while (nbytes) {
    const size_t nbytes_to_write =
        std::min(nbytes,
                 static_cast<size_t>(
                     std::numeric_limits<ssize_t>::max()));
    const ssize_t nbytes_written = write(fd, pos, nbytes_to_write);
    if (nbytes_written < 0 && errno == EINTR)
      continue;
    if (nbytes_written < 0)
      throw_from_errno();
    assume(nbytes_written <= nbytes);
    nbytes -= static_cast<size_t>(nbytes_written);
    pos += nbytes_written;
  }
}

void
safe_unlinkat(int dirfd, std::string_view name) noexcept
{
  // We're noexcept, so we can't allocate memory.  But NAME isn't
  // guaranteed to be NUL-terminated.  Allocate a string on the
  // stack instead.
  assume(name.size() < std::numeric_limits<size_t>::max());
  char* name_nul_terminated =
      static_cast<char*>(alloca(name.size() + 1));
  memcpy(name_nul_terminated, name.data(), name.size());
  name_nul_terminated[name.size()] = '\0';
  if (unlinkat(dirfd, name_nul_terminated, /*flags=*/0)) {
    // unlinkat failed: best-effort attempt to whine about it
    try {
      PyErr_WriteUnraisable(
          pyref(make_pystr_fmt("unlink cleanup failed: %s",
                               strerror(errno))).get());
    } catch (...) {}
  }
}

void
xtruncate(int fd, size_t nbytes)
{
  if (ftruncate(fd, nbytes))
    throw_from_errno();
}

}  // namespace dctv
