# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Makefile containing native build configuration Split into a separate
# file so we can make native code depend on it independently of
# native.mk, allowing us to add a new module without recompiling
# the world.

PYVER:=$(shell $(VENV)/run $(PYBIN) -c \
	'import sys; print("{}.{}".format(*sys.version_info[:2]))')
ifeq ($(PYVER),)
$(error "No $(PYBIN) binary found")
endif

LPAREN:=(
RPAREN:=)
CXX_VERSION:=$(shell $(CXX) --version | head -n1 | sed -e 's/ *$(LPAREN)[^$(RPAREN)]*$(RPAREN)//g')
ifeq ($(CXX_VERSION),)
$(error "No CXX compiler found")
endif

ifneq ($(findstring g++ 7., $(CXX_VERSION)),)
BADCXX:=ICEs during template resolution
endif

# Dies on duplicate local variable names in extern "C"
ifneq ($(findstring g++ 8.0., $(CXX_VERSION)),)
BADCXX:=Extern "C" duplicate symbol detector trips over local variables (https://gcc.gnu.org/bugzilla/show_bug.cgi?id=85580)
endif

ifneq ($(findstring g++ 8.3., $(CXX_VERSION)),)
BADCXX:=ICE in enclosing_instantiation_of: https://www.mail-archive.com/gcc-bugs@gcc.gnu.org/msg602907.html
endif

ifneq ($(BADCXX),)
ifeq ($(origin CXX),default)
NEW_CXX:=clang++-9
$(info NOTE: default compiler $(CXX) $(LPAREN)version $(CXX_VERSION)$(RPAREN) is known buggy $(LPAREN)$(BADCXX)$(RPAREN): autoselecting $(NEW_CXX) instead)
CXX:=$(NEW_CXX)
CXX_VERSION:=$(shell $(CXX) --version | head -n1 | sed -e 's/ *$(LPAREN)[^$(RPAREN)]*$(RPAREN)//g')
else
$(warning known-bad compiler $(CXX) $(LPAREN)version $(CXX_VERSION)$(RPAREN) detected $(LPAREN)$(BADCXX)$(RPAREN): compiling anyway because compiler was explicitly specified)
endif
endif

ifeq ($(findstring clang,$(CXX_VERSION)),clang)
is_clang:=1
else
is_clang:=0
endif

ifeq ($(findstring g++,$(CXX_VERSION)),g++)
is_gcc:=1
else
is_gcc:=0
endif

NDEBUG:=-DNDEBUG
CFLAGS_OPT:=-O3 -flto -march=native

WARNINGS:=-Wall -Werror
WARNINGS+=-Woverflow
WARNINGS+=-Wno-unused
WARNINGS+=-Wno-missing-braces
WARNINGS+=-Wno-invalid-offsetof
WARNINGS+=-Wmissing-declarations

# Boost iostreams needs rtti to compile even though we don't use it at
# runtime. :-(
CFLAGS=$(CFLAGS_OPT) $(NDEBUG) -ffunction-sections
override CPPFLAGS+=-DNPY_NO_DEPRECATED_API=NPY_1_7_API_VERSION -DDCTV_DETERMINISTIC_ABSEIL=1
override CFLAGS+=-pthread
override CFLAGS+=-fvisibility=hidden -fvisibility-inlines-hidden -fPIC $(CXX17_CFLAGS)
override CFLAGS+=-ggdb
override CFLAGS+=-ftemplate-backtrace-limit=0

ifeq ($(is_clang),1)
CXX17_CFLAGS:=-std=gnu++1z
endif
ifeq ($(is_gcc),1)
CXX17_CFLAGS:=-std=gnu++17
# Work around GCC bug https://gcc.gnu.org/bugzilla/show_bug.cgi?id=80947
override CFLAGS+=-Wno-attributes
endif

ifndef CXX17_CFLAGS
$(error	"Don't know how to enable C++17 for $(CXX)")
endif

# Ugh.  Hack.
AR:=ar
RANLIB:=ranlib
ifneq ($(filter -flto,$(CFLAGS)),)
LTO_CC:=$(filter clang++%,$(CXX))
ifneq ($(LTO_CC),)
LTO_AR:=$(LTO_CC:clang++%=llvm-ar%)
LTO_RANLIB:=$(LTO_CC:clang++%=llvm-ranlib%)
endif
LTO_CC:=$(filter g++%,$(CXX))
ifneq ($(LTO_CC),)
LTO_AR:=$(LTO_CC:g++%=gcc-ar%)
LTO_RANLIB:=$(LTO_CC:g++%=gcc-ranlib%)
endif
ifndef LTO_AR
$(error "Don't know about LTO configuration for $(CXX)")
endif
AR:=$(LTO_AR)
RANLIB:=$(LTO_RANLIB)
endif

NUMPY_CORE:=$(shell $(VENV)/run $(PYBIN) -c \
	'import os, numpy; print(os.path.dirname(numpy.__file__)+"/core")')
ifeq ($(NUMPY_CORE),)
$(error "No numpy found")
endif

NUMPY_INCDIR=$(NUMPY_CORE)/include
NUMPY_LIBDIR=$(NUMPY_CORE)/lib

ifeq ($(OS),Windows_NT)
	$(error "Windows not yet a supported build environment")
else
	UNAME_S:=$(shell uname -s)
endif

BOOST_LIBS:=-lboost_coroutine -lboost_context

ifeq ($(UNAME_S),Linux)
	PYTHON_CFLAGS:=$(shell pkg-config --cflags python-$(PYVER))
	PYTHON_LIBS:=$(shell pkg-config --libs python-$(PYVER))
        # TODO: Point to zdefs explicitly.
        # https://groups.google.com/forum/#!topic/mailing.openbsd.tech/TKRTaMu18Vw
        override LDFLAGS+=-Wl,-zdefs -Wl,-ztext
	GTK_SHARED_LIBRARY:=libgtk-3.so.0,libgdk-3.so.0
	GOBJECT_SHARED_LIBRARY:=libgobject-2.0.so.0
endif
ifeq ($(UNAME_S),Darwin)
	PYCONFIG_CMD:="python${PYVER}m-config"
	PYTHON_CFLAGS:=$(shell ${PYCONFIG_CMD} --cflags) -Wno-all
	PYTHON_LIBS:=$(shell ${PYCONFIG_CMD} --ldflags)
	GTK_SHARED_LIBRARY:=libgtk-3.dylib,libgdk-3.dylib
	GOBJECT_SHARED_LIBRARY:=libgobject-2.0.dylib
endif

ifeq ($(PYTHON_CFLAGS),)
$(error "could not find python3 flags")
endif

ifeq ($(PYTHON_LIBS),)
# Ugh. With Python 3.8, pkg-config --libs stopped giving us a shared
# library for resolving exports, so just specify it manually instead.
PYTHON_LIBS:=-lpython$(PYVER)
endif

PCRE2_LIBS:=$(shell pcre2-config --libs8)
PCRE2_CFLAGS:=$(shell pcre2-config --cflags)
ifeq ($(PCRE2_LIBS),)
$(error "could not find pcre2")
endif

# The boost libraries that need compilation and linking. Header-only
# boost libraries don't need any special notes in the build system.
boost_libs:=coroutine context
