#!/bin/zsh
# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# This script is used as part of a magic incantation in DCTV top-level
# scripts that makes programs run inside the DCTV virtual environment
# even if run as normal executables, perhaps via symlink, while still
# allowing a direct Python invocation to run the script normally (a
# capability which is important for debugging).  The hack works by
# turning the launcher scripts into zsh and Python polyglots by
# abusing Python and shell string quoting syntax.
dctv_dir=${0:h}
stamp="${dctv_dir}/venv/setup.stamp"
if ! [[ -r $stamp ]]; then
    printf >&2 "%s: venv not configured: did you forget to run 'make dev'?\n" \
               "${ZSH_ARGZERO:t}"
    exit 1
fi
interp="${dctv_dir}/venv/bin/python"
if ! [[ -x $interp ]]; then
    printf >&2 "%s: venv corrupted: no interpreter!'.\n" "${ZSH_ARGZERO:t}"
    exit 1
fi
if [[ $interp -nt $stamp ]]; then
    printf >&2 "%s: venv older than interpreter: refresh with 'make clean && make dev'.\n" \
               "${ZSH_ARGZERO:t}"
    exit 1
fi
. "${dctv_dir}/venv/run" "$@"
exit 1  # just in case: exec isn't supposed to return
