# Copyright (C) 2020 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# Run me like this:
# $ dctv query --format=csv "$(cat examples/rss_by_type.sql)" trace=mytrace.txt > memory-info.csv
# $ gnuplot -p -c examples/memory-info.sql memory-info.csv

set datafile separator comma
set key box autotitle columnheader width 2 noenhanced
set key title "Columns"
set y2tic
set xlabel "Time"
set ylabel "Bytes"
set y2label "#proc"
set key outside
set key right top
prg = ARG1
memcols = "rss_oom_lt_zero rss_oom_eq_zero rss_fg rss_bg rss_cached rss_unknown"
proccols = "nr_unknown_oom_score nr_processes"

plot for [memcol in memcols] \
     prg using (column("_ts")):(column(memcol)) title memcol \
     with lines lw 2, \
     for [proccol in proccols] \
     prg using (column("_ts")):(column(proccol)) title proccol \
     axis x1y2 with lines \
     #end
